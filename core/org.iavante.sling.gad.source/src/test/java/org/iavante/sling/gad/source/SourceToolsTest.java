/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.source;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.gad.source.impl.SourceToolsImpl;

public class SourceToolsTest extends RepositoryTestBase {
	
	SourceToolsImpl source_tools;
	SlingRepository repo;
	Session session;
	
	private Node rootNode;
	private Node sources_node;
	private Node source1_node;
	private Node source2_node;
	private Node source3_node;
	
	private final String source1_title = "source1";
	private final String source2_title = "source2";
	private final String source3_title = "source3";
	
	
	 @Override
	 protected void setUp() throws Exception {	    	
		 super.setUp();
		 session = getSession();	    		
		 rootNode = getSession().getRootNode(); 
		 source_tools = new SourceToolsImpl();
		 
		 // Add sources node
		 sources_node = rootNode.addNode("sources", "nt:unstructured");
		 session.save();
		 
		 // Add sources
		 source1_node = sources_node.addNode(source1_title, "nt:unstructured");
		 source2_node = sources_node.addNode(source2_title, "nt:unstructured");
		 source3_node = sources_node.addNode(source3_title, "nt:unstructured");		 
		 sources_node.save();
		 
		 // Set properties
		 source1_node.setProperty("schematype", "schema");
		 source3_node.setProperty("schematype", "playlist");
		 
		 source1_node.save();
		 source2_node.save(); 
		 source3_node.save();
	}
	
    protected void tearDown() {
    }
    
    public void test_is_a_playlist_source() throws RepositoryException {    

    	assertTrue(!source_tools.is_a_playlist_source(source1_node));
    	assertTrue(source_tools.is_a_playlist_source(source2_node));
    	assertTrue(source_tools.is_a_playlist_source(source3_node));    	
    }
}