/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.source;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test content creation and schema association
 */
public class SourceServiceTestIT
		extends TestCase {

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	private String COL_URL = "/content/colecciones/";
	private String CONTENTS_FOLDER = "contents";
	private String SOURCES_FOLDER = "sources";
	private String TRANSFORMATIONS_FOLDER = "transformations";
	private String TRANSFORMATION_PREVIEW = "preview";
	private String TRANSFORMATION_TEST_TITLE = "test";
	private String TRANSFORMATION_PREVIEW_TITLE = "preview";
	private String TAGS_REQUEST = "    test,     case";
	private String TAGS_RESPONSE = "test,case";

	private String col_title;
	private String source_title;
	private String content_title;
	private String file;
	private String mimetype;
	private String type;
	private String schema;
	private List authPrefs;
	private HttpClient client;
	private Credentials defaultcreds;

	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		col_title = "it_col";
		content_title = "it_content";
		source_title = "it_source";
		file = "integration-test.flv";
		mimetype = "video/flv";
		type = "video";
		schema = "default";

		authPrefs = new ArrayList(2);
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);

		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Create collection
		PostMethod post_create_col = new PostMethod(SLING_URL + COL_URL + col_title);
		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", col_title),
				new NameValuePair("schema", schema), new NameValuePair("subtitle", ""),
				new NameValuePair("extern_storage", "on"),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Collection created
		assertEquals(201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();

		// Create content in collection
		PostMethod post_create_content = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
		post_create_content.setDoAuthentication(true);

		NameValuePair[] data_create_content = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", content_title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum "
								+ "dolor sit amet, consectetur adipisicing elit, sed do eiusmod "
								+ "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad "
								+ "minim veniam, quis nostrud exercitation ullamco laboris nisi ut "
								+ "aliquip ex ea commodo consequat. Duis aute irure dolor in "
								+ "reprehenderit in voluptate velit esse cillum dolore eu fugiat "
								+ "nulla pariatur."), new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags", "test case"),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_content.setRequestBody(data_create_content);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Content created
		assertEquals(201, post_create_content.getStatusCode());
		post_create_content.releaseConnection();
	}

	protected void tearDown() {

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, post_delete.getStatusCode());
		post_delete.releaseConnection();
	}

	/*
	 * Tests the property <em>notify_status_code</em>. This property has the
	 * status code returned by the transcoder
	 */
	public void test_notify_status_code() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", ""), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", TAGS_REQUEST),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create.getStatusCode());
		post_create.releaseConnection();

		// Edit the source
		PostMethod post_edit = new PostMethod(SLING_URL + COL_URL + col_title + "/"
				+ CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER + "/"
				+ source_title);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_edit = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("file", file),
				new NameValuePair("mimetype", mimetype),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", type), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_edit.setRequestBody(data_edit);
		post_edit.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit.getStatusCode());

		try {
			Thread.sleep(12000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get trans preview notify_status_code
		HttpMethod get_trans_preview_status = new GetMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/"
				+ SOURCES_FOLDER + "/" + source_title + "/" + TRANSFORMATIONS_FOLDER
				+ "/" + TRANSFORMATION_PREVIEW + "/" + "notify_status_code");
		try {
			client.executeMethod(get_trans_preview_status);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		int status_code = get_trans_preview_status.getStatusCode();		

		boolean failed = true;

		if (status_code != 404)
			failed = false;

		assertFalse(failed);
		get_trans_preview_status.releaseConnection();

	}

	/*
	 * Create a new source under /content/colecciones/it_col/contents/it_content/
	 * and checks if it has been created. Also checks if it creates a new preview
	 * transformation
	 */
	public void test_create_source_in_content() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", ""), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", TAGS_REQUEST),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create.getStatusCode());
		post_create.releaseConnection();

		// Edit the source
		PostMethod post_edit = new PostMethod(SLING_URL + COL_URL + col_title + "/"
				+ CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER + "/"
				+ source_title);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_edit = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("file", file),
				new NameValuePair("mimetype", mimetype),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", type), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_edit.setRequestBody(data_edit);
		post_edit.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit.getStatusCode());

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get trans preview
		HttpMethod get_trans_preview = new GetMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/"
				+ SOURCES_FOLDER + "/" + source_title + "/" + TRANSFORMATIONS_FOLDER
				+ "/" + TRANSFORMATION_PREVIEW + "/" + "title");
		try {
			client.executeMethod(get_trans_preview);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String res_trans_title = "";
		try {
			res_trans_title = get_trans_preview.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(TRANSFORMATION_PREVIEW_TITLE, res_trans_title);
		get_trans_preview.releaseConnection();
	}

	public void test_source_finish_prop() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", ""), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", TAGS_REQUEST),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the source
		HttpMethod get_source = new GetMethod(SLING_URL + COL_URL + col_title + "/"
				+ CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER + "/"
				+ source_title);
		try {
			client.executeMethod(get_source);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_source.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(response_body.contains("<finish>1"));
		get_source.releaseConnection();
	}

	/*
	 * Create a new source under /content/colecciones/it_col/sources/ and checks
	 * if it has been created. Also checks if it creates a new preview
	 * transformation
	 */
	public void test_create_source_in_sources() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + SOURCES_FOLDER + "/" + source_title);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", ""), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create.getStatusCode());

		// Edit source
		PostMethod post_edit = new PostMethod(SLING_URL + COL_URL + col_title + "/"
				+ SOURCES_FOLDER + "/" + source_title);
		post_edit.setDoAuthentication(true);

		NameValuePair[] data_edit = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("file", file),
				new NameValuePair("mimetype", mimetype),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", type), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_edit.setRequestBody(data_edit);
		post_edit.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit.getStatusCode());

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the source mime type
		HttpMethod get_trans_preview = new GetMethod(SLING_URL + COL_URL
				+ col_title + "/" + SOURCES_FOLDER + "/" + source_title + "/"
				+ TRANSFORMATIONS_FOLDER + "/" + TRANSFORMATION_PREVIEW + "/" + "title");
		try {
			client.executeMethod(get_trans_preview);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String res_trans_title = "";
		try {
			res_trans_title = get_trans_preview.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(TRANSFORMATION_PREVIEW_TITLE, res_trans_title);
		get_trans_preview.releaseConnection();
	}

	/*
	 * Create a new source under /content/colecciones/it_col/contents/it_content/
	 * and checks if the transformation node has been created
	 */
	public void test_transformation_folder_created() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", ""), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", TAGS_REQUEST),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create.getStatusCode());
		post_create.releaseConnection();

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get trans folder
		HttpMethod get_trans_folder = new GetMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title + "/" + TRANSFORMATIONS_FOLDER + ".html");
		// get_trans_folder.setDoAuthentication(true);
		try {
			client.executeMethod(get_trans_folder);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(200, get_trans_folder.getStatusCode());
		get_trans_folder.releaseConnection();
	}

}