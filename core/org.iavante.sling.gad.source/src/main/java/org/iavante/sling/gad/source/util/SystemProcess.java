/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.gad.source.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Execute a system command and look for given strings in input and error
 * streams.
 */
public class SystemProcess implements Runnable, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger out = LoggerFactory.getLogger(SystemProcess.class);

	/** Process that execute the command. */
	private Process p;

	/** Thread to capture both of the streams. */
	private Thread t;

	/** Command to execute. */
	private String command;

	/** Strings to look for in the streams. */
	private List textSearched;

	/** Matches of element of textSearched in errorStream. */
	private Map foundInErrorstream;

	/** Matches of element of textSearched in inputStream. */
	private Map foundInInputstream;

	/** All results from error stream. */
	private String returnAllFromErrorstream = "";

	/** All results from input stream. */
	private String returnAllFromInputstream = "";

	/** Needed to synchronize both of the streams. */
	private boolean streamErrorReady = false;

	/** False if search a specific string. */
	private boolean returnAll = true;

	// ---------------- Constructor ----------------------
	public SystemProcess() {
	}

	/**
	 * Launch a specified command.
	 * 
	 * @param command
	 *          command to execute
	 * @param texts
	 *          Strings to look for in the streams.
	 * @return Map - key=text, values=matches. null if exception
	 */
	public Map start(String command, List texts) {

		returnAll = false;
		returnAllFromErrorstream = "";
		returnAllFromInputstream = "";

		if (out.isInfoEnabled())
			out.info("start, ini, command -> " + command);

		Map results = new HashMap();

		this.command = command;
		this.textSearched = texts;
		this.t = new Thread(this, "Hebra associated to the command " + this.command);

		// Executing the command.
		try {
			p = Runtime.getRuntime().exec(command);
			if (this.textSearched != null) {
				this.t.start();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}

		if (this.textSearched != null && this.textSearched.size() > 0) {

			if (out.isInfoEnabled())
				out.info("start, processing inputStream");

			// Processing output
			InputStream is = p.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			this.foundInInputstream = this.grep(br, this.textSearched);

			// Close streams
			try {
				br.close();
				is.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			while (!this.streamErrorReady && texts != null) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}

			// results <-- found_in_inputstream
			results.putAll(this.foundInInputstream);

			// results <-- found_in_errorstream
			Set set2 = foundInErrorstream.entrySet();
			Iterator it2 = set2.iterator();
			while (it2.hasNext()) {
				Map.Entry entry = (Map.Entry) it2.next();
				if (results.containsKey(entry.getKey())) { // if key already exists in
					// results, then we add the content
					((List) results.get(entry.getKey())).addAll((List) entry.getValue());
				}
			}
		}

		if (out.isInfoEnabled())
			out.info("start, end");

		return results;
	}

	/***
	 * Returns all out of the command execution.
	 * 
	 * @param command
	 * @return
	 */
	public String start(String command) {

		returnAll = true;
		returnAllFromErrorstream = "";
		returnAllFromInputstream = "";

		if (out.isInfoEnabled())
			out.info("start, ini");

		String res = "";

		this.command = command;
		this.t = new Thread(this, "Hebra associated to the command " + this.command);

		// Executing the command.
		try {
			p = Runtime.getRuntime().exec(command);
			this.t.start();

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}

		// Processing output
		InputStream is = p.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		returnAllFromInputstream = bf2string(br);

		// Close streams
		try {
			br.close();
			is.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		while (!this.streamErrorReady) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}

		res = returnAllFromInputstream + "\n" + returnAllFromErrorstream;

		if (out.isInfoEnabled())
			out.info("start, end");

		return res;
	}

	/*
	 * @see java.lang.Runnable
	 */
	public void run() {

		if (out.isInfoEnabled())
			out.info("start, processing errorStream");

		// Processing output
		InputStream isError = p.getErrorStream();
		BufferedReader brError = new BufferedReader(new InputStreamReader(isError));

		if (returnAll) {
			this.returnAllFromErrorstream = this.bf2string(brError);
		} else {
			this.foundInErrorstream = this.grep(brError, this.textSearched);
		}

		// Close streams
		try {
			brError.close();
			isError.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		this.streamErrorReady = true;
	}

	// ------------ Internal ------------------

	/**
	 * Look for matches of texts in the buffer.
	 * 
	 * @param br
	 *          Buffer where to search
	 * @param texts
	 * @return Map - Key=text, value=lines matched
	 */
	private Map grep(BufferedReader br, List texts) {

		if (out.isInfoEnabled())
			out.info("grep, ini");

		Map map = new HashMap();
		String line = "";

		for (int i = 0; i < texts.size(); i++)
			map.put(texts.get(i), new ArrayList()); // Create map keys

		try {
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < texts.size(); i++) {
					if (line.indexOf((String) texts.get(i)) != -1) {
						((List) map.get(texts.get(i))).add(line); // add the line
					}
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		if (out.isInfoEnabled())
			out.info("grep, end");

		return map;
	}

	/**
	 * From BufferedReader to String.
	 * 
	 * @param br
	 * @return
	 */
	private String bf2string(BufferedReader br) {

		String res = "";

		String line = "";
		try {
			while ((line = br.readLine()) != null) {
				res += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}

}
