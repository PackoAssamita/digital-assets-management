/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.s3backend;

import java.util.Map;

/**
 * Manage S3 service factroy
 * 
 * @scr.property name="service.description" value="IAVANTE - S3backend Factory"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface S3BackendFactory {

	/**
	 * Returns the s3 service configured with your custom properties
	 * 
	 * @param custom_properties
	 * @return
	 */
	public Object getService(Map<String, String> custom_properties);
}
