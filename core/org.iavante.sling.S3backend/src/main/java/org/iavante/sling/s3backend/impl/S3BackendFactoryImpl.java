/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.s3backend.impl;

import java.util.Map;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.s3backend.S3BackendFactory;

/**
 * @scr.component immediate="true"
 * @scr.service interface="org.iavante.sling.s3backend.S3BackendFactory"
 * @scr.property name="service.description"
 *               value="IAVANTE - S3backend Service Factory"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class S3BackendFactoryImpl implements S3BackendFactory {

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(S3BackendFactoryImpl.class);

	/** @scr.reference */
	private PortalSetup portalSetup;

	protected void activate(ComponentContext context) {
	}

	public Object getService(Map<String, String> custom_properties) {
		if (log.isInfoEnabled())
			log.info("Getting custom service");
		Map<String, String> properties = portalSetup
				.get_config_properties("/s3backend");
		S3backendImpl s3_service = new S3backendImpl(properties);
		s3_service.configure(custom_properties);
		return s3_service;
	}
}