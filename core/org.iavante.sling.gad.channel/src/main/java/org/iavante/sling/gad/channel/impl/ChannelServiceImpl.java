/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel.impl;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.version.VersionException;

import org.apache.jackrabbit.value.StringValue;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.commons.json.*;
import org.apache.sling.commons.json.jcr.JsonJcrNode;
import org.iavante.sling.commons.services.FileToolsService;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.ITranscoderService;
import org.iavante.sling.commons.services.JcrTools;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.channel.ChannelService;
import org.iavante.sling.gad.content.ContentTools;
import org.iavante.sling.s3backend.S3BackendFactory;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * gson.fromJson(json, listType); Channel Listener implementation.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Channel listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class ChannelServiceImpl implements ChannelService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private JcrTools jcrTool;

	/** @scr.reference */
	private ITranscoderService transcoderService;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private FileToolsService fileTool;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** @scr.reference */
	private ContentTools contentTool;

	/** Path to channel's schemas. */
	private final String SCHEMAS_PATH = "content/schemas/channel";

	/** Path to channels. */
	private final String DOCUMENT_PATH = "/content/canales";

	/** Unpublished path. */
	private final String UNPUBLISHED_PATH = "/unpublished";

	/** Revised path. */
	private final String REVISED_FOLDER = "/content/catalogo/revisados";

	/** Channel resource type. */
	private final String CHANNEL_RESOURCE_TYPE = "gad/channel";

	/** Revision resource type. */
	private final String REVISIONL_RESOURCE_TYPE = "gad/revision";

	/** Source resource type. */
	private final String SOURCE_RESOURCE_TYPE = "gad/source";

	/** Sources folder. */
	private final String SOURCES_FOLDER = "sources";

	/** Transformations folder. */
	private final String TRANSFORMATIONS_NODE = "transformations";

	/** Property to save the original content path. */
	private final String PATH_PROP = "jcr_path";

	/** Tags property. */
	private final String TAG_PROP = "tags";

	/** Total votes property. */
	private final String NUMBER_VOTES_PROP = "number_votes";

	/** Sum votes property. */
	private final String SUM_VOTES_PROP = "sum_votes";

	/** Type video property. */
	private final String TYPE_VIDEO = "video";

	/** Type image property. */
	private final String TYPE_IMAGE = "image";

	/** State revision published. */
	private final String STATE_PUBLISHED = "published";

	/** State revision unpublished. */
	private final String STATE_UNPUBLISHED = "unpublished";

	/** Sling properties. */
	private Map<String, String> globalProperties = null;

	/** S3 properties. */
	private Map<String, String> s3backendProperties = null;

	/** Default file folder. */
	private String defaultFileFolder;

	/** Distribution server custom property */
	private String DS_CUSTOM_PROPS = "distribution_server_config";

	/** Distribution server custom properties */
	private String DS_CUSTOM_PROPS_FOLDER = "ds_custom_props";

	/** sling url. */
	private String slingUrl;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);

			// Get properties
			this.globalProperties = portalSetup.get_config_properties("/sling");
			this.s3backendProperties = portalSetup
					.get_config_properties("/s3backend");

			this.defaultFileFolder = this.globalProperties.get("default_file_folder");
			this.slingUrl = this.globalProperties.get("sling_url");

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED
						| Event.PROPERTY_REMOVED, DOCUMENT_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();

			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));

					if (addedNode.hasProperty("sling:resourceType")) {

						// a new channel is added
						if (addedNode.getProperty("sling:resourceType").getValue()
								.getString().equals(CHANNEL_RESOURCE_TYPE)) {
							if (log.isInfoEnabled())
								log.info("Channel Added " + addedNode.toString());
							createSchema(addedNode);

							// Custom properties
							if ((addedNode.hasProperty(DS_CUSTOM_PROPS))
									&& (!"".equals(addedNode.getProperty(DS_CUSTOM_PROPS)
											.getValue().getString()))) {
								if (log.isInfoEnabled())
									log.info("ds custom properties");
								String custom_props = addedNode.getProperty(DS_CUSTOM_PROPS)
										.getValue().getString();
								try {
									JSONObject json_props = new JSONObject(custom_props);
									Iterator props = json_props.keys();
									Node ds_custom_props_folder = addedNode
											.getNode(DS_CUSTOM_PROPS_FOLDER);
									while (props.hasNext()) {
										String prop = props.next().toString();
										String value = json_props.get(prop).toString();
										ds_custom_props_folder.setProperty(prop, value);
										ds_custom_props_folder.save();
										if (log.isInfoEnabled())
											log.info("ds custom folder saved");
									}
									ds_custom_props_folder.setProperty("edited", "1");
									ds_custom_props_folder.save();

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}

						// a new revision is added
						else if (addedNode.getProperty("sling:resourceType").getValue()
								.getString().equals(REVISIONL_RESOURCE_TYPE)) {

							String destPath = addedNode.getPath();

							// Revision unpublished
							if (destPath.contains(UNPUBLISHED_PATH)) {

								if (log.isInfoEnabled())
									log.info("Revision Unpublished " + addedNode.toString());

								String refContentPath = addedNode.getProperty(PATH_PROP)
										.getString();
								Node refContentNode = session.getRootNode().getNode(
										refContentPath.substring(1));

								// Update the log node with the new state
								gadContentTool.updateLogNode(refContentNode, STATE_UNPUBLISHED,
										destPath);
								session.save();

								if (log.isInfoEnabled())
									log.info("New status: " + STATE_UNPUBLISHED + " Ref: "
											+ refContentPath);
							}

							// Revision published
							else {
								if (log.isInfoEnabled())
									log.info("Revision Added " + addedNode.toString());
								String refContentPath = addedNode.getProperty(PATH_PROP)
										.getString();
								Node refContentNode = null;

								// Update the log node with the new state
								if (session.getRootNode().hasNode(refContentPath.substring(1))) {
									refContentNode = session.getRootNode().getNode(
											refContentPath.substring(1));
									String addednodeName = addedNode.getName().toString();
									Node contentInCatalog = session.getRootNode().getNode(
											REVISED_FOLDER.substring(1) + "/" + addednodeName);
									gadContentTool.updateLogNode(refContentNode, STATE_PUBLISHED,
											destPath);
									gadContentTool.updateLogNode(contentInCatalog,
											STATE_PUBLISHED, destPath);
									session.save();
									if (log.isInfoEnabled())
										log.info("New status: " + STATE_PUBLISHED + " Ref: "
												+ refContentPath);
								}

								if (addedNode.hasProperty(TAG_PROP)) {

									Value[] tags = addedNode.getProperty(TAG_PROP).getValues();
									Value[] tagsNormal = new Value[tags.length];
									Node channelNode = addedNode.getParent().getParent();
									for (int i = 0; i < tags.length; i++) {
										String tagNormal = gadContentTool.normalizeTag(tags[i]
												.getString());
										tagsNormal[i] = new StringValue(tagNormal);
										gadContentTool.updateTags(channelNode, tagsNormal[i]
												.getString());
										if (log.isInfoEnabled())
											log.info("Update tag: " + tagNormal);
									}

									addedNode.setProperty(TAG_PROP, tagsNormal);
									addedNode.save();
								}

								addedNode.setProperty("space", "channel");
								addedNode.save();
								if (log.isInfoEnabled())
									log.info("Space "
											+ addedNode.getProperty("space").getValue().getString());

								// Publication postprocessor
								publication_postprocess(addedNode);
								if (log.isInfoEnabled())
									log.info("Publication postprocessing finished");

								// Set finish property to 1
								addedNode.setProperty("finish", "1");
								Long now = new Date().getTime();
								
								// Set pub date in millis and formated
								addedNode.setProperty("pubDate", now);
								DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS:SZ");
							    Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(now);
								String pubDate = formatter.format(calendar.getTime());
								addedNode.setProperty("pubDate_formated", pubDate);
								addedNode.save();
								if (log.isInfoEnabled())
									log.info("Session saved");
							}
						}

					}

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			} else if (event.getType() == Event.PROPERTY_REMOVED) {

				try {
					String nodePath = event.getPath().substring(1,
							event.getPath().lastIndexOf("/"));
					Node changedNode = session.getRootNode().getNode(nodePath);

					String vote = "addvote";
					String votearray = "votes";

					if (changedNode.hasProperty(vote)
							&& changedNode.hasProperty(votearray)
							&& changedNode.getProperty("sling:resourceType").getValue()
									.getString().equals(REVISIONL_RESOURCE_TYPE)) {

						Property pvote = changedNode.getProperty(vote);
						Property parray = changedNode.getProperty(votearray);
						if (parray.getDefinition().isMultiple()) {

							// Copy all old values to new array
							Value[] values = Arrays.copyOf(parray.getValues(), parray
									.getValues().length + 1);
							// Add new value to new array
							values[values.length - 1] = pvote.getValue();
							// Set new array to property
							parray.setValue(values);

							int i = parray.getValues().length - 1;
							if (log.isInfoEnabled())
								log.info("onEvent, " + votearray + "[" + i + "]="
										+ parray.getValues()[i].getString() + " added");

							changedNode.save();

							changedNode.setProperty(NUMBER_VOTES_PROP, contentTool
									.number_of_votes(changedNode));
							changedNode.setProperty(SUM_VOTES_PROP, contentTool
									.number_of_votes_points(changedNode));
						}
						// Remove 'vote' property
						if (changedNode.hasProperty(vote)) {
							changedNode.setProperty(vote, (Value) null);
						}
					}
					session.save();

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			} // end 'else if'

		}
	}

	// ------------- Internal --------------------

	/**
	 * Get the channel properties and checks if the revision needs to be
	 * converted. It that case it sends a conversion task.
	 * 
	 * @param Node
	 */
	private void publication_postprocess(Node addedNode) {

		Node channelNode = null;
		String distributionFormatsVideo = "";
		String distributionFormatsImage = "";
		String distributionFormats = "";

		String externalStorageUrl = "";
		String externalStorageServer = "";

		try {
			channelNode = addedNode.getParent().getParent();
			distributionFormatsVideo = channelNode.getProperty(
					"distribution_format_video").getValue().getString();
			distributionFormatsImage = channelNode.getProperty(
					"distribution_format_image").getValue().getString();
		} catch (ItemNotFoundException e1) {
			e1.printStackTrace();
		} catch (AccessDeniedException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		// It takes the S3 storage by default. We have to check the distribution
		// service selected in the channel
		// and gives the apropiate value to external_storage variable
		externalStorageServer = "s3";

		try {
			if ((channelNode.hasProperty("distribution_server"))
					&& (!"".equals(channelNode.getProperty("distribution_server")
							.getValue().getString()))) {
				externalStorageServer = channelNode.getProperty("distribution_server")
						.getValue().getString();
			}
		} catch (ValueFormatException e1) {
			e1.printStackTrace();
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (PathNotFoundException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		externalStorageUrl = this.s3backendProperties.get("url")
				+ this.s3backendProperties.get("bucket");

		try {
			// Iter its sources
			NodeIterator sources = addedNode.getNode(SOURCES_FOLDER).getNodes();
			while (sources.hasNext()) {
				Node source = (Node) sources.next();
				if (source.hasNode(TRANSFORMATIONS_NODE)) {

					String sourceType = source.getProperty("type").getValue().getString();
					if (TYPE_VIDEO.compareTo(sourceType) == 0) {
						distributionFormats = distributionFormatsVideo;
					} else if (TYPE_IMAGE.compareTo(sourceType) == 0) {
						distributionFormats = distributionFormatsImage;
					} else {
						distributionFormats = null;
					}

					if (distributionFormats != null
							&& !source.getNode(TRANSFORMATIONS_NODE).hasNode(
									distributionFormats)) {

						String[] formats = distributionFormats.trim().split(",");
						String format = "";

						String sourceFilename = source.getProperty("file").getValue()
								.getString();
						String sourceLocation = defaultFileFolder + "/" + sourceFilename;

						for (int i = 0; i < formats.length; i++) {
							format = formats[i];

							if (!source.hasNode(TRANSFORMATIONS_NODE + "/" + format)) {
								if (log.isInfoEnabled())
									log.info("Creating transformation format: " + format);
								Map<String, String> conversionTypeMap = transcoderService
										.getConversion(format);
								String conversionExtension = conversionTypeMap
										.get("output_extension");
								String conversionMime = conversionTypeMap
										.get("output_mimetype");

								String transformationFilename = fileTool
										.genTransformationFilename(sourceFilename, format,
												conversionExtension);

								String targetLocation = defaultFileFolder + "/"
										+ transformationFilename;

								String transformationPath = gadContentTool
										.createTransformation(source, format, conversionMime);
								String notificationUrl = this.slingUrl + transformationPath;

								String ds_custom_props = "";
								if (channelNode.hasNode("ds_custom_props")
										&& channelNode.getNode("ds_custom_props").hasProperty(
												"edited")
										&& "1".equals(channelNode.getNode("ds_custom_props")
												.getProperty("edited").getString())) {
									ds_custom_props = stringify(channelNode
											.getNode("ds_custom_props"));
								}

								int statusCode = transcoderService.sendConversionTask3(format,
										sourceLocation, targetLocation, notificationUrl,
										externalStorageServer, externalStorageUrl, null,
										ds_custom_props);
								if (log.isInfoEnabled())
									log.info("Distribution format: " + format + " Source: "
											+ sourceLocation + " Target: " + targetLocation + " "
											+ " Notification: " + notificationUrl
											+ " External Storage Url: " + externalStorageUrl
											+ " ds_custom_props: " + ds_custom_props + " Response: "
											+ statusCode);

								Node transformationNode = source.getNode(TRANSFORMATIONS_NODE)
										.getNode(format);
								transformationNode.setProperty("notify_status_code", Integer
										.toString(statusCode));
								transformationNode.save();
							}

						}
					}
				}
			}

		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add the schema to the addedNode.
	 * 
	 * @param Node
	 */
	private void createSchema(Node addedNode) {

		Node schemaNode = null;
		String schema = null;

		// Get schema property
		try {
			schema = addedNode.getProperty("schema").getString();
		} catch (ValueFormatException e1) {
			e1.printStackTrace();
		} catch (PathNotFoundException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("Node schema: " + schema);
		// Get schema node
		try {
			schemaNode = session.getRootNode().getNode(SCHEMAS_PATH + "/" + schema);
		} catch (PathNotFoundException e) {
			if (log.isInfoEnabled())
				log.info("Schema not found, path:" + SCHEMAS_PATH + "/" + schema);
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		try {
			NodeIterator nodeIterator = schemaNode.getNodes();
			while (nodeIterator.hasNext()) {
				Node node = nodeIterator.nextNode();
				jcrTool.copy(node, addedNode, null);
			}
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		// Save session
		try {
			addedNode.save();
			if (log.isInfoEnabled())
				log.info("GAD Schema asocciated");
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (InvalidItemStateException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (NoSuchNodeTypeException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a json with all node params
	 * 
	 * @param node
	 * @return
	 */
	private String stringify(Node node) {
		String res = "";

		try {
			Set<String> ignoredProperties = new HashSet<String>();
			ignoredProperties.add("jcr:path");
			ignoredProperties.add("jcr:name");
			ignoredProperties.add("sling:resourceType");
			ignoredProperties.add("jcr:created");
			ignoredProperties.add("jcr:primaryType");
			ignoredProperties.add("jcr:lastModifiedBy");
			ignoredProperties.add("jcr:createdBy");
			ignoredProperties.add("jcr:lastModified");

			JsonJcrNode jsonJcrNode = new JsonJcrNode(node, ignoredProperties);
			res = jsonJcrNode.toString();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return res;
	}

}
