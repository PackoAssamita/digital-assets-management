/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel.impl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.version.VersionException;

import org.apache.jackrabbit.value.StringValue;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.commons.json.*;
import org.iavante.sling.commons.services.FileToolsService;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.ITranscoderService;
import org.iavante.sling.commons.services.JcrTools;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.channel.ChannelService;
import org.iavante.sling.gad.channel.MultimediaAssetsService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MultimediaAssets Listener implementation.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="MultimediaAssets listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class MultimediaAssetsServiceImpl implements MultimediaAssetsService,
		EventListener, Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private JcrTools jcrTool;

	/** @scr.reference */
	private ITranscoderService transcoderService;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private FileToolsService fileTool;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** Path to channel's schemas. */
	private final String SCHEMAS_PATH = "content/schemas/channel";

	/** Path to channels. */
	private final String DOCUMENT_PATH = "/content/canales";

	/** Unpublished path. */
	private final String UNPUBLISHED_PATH = "/unpublished";

	/** Revised path. */
	private final String REVISED_FOLDER = "/content/catalogo/revisados";

	/** Channel resource type. */
	private final String CHANNEL_RESOURCE_TYPE = "gad/channel";

	/** Revision resource type. */
	private final String REVISIONL_RESOURCE_TYPE = "gad/revision";

	/** Source resource type. */
	private final String SOURCE_RESOURCE_TYPE = "gad/source";

	/** Sources folder. */
	private final String SOURCES_FOLDER = "sources";

	/** Transformations folder. */
	private final String TRANSFORMATIONS_NODE = "transformations";

	/** Property to save the original content path. */
	private final String PATH_PROP = "jcr_path";

	/** Tags property. */
	private final String TAG_PROP = "tags";

	/** Tags property. */
	private final String TYPE_VIDEO = "video";

	/** Tags property. */
	private final String TYPE_IMAGE = "image";

	/** State revision published. */
	private final String STATE_PUBLISHED = "published";

	/** State revision unpublished. */
	private final String STATE_UNPUBLISHED = "unpublished";

	/** Sling properties. */
	private Map<String, String> globalProperties = null;

	/** S3 properties. */
	private Map<String, String> s3backendProperties = null;

	/** Default file folder. */
	private String defaultFileFolder;

	/** Distribution server custom property */
	private String DS_CUSTOM_PROPS = "distribution_server_config";

	/** Distribution server custom properties */
	private String DS_CUSTOM_PROPS_FOLDER = "ds_custom_props";

	/** sling url. */
	private String slingUrl;

	/** External storage. */
	private String externalStorageServer;

	/** External storage. */
	private String externalStorageUrl;
	
	/** Channel */
	private String channel = "";
	
	private final String TRANSFORMATION_PREVIEW_FLV = "preview";
	private final String TRANSFORMATION_PREVIEW_OGV = "preview_ogv";
	private final String TRANSFORMATION_PREVIEW_IMAGE = "image_preview";
	private final String PREVIEW_MIMETYPE_FLV = "video/x-flv";
	private final String PREVIEW_MIMETYPE_OGV = "video/ogg";
	private final String PREVIEW_MIMETYPE_IMAGE = "image/jpeg";
	private final String PREVIEW_MIMETYPE_GIF = "image/gif";
	private final String TRANSFORMATION_PREVIEW_THUMBNAIL = "thumbnail_preview";
	private final int NUM_THUMBNAILS = 3;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);

			// Get properties
			this.globalProperties = portalSetup.get_config_properties("/sling");
			this.s3backendProperties = portalSetup
					.get_config_properties("/s3backend");

			this.defaultFileFolder = this.globalProperties.get("default_file_folder");
			this.slingUrl = this.globalProperties.get("sling_url");

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED
						| Event.PROPERTY_REMOVED | Event.PROPERTY_ADDED, DOCUMENT_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			
			Node eventNode = null;
			boolean isMultimediaAssets = false;
			String propPath = "";

			try {
				propPath = event.getPath();
				
				if(log.isInfoEnabled()) log.info("onEvent, event: " + propPath);

				String[] pathSplit = event.getPath().split("/"); // multimedia_assets
				if (pathSplit.length > 4 && "content".equals(pathSplit[1])
						&& "canales".equals(pathSplit[2])
						&& "multimedia_assets".equals(pathSplit[4])) {
					isMultimediaAssets = true;
					this.channel = pathSplit[3];
				}

			} catch (RepositoryException e1) {
				e1.printStackTrace();
			}

			if (event.getType() == Event.NODE_ADDED) {

				try {

					eventNode = session.getRootNode().getNode(
							event.getPath().substring(1));
					
					if (eventNode.hasProperty("sling:resourceType")
							&& eventNode.getProperty("sling:resourceType").getValue()
							.getString().equals(SOURCE_RESOURCE_TYPE)
							&& isMultimediaAssets) {

						// Add transformation folder
						eventNode.addNode(TRANSFORMATIONS_NODE);
						eventNode.setProperty("finish", "1");
						eventNode.save();
						if (log.isInfoEnabled())
							log.info("onEvent, new source node: " + eventNode.getPath());
					}
				} catch (RepositoryException e) {
					e.printStackTrace();
				}

			} else if (event.getType()==Event.PROPERTY_REMOVED
					&& propPath.endsWith("jcr:lastModified")	) { //

				try {
					
					int pos = propPath.lastIndexOf('/');
					String nodePath = propPath.substring(0, pos);
					eventNode = session.getRootNode().getNode(nodePath.substring(1));

					if (eventNode.hasProperty("sling:resourceType")
							&& eventNode.getProperty("sling:resourceType").getString()
									.equals(SOURCE_RESOURCE_TYPE)
							&& eventNode.hasProperty("file")
							&& !"".equals(eventNode.getProperty("file").getString())) {

						String file = eventNode.getProperty("file").getString();

						if (log.isInfoEnabled())
							log.info("onEvent, Edit source, file: " + file);

						processSource(eventNode);
					}

				} catch (PathNotFoundException e) {
					e.printStackTrace();
				} catch (RepositoryException e) {
					e.printStackTrace();
				}

			} // end 'else if'
		}
	}

	// -------------------- Internal ----------------------

	/**
	 * Process the source. Checks if it needs conversion.
	 * 
	 * @param source
	 * @throws RepositoryException
	 * @throws PathNotFoundException
	 * @throws ValueFormatException
	 */
	private void processSource(Node source) throws ValueFormatException,
			PathNotFoundException, RepositoryException {

		String sourceMimetype = "";

		if (source.hasProperty("type")) {
			sourceMimetype = source.getProperty("type").getString();
		}

		if (log.isInfoEnabled())
			log.info("processSource, type: " + sourceMimetype);

		// S3 default
		this.externalStorageServer = "S3";
		this.externalStorageUrl = this.s3backendProperties.get("url")
				+ this.s3backendProperties.get("bucket");

		Node channelNode = session.getRootNode().getNode(
				DOCUMENT_PATH.substring(1) + "/" + this.channel);

		// Video
		if (sourceMimetype.contains("video")) {

			String previewNodePathFlv = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_FLV, PREVIEW_MIMETYPE_FLV);
			String previewNodePathOgv = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_OGV, PREVIEW_MIMETYPE_OGV);
			Node previewNodeFlv = session.getRootNode().getNode(previewNodePathFlv.substring(1));
			Node previewNodeOgv = session.getRootNode().getNode(previewNodePathOgv.substring(1));

			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePathFlv);
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePathOgv);

			// Get file name
			String fileName = source.getProperty("file").getString();

			String outputFileNameFlv = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_FLV, "flv");
			String outputFileNameOgv = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_OGV, "ogv");

			String inputPath = this.defaultFileFolder + "/" + fileName;
			if (log.isInfoEnabled())
				log.info("processSource, Input path: " + inputPath);
			String outputPathFlv = this.defaultFileFolder + "/" + outputFileNameFlv;
			String outputPathOgv = this.defaultFileFolder + "/" + outputFileNameOgv;
			String notificationUrlFlv = this.slingUrl + previewNodePathFlv;
			String notificationUrlOgv = this.slingUrl + previewNodePathOgv;

			if (log.isInfoEnabled())
				log.info("processSource, Conversion params: " + inputPath + " "
						+ outputPathFlv + " " + notificationUrlFlv + " "
						+ externalStorageServer + " " + externalStorageUrl);
			int statusCodeFlv = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_FLV, inputPath, outputPathFlv,
					notificationUrlFlv, externalStorageServer, externalStorageUrl);

			if (log.isInfoEnabled())
				log.info("processSource, Conversion params: " + inputPath + " "
						+ outputPathOgv + " " + notificationUrlOgv + " "
						+ externalStorageServer + " " + externalStorageUrl);
			int statusCodeOgv = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_OGV, inputPath, outputPathOgv,
					notificationUrlOgv, externalStorageServer, externalStorageUrl);

			if (log.isInfoEnabled())
				log.info("processSource, Notification sent. Status code: "
						+ statusCodeFlv);
			if (log.isInfoEnabled())
				log.info("processSource, Notification sent. Status code: "
						+ statusCodeOgv);

			update_trans_prew_notification(previewNodeFlv, statusCodeFlv);
			update_trans_prew_notification(previewNodeOgv, statusCodeOgv);
			
			
			// -----------------------------------
			// Automatic thumbnail generation
			// -----------------------------------
			if (source.hasProperty("lengths")) {

				String[] thumbFilesPath = new String[NUM_THUMBNAILS];

				// Calc hh:mm:ss when snapshots will be done
				String duration = source.getProperty("lengths").getString().trim();
				String[] hhmmss = duration.split(":");
				int durationInSeconds = Integer.parseInt(hhmmss[0]) * 3600
						+ Integer.parseInt(hhmmss[1]) * 60
						+ Integer.parseInt(hhmmss[2].substring(0, 2));
				log.info("processSource, durationInSeconds=" + durationInSeconds);

				String[] timeSnapshot = new String[NUM_THUMBNAILS];
				timeSnapshot[0] = "00:00:01";
				for (int i = 1; i < NUM_THUMBNAILS; i++) {
					int sss = durationInSeconds / NUM_THUMBNAILS * i;
					int hh = sss / 3600;
					int mm = (sss - hh * 3600) / 60;
					int ss = sss - hh * 3600 - mm * 60;
					timeSnapshot[i] = hh + ":" + mm + ":" + ss;
					log.info("processSource, timeSnapshot[" + i + "] = "
							+ timeSnapshot[i]);
				}

				for (int i = 0; i < timeSnapshot.length; i++) {

					String previewNodePath = gadContentTool.createTransformation(source,
							TRANSFORMATION_PREVIEW_THUMBNAIL + "_" + i,
							PREVIEW_MIMETYPE_IMAGE);
					Node previewNode = session.getRootNode().getNode(previewNodePath.substring(1));

					if (log.isInfoEnabled())
						log.info("processSource, Preview path thumbnail: "
								+ previewNodePath);

					String outputFileName = fileTool.genTransformationFilename(fileName,
							PREVIEW_MIMETYPE_IMAGE + "_" + i, "jpg");
					String outputPath = this.defaultFileFolder + "/" + outputFileName;
					thumbFilesPath[i] = outputPath;
					String notificationUrl = this.slingUrl + previewNodePath;

					if (log.isInfoEnabled())
						log.info("processSource, Conversion params thumbnail: " + inputPath
								+ " " + outputPath + " " + notificationUrl + " "
								+ externalStorageServer + " " + externalStorageUrl);

					int statusCode = transcoderService.sendConversionTask2(
							TRANSFORMATION_PREVIEW_THUMBNAIL, inputPath, outputPath,
							notificationUrl, externalStorageServer, externalStorageUrl,
							" -ss " + timeSnapshot[i] + " -s 470x320 -f image2 -vframes 1 ");

					if (log.isInfoEnabled())
						log
								.info("processSource, Notification thumbnail sent. Status code: "
										+ statusCode);
					update_trans_prew_notification(previewNode, statusCode);

				} // end for

				// -------------------------------------------------
				// Generate the gif animation (The last thumbnail)
				// -------------------------------------------------
				String previewNodePath = gadContentTool.createTransformation(source,
						TRANSFORMATION_PREVIEW_THUMBNAIL + "_" + NUM_THUMBNAILS,
						PREVIEW_MIMETYPE_GIF);
				Node previewNode = session.getRootNode().getNode(previewNodePath.substring(1));

				if (log.isInfoEnabled())
					log.info("processSource, Preview path thumbnail: " + previewNodePath);

				String outputFileName = fileTool.genTransformationFilename(fileName,
						PREVIEW_MIMETYPE_GIF + "_" + NUM_THUMBNAILS, "gif");
				String outputPath = this.defaultFileFolder + "/" + outputFileName;
				String notificationUrl = this.slingUrl + previewNodePath;

				if (log.isInfoEnabled())
					log.info("processSource, Conversion params thumbnail: " + inputPath
							+ " " + outputPath + " " + notificationUrl + " "
							+ externalStorageServer + " " + externalStorageUrl);

				// execute full command
				String params = "custom /convert -delay 100 -size 235x160";
				for (int i = 0; i < thumbFilesPath.length; i++) {
					params += " -page +0+0 " + thumbFilesPath[i];
				}
				params += " -loop 0 " + outputPath;

				int statusCode = transcoderService.sendConversionTask2(
						TRANSFORMATION_PREVIEW_IMAGE, inputPath, outputPath,
						notificationUrl, externalStorageServer, externalStorageUrl, params);

				if (log.isInfoEnabled())
					log.info("processSource, Notification thumbnail sent. Status code: "
							+ statusCode);
				update_trans_prew_notification(previewNode, statusCode);
			}
		}

		// Image
		else if (sourceMimetype.contains("image")
				|| sourceMimetype.contains("application")) {

			String previewNodePath = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_IMAGE, sourceMimetype);
			Node previewNode = session.getRootNode().getNode(previewNodePath.substring(1));
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePath);

			// Get file name
			String fileName = source.getProperty("file").getString();

			String outputFileName = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_IMAGE, "jpg");

			String inputPath = this.defaultFileFolder + "/" + fileName;
			if (log.isInfoEnabled())
				log.info("processSource, Input path: " + inputPath);
			String outputPath = this.defaultFileFolder + "/" + outputFileName;
			String notificationUrl = this.slingUrl + previewNodePath;

			// Send conversion task
			int statusCode = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_IMAGE, inputPath, outputPath, notificationUrl,
					externalStorageServer, externalStorageUrl);
			if (log.isInfoEnabled())
				log
						.info("processSource, Notification sent. Status code: "
								+ statusCode);

			update_trans_prew_notification(previewNode, statusCode);
		}

		if (log.isInfoEnabled())
			log.info("processSource, Processs source finished");
	}

	/**
	 * Update the preview node with the notification status code.
	 * 
	 * @param previewNode
	 * @param status
	 */
	private void update_trans_prew_notification(Node previewNode, int status) {
		try {
			previewNode.setProperty("notify_status_code", Integer.toString(status));
			previewNode.save();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
}
