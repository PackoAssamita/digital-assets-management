<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<%@ page trimDirectiveWhitespaces="true"%>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%><%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String OFFSET = "offset";
String ITEMS = "items";

NodeIterator it = currentNode.getNodes();
%>

<count><%= Long.toString(it.getSize())%></count>

<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}
%>

<channels>
<%
while (it.hasNext() && count != 0) {
Node node = it.nextNode(); 
%>
<channel>   
    <title><%if(node.hasProperty("title")) { %><%=node.getProperty("title").getValue().getString()%><%}%></title>
	<path><%=node.getPath() %></path>
	<resourceType><%if(node.hasProperty("sling:resourceType")){%>
	<%=node.getProperty("sling:resourceType").getValue().getString()%>
	<%}%></resourceType>
</channel>
<%count--; %>
<%}%>
</channels>
</Result>