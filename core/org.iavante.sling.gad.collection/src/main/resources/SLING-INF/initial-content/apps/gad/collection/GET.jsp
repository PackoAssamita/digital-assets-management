<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<%@ page trimDirectiveWhitespaces="true"%>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.iavante.sling.commons.services.LDAPConnector"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, org.apache.sling.api.resource.Resource"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("subtitle");
	etiquetas.add("schemas");	
	etiquetas.add("description");
	etiquetas.add("extern_storage");
	etiquetas.add("author");
	etiquetas.add("picture");
%>

<% for(int i=0; i<etiquetas.size(); i++){
	String tagName = (String) etiquetas.get(i);%>
<<%=tagName%>><%if(currentNode.hasProperty(tagName)){%><%=currentNode.getProperty(tagName).getValue().getString()%><%}%></<%=tagName%>>
<%}%>
<% if(currentNode.hasProperty("jcr:created")) { %>
<created><%=currentNode.getProperty("jcr:created").getValue().getString()%></created>
<%}%>
<% if(currentNode.hasProperty("jcr:createdBy")) { %>
<createdBy><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%></createdBy>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModified")) { %>
<lastModified><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%></lastModified>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %>
<lastModifiedBy><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%></lastModifiedBy>
<%}%>
<path><%=currentNode.getPath()%></path>
<channels_allowed>
<%	
ArrayList<String> channels = sling.getService(LDAPConnector.class).getAuthorizedChannels(request,response);
Iterator<String> it = channels.iterator();
while(it.hasNext()){%>
  <channel><%=it.next()%></channel>
<%}%>
</channels_allowed>
</Result>