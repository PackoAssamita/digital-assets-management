/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.collection.impl;

import java.io.Serializable;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.version.VersionException;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.JcrTools;
import org.iavante.sling.gad.collection.CollectionService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Collection Listener implementation
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Collection listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class CollectionServiceImpl implements CollectionService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private JcrTools jcrTool;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(CollectionServiceImpl.class);

	/** Schema's path. */
	private String schemasPath = "content/schemas/collection";

	/** Document path. */
	private String documentPath = "/content/colecciones";

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED,
						documentPath, false, null, types, false);
			}
		} catch (Exception e) {
			log("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));
					log("GAD: Collection Added " + addedNode.toString());
					createSchema(addedNode);

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// --------------- Internal ------------------

	/**
	 * Add the schema to the addedNode.
	 * 
	 * @param addedNode
	 */
	private void createSchema(Node addedNode) {

		Node schemaNode = null;
		String schema = null;

		// Get schema property
		try {
			schema = addedNode.getProperty("schema").getString();
		} catch (ValueFormatException e1) {
			e1.printStackTrace();
		} catch (PathNotFoundException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		log("GAD Node schema: " + schema);
		// Get schema node
		try {
			schemaNode = session.getRootNode().getNode(
					this.schemasPath + "/" + schema);
		} catch (PathNotFoundException e) {
			log("GAD Schema not found, path:" + this.schemasPath + "/" + schema);
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		try {
			NodeIterator nodeIterator = schemaNode.getNodes();
			while (nodeIterator.hasNext()) {
				Node node = nodeIterator.nextNode();
				jcrTool.copy(node, addedNode, null);
			}
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		// Save session
		try {
			session.save();
			log("GAD Esquema bound");
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (InvalidItemStateException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (NoSuchNodeTypeException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
}
