/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.collection;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test content creation and schema association
 */
public class CollectionServiceTestIT
		extends TestCase {

	private String COLLECTION_URL = "/content/colecciones/";

	private String title;
	private String subtitle;
	private String schema;
	private String slug;
	private String extern_storage;
	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";
	private String CONTENTS_FOLDER = "contents";
	private String SOURCES_FOLDER = "sources";
	private String TAGS_FOLDER = "tags";
	private String CONTENTS_RESOURCE_TYPE = "gad/contents";
	private String SOURCES_RESOURCE_TYPE = "gad/sources";
	private String TAGS_RESOURCE_TYPE = "gad/tags";

	HttpClient client;

	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}

		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		title = "IT col";
		schema = "default";
		slug = "it_col";
		subtitle = "subtitle it";
		extern_storage = "on";

		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		PostMethod post_create = new PostMethod(SLING_URL + COLLECTION_URL + slug);

		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", title), new NameValuePair("schema", schema),
				new NameValuePair("subtitle", subtitle),
				new NameValuePair("extern_storage", extern_storage),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();
	}

	protected void tearDown() {

		// Delete the content
		PostMethod post_delete = new PostMethod(SLING_URL + COLLECTION_URL + slug);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		post_delete.releaseConnection();
	}

	public void test_getContentsFolder() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the content
		HttpMethod get_col_contents = new GetMethod(SLING_URL + COLLECTION_URL
				+ slug + "/" + CONTENTS_FOLDER + "/" + "sling:resourceType");
		try {
			client.executeMethod(get_col_contents);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_col_contents.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, CONTENTS_RESOURCE_TYPE);

	}

	public void test_getSourcesFolder() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String response_body = "";
		HttpMethod get_col_sources = new GetMethod(SLING_URL + COLLECTION_URL
				+ slug + "/" + SOURCES_FOLDER + "/" + "sling:resourceType");
		try {
			client.executeMethod(get_col_sources);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {
			response_body = get_col_sources.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, SOURCES_RESOURCE_TYPE);
		get_col_sources.releaseConnection();
	}

	public void test_getTagsFolder() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String response_body = "";
		HttpMethod get_col_tags = new GetMethod(SLING_URL + COLLECTION_URL + slug
				+ "/" + TAGS_FOLDER + "/" + "sling:resourceType");
		try {
			client.executeMethod(get_col_tags);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {
			response_body = get_col_tags.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, TAGS_RESOURCE_TYPE);
		get_col_tags.releaseConnection();
	}
}