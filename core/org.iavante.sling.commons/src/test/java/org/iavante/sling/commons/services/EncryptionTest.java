/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services;


import java.io.IOException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.iavante.sling.commons.services.impl.EncryptionServiceImpl;

import junit.framework.TestCase;


/**
 * Test validation filter
 */
public class EncryptionTest extends TestCase {

	private EncryptionServiceImpl enc;
	
	protected void setUp() {	
		try {
			enc = new EncryptionServiceImpl();
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		assertEquals(enc.getRSA_Key_Size(), 2048);
		assertEquals(enc.getEncoding(), "UTF-8");
    }
	
    protected void tearDown() {
    }

    public void testEncryptAndDecryptMessage() {
    	// With default encoding
    	String message = "test_encrypt_and_decrypt_message   -> LOL Characters ;) ñ@áéíóú€";
    	String encrypted;
    	
		try {
			encrypted = enc.encryptMessage(message);
			assertEquals(enc.decryptMessage(encrypted), message);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
    	
 		// With public key
		try {
	    	encrypted = enc.encryptMessage(message, enc.getPublicKey());
	    	assertEquals(enc.decryptMessage(encrypted), message);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	
    	// With other encoding
		try {
			encrypted = enc.encryptMessage(message, enc.getPublicKey(), "ISO-8859-1");
	    	assertEquals(enc.decryptMessage(encrypted, "ISO-8859-1"), message);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		
		// With a key public as string
		try {
			encrypted = enc.encryptMessage(message, enc.getPublicKeyAsString(null));
	    	assertEquals(enc.decryptMessage(encrypted), message);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		
		
		// With a key public as string and a specific encoding
		try {
			encrypted = enc.encryptMessage(message, enc.getPublicKeyAsString("ISO-8859-1"), "ISO-8859-1");
	    	assertEquals(enc.decryptMessage(encrypted,"ISO-8859-1"), message);
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
		
		
    	 	
    	// Log results
 		System.out.println("Message encrypted and decrypted correctly");
    }
}