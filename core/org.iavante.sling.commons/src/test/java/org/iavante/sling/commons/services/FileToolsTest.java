/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import junit.framework.TestCase;

import org.iavante.sling.commons.services.impl.FileToolsServiceImpl;

public class FileToolsTest extends TestCase {
	
	FileToolsServiceImpl file_tools;
	String file_name;
	String suffix;
	
	protected void setUp() {		
		file_tools = new FileToolsServiceImpl();
		file_name = "test.mpeg";
		suffix = "testformat";
	}
	
    protected void tearDown() {
    }
    
    public void test_genTransformationFilename() {    
    	
    	String input_extension = file_name.split("\\.")[1];
    	
    	String output_extension = "flv";
    	String output_file_name = file_tools.genTransformationFilename(file_name, suffix ,output_extension);

    	assertTrue(output_file_name.endsWith(output_extension));
    	assertTrue(output_file_name.startsWith(file_name.replace(".", "_")));   
    	
    	output_extension = "3gp";
    	output_file_name = file_tools.genTransformationFilename(file_name, suffix, output_extension);
    	
    	assertTrue(output_file_name.endsWith(output_extension));
    	assertTrue(output_file_name.startsWith(file_name.replace(".", "_")));   
    	
    	output_extension = "";
    	output_file_name = file_tools.genTransformationFilename(file_name, suffix, output_extension);

    	assertTrue(output_file_name.endsWith(input_extension));
    	assertTrue(output_file_name.startsWith(file_name.replace(".", "_")));
    }
    
    public void test_inputStream2file() {    
    	
    	String file_path = "out_test.txt";
    	String out_file_path = "out_test_converted.txt";
    	
    	// Create a file    	
    	try {
   		    FileWriter fstream = new FileWriter(file_path);
	        BufferedWriter out = new BufferedWriter(fstream);
 		    out.write("Hello Test");
  		    //FileNotFoundExceptionClose the output stream
   		    out.close();
	    } catch (Exception e){//Catch exception if any
    	    System.err.println("Error: " + e.getMessage());
    	}
	    
	    InputStream is = null;
	    try {
			is = new BufferedInputStream(new FileInputStream(file_path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String output_file_name = file_tools.inputStream2file(is, "", out_file_path);
		
		File f = new File(output_file_name);
		assertTrue(f.exists());

		
    }
}
