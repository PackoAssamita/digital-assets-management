/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */


package org.iavante.sling.commons.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

public class CopyAndRenameOperationIT extends TestCase {
	
	private final String COLLECTIONS_URL = "/content/colecciones/";
	private final String CONTENTS_FOLDER = "contents";
	private final String SOURCES_FOLDER  = "sources";
	
	private String title_col;
	private String title1;
	private String title2;
	
	private String source_title;
	private String source_file;
	private String source_mime;
	private String source_dest_title;
	private String source_dest_slug;
	
	private String description1;
	private String description2;
	
	private String schema;
	private String slug_content1;
	private String slug_content2;
	
	private String slug_collection;
	
	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);
	
	private final String HOSTVAR = "SLINGHOST";
  private final String HOSTPREDEF = "localhost:8888";
  private String SLING_URL = "http://";
		
	HttpClient client;
	
	protected void setUp() {	
		
	  Map<String,String> envs = System.getenv();
	  Set<String> keys = envs.keySet();
		
	  Iterator<String> it = keys.iterator();
	  boolean hashost = false;
	  while (it.hasNext()){
	    String key = (String) it.next();
	      
	    if (key.compareTo(HOSTVAR) == 0 ){
	      SLING_URL = SLING_URL + (String) envs.get(key);
	    	hashost = true;
	    }
	  }	    
	
    if (hashost == false)
	    SLING_URL = SLING_URL + HOSTPREDEF;
		
	  client = new HttpClient();
	  
	  title_col = "Title collection";
   	title1 = "Test case content 1";
   	title2 = "Test case content 2";
   	
   	source_file = "test.avi";
   	source_mime = "video/avi";
   	source_dest_slug = "fuente_default";
   	source_dest_title = "Fuente por defecto";
   	
   	description1 = "Description 1";
   	description2 = "Description 2";
   	
   	schema = "default";
   	slug_collection = "admin";
   	slug_content1 = "it_content_1";   
   	slug_content2 = "it_content_2";
 
    authPrefs.add(AuthPolicy.DIGEST);
    authPrefs.add(AuthPolicy.BASIC);
    defaultcreds = new UsernamePasswordCredentials("admin", "admin");
  	 	
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(AuthScope.ANY, defaultcreds);
    client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

    // Create collection
    PostMethod post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection);	  
   	
    post_create_col.setDoAuthentication(true);    	 
  	 
    NameValuePair[] data_create_col = {
        new NameValuePair("sling:resourceType", "gad/collection"),
        new NameValuePair("title", title_col),
        new NameValuePair("subtitle", ""),
		    new NameValuePair("schema", schema),         
	      new NameValuePair("jcr:created", ""),
        new NameValuePair("jcr:createdBy", ""),
        new NameValuePair("jcr:lastModified", ""),
        new NameValuePair("jcr:lastModifiedBy", "")            
    };
        
	  post_create_col.setRequestBody(data_create_col);
              
    try {
	    client.executeMethod(post_create_col);
	  } catch (HttpException e1) {
	    e1.printStackTrace();
	  } catch (IOException e1) { 		
	    e1.printStackTrace();
	  }

	  post_create_col.releaseConnection();

	  // Create content in collection    	
    try {
      Thread.sleep(2000);
 	   } catch (InterruptedException e1) {
    	 e1.printStackTrace();
 	   }  

     PostMethod post_create = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content1);	  
     post_create.setDoAuthentication(true);    	 
   	 
     NameValuePair[] data_create = {
  	     new NameValuePair("sling:resourceType", "gad/content"),
         new NameValuePair("title", title1),
         new NameValuePair("schema", schema),
         new NameValuePair("description", description1),
         new NameValuePair("author", "Test case"),
         new NameValuePair("origin", "Test case"),
         new NameValuePair("lang", "es"),
         new NameValuePair("tags@TypeHint", "String[]"),
         new NameValuePair("state", "pending"),
         new NameValuePair("jcr:created", ""),
         new NameValuePair("jcr:createdBy", ""),
         new NameValuePair("jcr:lastModified", ""),
         new NameValuePair("jcr:lastModifiedBy", ""), 
         new NameValuePair("_charset_", "utf-8")
     };
         
     post_create.setRequestBody(data_create);
     try {
       client.executeMethod(post_create);
 	   } catch (HttpException e1) {
 	   	 e1.printStackTrace();
 	   } catch (IOException e1) { 		
 		   e1.printStackTrace();
     }	

     post_create.releaseConnection();

		 try {
			 Thread.sleep(2000);
 		 } catch (InterruptedException e1) {
	     e1.printStackTrace();
 		 } 
 		 // Create the source
  	 PostMethod post_create_source = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection + "/" + SOURCES_FOLDER + "/" + source_title);   
  	 post_create.setDoAuthentication(true);    	 
     NameValuePair[] data_create_source = {        		 
      		 	new NameValuePair("sling:resourceType", "gad/source"),
     	 			new NameValuePair("title", source_dest_title),
     	 			new NameValuePair("file", source_file),
     	 			new NameValuePair("mimetype",source_mime),
     	 			new NameValuePair("text_encoding", ""),
     	 			new NameValuePair("lang", ""),
     	 			new NameValuePair("length", ""),
     	 			new NameValuePair("size", ""),
     	 			new NameValuePair("type", ""),
     	 			new NameValuePair("bitrate", ""),
     	 			new NameValuePair("tracks_number", ""),
     	 			new NameValuePair("track_1_type", ""),
     	 			new NameValuePair("track_1_encoding", ""),
     	 			new NameValuePair("track_1_features", ""),
     	 			new NameValuePair("track_2_type", ""),
     	 			new NameValuePair("track_2_encoding", ""),
     	 			new NameValuePair("track_2_features", ""),   	
     	 			new NameValuePair("jcr:created", ""),
     	 			new NameValuePair("jcr:createdBy", ""),
     	 			new NameValuePair("jcr:lastModified", ""),
     	 			new NameValuePair("jcr:lastModifiedBy", "")            
       };
       post_create_source.setRequestBody(data_create_source);

       try {
			client.executeMethod(post_create_source);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}
		
		assertEquals(201, post_create_source.getStatusCode());
		post_create_source.releaseConnection(); 	
  }
	
  protected void tearDown() {

    // Delete the content
    try {
 		  Thread.sleep(6000);
 		} catch (InterruptedException e1) {
	    e1.printStackTrace();
 		}  
 		
		PostMethod post_delete = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection);		
		NameValuePair[] data_delete = {
		           new NameValuePair(":operation", "delete"),		             
		};
		
   	post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

    try {
 			client.executeMethod(post_delete);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		

 		post_delete.releaseConnection();   
    }
  
  public void test_copy_and_rename() {
  	
  	try {
			 Thread.sleep(2000);
		 } catch (InterruptedException e1) {
	     e1.printStackTrace();
		 } 
  	
  	PostMethod post_copyandrename = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection + "/" + SOURCES_FOLDER + "/" + source_title);	  
    post_copyandrename.setDoAuthentication(true);    	 
  	 
    String dest = COLLECTIONS_URL + slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content1 + "/" + SOURCES_FOLDER + "/" + source_dest_slug;
    
    NameValuePair[] data_copy = {
 	     new NameValuePair(":operation", "copyandrename"),
       new NameValuePair(":dest", dest)
 	  };
        
    post_copyandrename.setRequestBody(data_copy);
    try {
      client.executeMethod(post_copyandrename);
	   } catch (HttpException e1) {
	   	 e1.printStackTrace();
	   } catch (IOException e1) { 		
		   e1.printStackTrace();
    }	

    post_copyandrename.releaseConnection();
    
	   // Create another content in collection    	
    try {
      Thread.sleep(4000);
 	   } catch (InterruptedException e1) {
    	 e1.printStackTrace();
 	   }  
    
  	HttpMethod get_content = new GetMethod(SLING_URL + dest + "/file");
  	
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    
		// handle response.	
		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		assertEquals(response_body, source_file);	
		get_content.releaseConnection();  
		
   HttpMethod get_content_title = new GetMethod(SLING_URL + dest + "/title");
  	
		try {
			this.client.executeMethod(get_content_title);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    
		// handle response.	
		response_body = "";
		try {
			response_body = get_content_title.getResponseBodyAsString();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		assertEquals(response_body, source_dest_title);	
		get_content.releaseConnection(); 
		
		// Bad operation
		PostMethod post_copyandrename_bad = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection + "/" + SOURCES_FOLDER);	  
    dest = COLLECTIONS_URL + slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content1 + "/" + SOURCES_FOLDER + "/" + source_dest_slug;
    
    NameValuePair[] data_copy_bad = {
 	     new NameValuePair(":operation", "copyandrename"),
       new NameValuePair(":dest", dest)
    };
        
    post_copyandrename_bad.setRequestBody(data_copy_bad);
    try {
      client.executeMethod(post_copyandrename_bad);
	   } catch (HttpException e1) {
	   	 e1.printStackTrace();
	   } catch (IOException e1) { 		
		   e1.printStackTrace();
    }	

	  assertEquals(post_copyandrename_bad.getStatusCode(), 412);
    post_copyandrename_bad.releaseConnection(); 	   
  }
}
