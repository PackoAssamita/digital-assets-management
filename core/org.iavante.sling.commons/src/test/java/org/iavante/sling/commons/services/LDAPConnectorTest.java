/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;



import static junit.framework.Assert.*;


/**
 * Test LDAPConnector Class. This test has to change the right 
 * ldap environment properties to test a user that exist
 * and other that it doesn't exist to Test the LdapConnecotr Class
 *   
 */
public class LDAPConnectorTest{
	
	private final String HOSTVAR = "SLINGHOST";
   	private final String HOSTPREDEF = "localhost:8888";
   	private String SLING_URL = "http://";
   	private final String OK1_URL = "/content/colecciones/tests";
   	private final String OK2_URL = "/content/colecciones/tests/subnivel1";
   	private final String OK3_URL = "/content/colecciones/tests/subnivel1/subnivel2";
   	private final String BAD_URL = "/content/otrodestino";
   	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);
	HttpClient client;
	

	@org.junit.Before
	public void setUp() {
		
		
		Map<String,String> envs = System.getenv();
		Set<String> keys = envs.keySet();
		
	    Iterator<String> it = keys.iterator();
	    boolean hashost = false;
	    while (it.hasNext()){
	      String key = (String) it.next();
	      
	      if (key.compareTo(HOSTVAR) == 0 ){
	    	  SLING_URL = SLING_URL + (String) envs.get(key);
	    	  hashost = true;
	      }
	    }	    
		if (hashost == false)
			{SLING_URL = SLING_URL + HOSTPREDEF;}
		
	
		client = new HttpClient();
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		

		
    }
	
	@org.junit.After
    public void tearDown() {
    }

	@org.junit.Test
    public void accessAuthorized1() {
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
    	GetMethod okaccess = new GetMethod(SLING_URL + OK1_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
				authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals("Access authorized", okaccess.getStatusCode() , 404 );
		okaccess.releaseConnection();
    }
    public void accessAuthorized2() {
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
    	GetMethod okaccess = new GetMethod(SLING_URL + OK2_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
				authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals("Access authorized", okaccess.getStatusCode() , 404 );
		okaccess.releaseConnection();
    }	
    public void accessAuthorized3() {
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
    	GetMethod okaccess = new GetMethod(SLING_URL + OK3_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
				authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals("Access authorized", okaccess.getStatusCode() , 404 );
		okaccess.releaseConnection();
    }
	@org.junit.Test
    public void accessUnauthorized_bad_url() {
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
    	GetMethod okaccess = new GetMethod(SLING_URL + BAD_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
				authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue("Access authorized", okaccess.getStatusCode() == 403
										|| okaccess.getStatusCode() == 401);
		okaccess.releaseConnection();
    }
    
    
	@org.junit.Test
    public void accessUnauthorized_wrong_user() {
		defaultcreds = new UsernamePasswordCredentials("test", "test");
    	GetMethod okaccess = new GetMethod(SLING_URL + OK1_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
				authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue("Access authorized", okaccess.getStatusCode() == 403
										|| okaccess.getStatusCode() == 401);
		okaccess.releaseConnection();
    }
}