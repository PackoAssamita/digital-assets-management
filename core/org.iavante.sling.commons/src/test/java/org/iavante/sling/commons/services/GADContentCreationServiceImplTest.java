/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;

import org.iavante.sling.commons.services.impl.GADContentCreationServiceImpl;

public class GADContentCreationServiceImplTest extends RepositoryTestBase {
	
	GADContentCreationServiceImpl gad_content_tools;
	SlingRepository repo;
	Session session;
	
	private Node rootNode;
	private Node source_node;
	private Node content_node;
	private Node tags_node;
	
	private String tag1_title = "salud";
	private String tag2_title = "equilibrada";
	
	private String file_name = "test.flv";
	
	private final String TITLE_PROPERTY = "title";
	private final String COUNT_PROPERTY = "count";

    @Override
    protected void setUp() throws Exception {
    	
    	super.setUp();
    	session = getSession();
    	
    	String[] prefixes = session.getWorkspace().getNamespaceRegistry().getPrefixes();
    	
    	boolean registered = false;
    	
    	for (int i=0; i< prefixes.length; i++) {
    		if (prefixes[i].equals("sling")) registered = true;
    		
    	}
    	
    	if (!registered) session.getWorkspace().getNamespaceRegistry().registerNamespace("sling", "http://www.iavante.es/wiki/1.0");

        rootNode = getSession().getRootNode(); 
        
        // Add content node
        content_node = rootNode.addNode("test" +  System.currentTimeMillis(), "nt:unstructured");
        //content_node.setProperty("sling:resource/type", "gad/content");
        //content_node.setProperty("file", file_name);        
        rootNode.save();
        // Create tags node
        tags_node = content_node.addNode("tags", "nt:unstructured"); 
        content_node.save();
        Node tag_node = tags_node.addNode("salud", "nt:unstructured");
        tag_node.setProperty("sling:resourceType", "gad/tag");
        tag_node.setProperty(TITLE_PROPERTY, tag1_title);
        tag_node.setProperty(COUNT_PROPERTY, "0");
        tags_node.save();
        
        source_node = content_node.addNode("sources", "nt:unstructured").addNode("source_default", "nt:unstructured");
        //source_node.setProperty("sling:resourceType", "gad/source");
        //source_node.setProperty("file", file_name);
        // Add log node
        rootNode.save();
        
        content_node.addMixin("mix:versionable");
		content_node.addMixin("mix:referenceable");	
        content_node.addNode("log", "nt:unstructured");        
        
        // Add versionable properties
        Node log_node = content_node.getNode("log");
        log_node.addMixin("mix:versionable");
		log_node.addMixin("mix:referenceable");		

		content_node.save();
		
		content_node.setProperty("edit", "test");
		content_node.save();
		content_node.checkin();
		content_node.checkout();		
		
        session.save();     
        
        gad_content_tools = new GADContentCreationServiceImpl();
    }	
    
	@Override	
    protected void tearDown() throws Exception {
        super.tearDown();        
    }
    
    public void test_createTransformation() { 
    	
    	String name = "preview";
    	String mimetype = "video/x-flv";
    	String trans_path = gad_content_tools.createTransformation(source_node, name, mimetype);    	
    	Node trans_node;
    	String test_mimetype = null;
    	String test_node_name = null;
    	
		try {
			trans_node = rootNode.getNode(trans_path.substring(1));
			test_mimetype = trans_node.getProperty("mimetype").getValue().getString();
			test_node_name = trans_node.getName();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
    	
    	assertEquals(test_node_name, name);
    	assertEquals(test_mimetype, mimetype);
    	
    	name = "mobile";
    	mimetype = "vide/3gp";
    	trans_path = gad_content_tools.createTransformation(source_node, name, mimetype);    	
    	

		try {
			trans_node = rootNode.getNode(trans_path.substring(1));
			test_mimetype = trans_node.getProperty("mimetype").getValue().getString();
			test_node_name = trans_node.getName();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();			
		}
		
    	assertEquals(test_node_name, name);
    	assertEquals(test_mimetype, mimetype);    	
    }
    
    public void test_updateLogNode() throws PathNotFoundException, RepositoryException { 
    
    	String status = "pending";
    	String dest_path = "/content/catalogo/pendientes";
    	//String version = "jcr:rootVersion";
    	String version = "1.0";
    	String hardcoded_version ="1.10";
    	
    	String test_status;
    	String test_dest_path;   
    	String test_version;
    	
    	gad_content_tools.updateLogNode(content_node, status, dest_path);
    	
    	Node test_log_node = content_node.getNode("log");
    	
    	test_status = test_log_node.getProperty("state").getValue().getString();
    	test_dest_path = test_log_node.getProperty("url").getValue().getString();
    	test_version = test_log_node.getProperty("version").getValue().getString();
    	
    	assertEquals(status, test_status);
    	assertEquals(dest_path, test_dest_path);
    	assertEquals(version, test_version);
    	
    	content_node.setProperty("version", hardcoded_version);
    	content_node.save();
    	
    	gad_content_tools.updateLogNode(content_node, status, dest_path);
    	
        test_log_node = content_node.getNode("log");
    	
    	test_status = test_log_node.getProperty("state").getValue().getString();
    	test_dest_path = test_log_node.getProperty("url").getValue().getString();
    	test_version = test_log_node.getProperty("version").getValue().getString();
    	
    	assertEquals(status, test_status);
    	assertEquals(dest_path, test_dest_path);
    	assertEquals(hardcoded_version, test_version);
    }
    
    public void test_updateTags() throws ValueFormatException, IllegalStateException, PathNotFoundException, RepositoryException {

    	gad_content_tools.updateTags(content_node, tag1_title);	
    	gad_content_tools.updateTags(content_node, tag2_title);	
    	gad_content_tools.updateTags(content_node, tag2_title);
		String tag1_count = tags_node.getNode(tag1_title).getProperty(COUNT_PROPERTY).getValue().getString();
		String tag2_count = tags_node.getNode(tag2_title).getProperty(COUNT_PROPERTY).getValue().getString();
    	
		assertEquals(tag1_count, "1");
		assertEquals(tag2_count, "2");
    }
    
    public void test_generateSlug() {
    	
    	String title = "aóúi?test";
    	String slug = "aoui-test";
    	
    	assertEquals(slug, gad_content_tools.generateSlug(title));
    }
    
    public void test_normalizeTag() {
    	
    	String tag_name = "baño formación";
    	String slug = "bano formacion";
    	    	
    	assertEquals(slug, gad_content_tools.normalizeTag(tag_name));
    	
    	tag_name = "áéíóú";
    	slug = "aeiou";
    	
    	assertEquals(slug, gad_content_tools.normalizeTag(tag_name));
    	
    }
    
    public void test_getFileNameFromNode() throws ValueFormatException, VersionException, LockException, ConstraintViolationException, RepositoryException {    
    	
    	content_node.setProperty("sling:resourceType", "gad/content");
    	content_node.setProperty("file", file_name);
    	content_node.save();
    	
    	source_node = content_node.getNode("sources").addNode("fuente_default");
    	content_node.save();
    	
    	source_node.setProperty("sling:resourceType", "gad/source");
    	source_node.setProperty("fle", file_name);
    	source_node.save();
    	
    	source_node.addNode("transformations");
    	source_node.save();
    	Node preview_node = source_node.getNode("transformations").addNode("preview");
    	preview_node.setProperty("sling:resourceType", "gad/transformation");
    	preview_node.setProperty("file", file_name);
    	
    	source_node.save();
    	content_node.save();
    	
    	assertEquals(file_name, gad_content_tools.getFilenameFromNode(content_node, ""));
    	assertEquals(file_name, gad_content_tools.getFilenameFromNode(source_node, ""));
    	assertEquals(file_name, gad_content_tools.getFilenameFromNode(preview_node, ""));
    	
    	String file_name1 = "file_trans_1.mp4";
    	Node transformations = source_node.getNode("transformations");
    	Node trans1_node = source_node.getNode("transformations").addNode("trans1");
    	transformations.save();
    	trans1_node.setProperty("sling:resourceType", "gad/transformation");
    	trans1_node.setProperty("file", file_name1);
    	trans1_node.save();
    	
    	assertEquals(file_name1, gad_content_tools.getFilenameFromNode(content_node, "trans1"));
    	assertEquals(file_name1, gad_content_tools.getFilenameFromNode(source_node, "trans1"));    	
    	assertEquals(file_name1, gad_content_tools.getFilenameFromNode(trans1_node, "trans1"));
    	
    	
    }
    
    
}