/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.security.PublicKey;

/**
 * @scr.property name="service.description" value="IAVANTE - EncryptionService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface EncryptionService {

	/**
	 * Getter for RSA key size.
	 * 
	 * @return Size of the RSA key generated
	 */
	public int getRSA_Key_Size();

	/**
	 * Public Key getter
	 * 
	 * @return Public Key
	 */
	public PublicKey getPublicKey();

	/**
	 * Public Key getter
	 * 
	 * @param encoding
	 * @return Public Key as String
	 * @throws Exception
	 */
	public String getPublicKeyAsString(String encoding) throws Exception;

	/**
	 * Encoding getter
	 * 
	 * @return Used encoding
	 */
	public String getEncoding();

	/**
	 * Encrypt a message with an internal Public Key
	 * 
	 * @param message
	 * @return Encrypted message
	 * @throws Exception
	 */
	public String encryptMessage(String message) throws Exception;

	/**
	 * Encrypt a message with a specified Public Key
	 * 
	 * @param message
	 * @param key
	 * @return Encrypted message
	 * @throws Exception
	 */
	public String encryptMessage(String message, PublicKey key) throws Exception;

	/**
	 * Encrypt a message with specific Public Key and encoding
	 * 
	 * @param message
	 * @param key
	 * @param encoding
	 * @return Encrypted message
	 * @throws Exception
	 */
	public String encryptMessage(String message, PublicKey key, String encoding)
			throws Exception;

	/**
	 * Encrypt a message with a specified Public Key as String
	 * 
	 * @param message
	 * @param key
	 * @return Encrypted message
	 * @throws Exception
	 */
	public String encryptMessage(String message, String key) throws Exception;

	/**
	 * Encrypt a message with specific Public Key and encoding as String
	 * 
	 * @param message
	 * @param key
	 * @param encoding
	 * @return Encrypted message
	 * @throws Exception
	 */
	public String encryptMessage(String message, String key, String encoding)
			throws Exception;

	/**
	 * Decrypt a message with an internal Private Key
	 * 
	 * @param message
	 * @return Decrypted message
	 * @throws Exception
	 */
	public String decryptMessage(String message) throws Exception;

	/**
	 * Decrypt a message with an internal Private Key and a specified encoding
	 * 
	 * @param message
	 * @param encoding
	 * @return Decrypted message
	 * @throws Exception
	 */
	public String decryptMessage(String message, String encoding)
			throws Exception;

}