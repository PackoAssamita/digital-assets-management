/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.util.Map;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

/**
 * Create gad structures.
 * 
 * @scr.property name="service.description"
 *               value="IAVANTE - GADContentCreationService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface GADContentCreationService {

	/**
	 * Create a transformation of a source
	 * 
	 * @param source
	 *          folder
	 * @param name
	 *          of the transformation
	 * @param mimetype
	 *          of the transformation
	 * @return new created transformation path
	 */
	public String createTransformation(Node source, String name, String mimetype);

	/**
	 * Update the log node with a new status change
	 * 
	 * @param content
	 * @param state
	 *          For example: peding, denied, published
	 * @param destPath
	 *          Each update implies a new destination path
	 */
	public void updateLogNode(Node content, String state, String destPath);

	/**
	 * If the tag exists it increments the tags counter, on the other hand it
	 * creates a new tag node with gad/tag as sling:resourceType
	 * 
	 * @param node
	 *          containing the tags cloud
	 * @param tag
	 *          to update
	 * @throws RepositoryException
	 * @throws PathNotFoundException
	 */
	public void updateTags(Node node, String tag) throws PathNotFoundException,
			RepositoryException;

	/**
	 * Generates the kind of string that you can use in an url.
	 * 
	 * @param s
	 *          The input string
	 * @return new string
	 */
	public String generateSlug(String s);

	/**
	 * Normalize the tag name. Converts to lowercase and strip accents.
	 * 
	 * @param tagName
	 *          The tag
	 * @return normalized tag
	 */
	public String normalizeTag(String tagName);
	
	/**
	 * Get a content type schemas
	 * @param content_type
	 * 					The type of content. Content, Collectin, Channel, etc.
	 * @return schemas 
	 */
	public Map<String, String> getSchemas(String content_type);
	
	/**
	 * A filename is associated to a node depending of the sling:resourceType.
	 * Each source has a filename associated, and a content has a filename of the
	 * preview associated. If get an error, return an empty string
	 * 
	 * @param node
	 *          node to get the media filename associated
	 * @param format
	 * 				  the transformation to search for the file. If blank format=preview
	 * @return the filename associated to the node depending of the
	 *         sling:resourceType
	 */
	public String getFilenameFromNode(Node node, String format);
}
