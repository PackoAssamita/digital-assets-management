/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.LDAPConnector;
import org.iavante.sling.commons.services.PortalSetup;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for LDAP connections, check users and permissions
 * 
 * @scr.component immediate="true"
 * @see org.iavante.sling.commons.services.LDAPConnector
 * @scr.property name="service.description"
 *               value="IAVANTE - LDAP Connector Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.commons.services.LDAPConnector"
 */
public class LDAPConnectorImpl implements LDAPConnector, Serializable {

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Internal properties. */
	private static Map<String, String> properties;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** LDAP URL. */
	private String DEFAULT_LDAP_SERVER;

	/** LDAP Manager user. */
	private String DEFAULT_LDAP_ROOTDN;

	/** LDAP Manager password. */
	private String DEFAULT_LDAP_ROOTPASSWORD;

	/** LDAP Default Context. */
	private String DEFAULT_LDAP_CONTEXT;

	/** LDAP Context to People Node. */
	private String DEFAULT_LDAP_PEOPLE;

	/** LDAP Context to Groups node. */
	private String DEFAULT_LDAP_GROUPS;

	/** Context for default server. */
	private InitialLdapContext defaultServerConnection;

	/** Context for group server. */
	private InitialLdapContext GroupServerConnection;

	/** Context for people server. */
	private InitialLdapContext PeopleServerConnection;

	protected void activate(ComponentContext context) {
		properties = portalSetup.get_config_properties("/authentication");

		DEFAULT_LDAP_SERVER = properties.get("default_ldap_server");
		DEFAULT_LDAP_ROOTDN = properties.get("default_ldap_rootdn");
		DEFAULT_LDAP_ROOTPASSWORD = properties.get("default_ldap_rootpassword");
		DEFAULT_LDAP_CONTEXT = properties.get("defualt_ldap_context");
		DEFAULT_LDAP_PEOPLE = properties.get("defualt_ldap_people");
		DEFAULT_LDAP_GROUPS = properties.get("defualt_ldap_groups");

		connectLdapServer();

	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public boolean checkUser(String userID, String passWord) {

		boolean usuarioAutenticado = false;

		String rootdn = "uid=" + userID + "," + DEFAULT_LDAP_PEOPLE;

		String rootpass = passWord;

		Hashtable<String, String> mycheck = new Hashtable<String, String>();

		mycheck.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		mycheck.put(Context.PROVIDER_URL, "ldap://" + DEFAULT_LDAP_SERVER + "/"
				+ DEFAULT_LDAP_CONTEXT);
		mycheck.put(Context.SECURITY_AUTHENTICATION, "simple");
		mycheck.put(Context.SECURITY_PRINCIPAL, rootdn);
		mycheck.put(Context.SECURITY_CREDENTIALS, rootpass);

		try {
			InitialLdapContext otherserverContext = new InitialLdapContext(mycheck,
					null);
			usuarioAutenticado = true;

		} catch (NamingException e) {
			log.info("Error checking user. The User is not valid or you havn't"
					+ " got the credentials properly configured. Please check the LDAP"
					+ " properties.");
		}
		return usuarioAutenticado;
	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public void connectLdapServer() {

		Hashtable<String, String> myenv = new Hashtable<String, String>();

		myenv.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		myenv.put(Context.PROVIDER_URL, "ldap://" + DEFAULT_LDAP_SERVER + "/"
				+ DEFAULT_LDAP_CONTEXT);
		myenv.put(Context.SECURITY_AUTHENTICATION, "simple");
		myenv.put(Context.SECURITY_PRINCIPAL, DEFAULT_LDAP_ROOTDN);
		myenv.put(Context.SECURITY_CREDENTIALS, DEFAULT_LDAP_ROOTPASSWORD);

		try {
			defaultServerConnection = new InitialLdapContext(myenv, null);
		} catch (NamingException e) {
			log.error("Problem connecting to LDAP ");
			e.printStackTrace();
		}

	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public void createGroup(String cn) {

		Attributes group = new BasicAttributes("cn", cn, true);

		Attribute members = new BasicAttribute("member");

		members.add(DEFAULT_LDAP_ROOTDN);
		members.add("");
		Attribute objectClass = new BasicAttribute("objectClass");
		objectClass.add("top");
		objectClass.add("groupOfNames");

		group.put(objectClass);
		group.put(members);

		Hashtable<String, String> myenv = new Hashtable<String, String>();

		myenv.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		myenv.put(Context.PROVIDER_URL, "ldap://" + DEFAULT_LDAP_SERVER + "/"
				+ DEFAULT_LDAP_GROUPS);
		myenv.put(Context.SECURITY_AUTHENTICATION, "simple");
		myenv.put(Context.SECURITY_PRINCIPAL, DEFAULT_LDAP_ROOTDN);
		myenv.put(Context.SECURITY_CREDENTIALS, DEFAULT_LDAP_ROOTPASSWORD);
		try {
			GroupServerConnection = new InitialLdapContext(myenv, null);
		} catch (NamingException e) {
			log.error(" >>>>>>>>>>>>>_____createGroup______->>>>>>>>>>>> Failed" + e);
		}

		try {
			DirContext ctx = GroupServerConnection
					.createSubcontext("cn=" + cn, group);
		} catch (NameAlreadyBoundException ignore) {
			log.error("Name already bound");
		} catch (NamingException ne) {
			log.error("Naming problem");
		}
	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public boolean checkUserByPath(String userID, String uri) {

		String collectionsUrl = null;
		try {
			collectionsUrl = properties.get("memberof_bypath");
		} catch (Exception e) {
			collectionsUrl = "/content/colecciones/";
		}

		if (uri.contains(collectionsUrl) == false) {
			return true;
		} else {
			if (uri.contains(collectionsUrl + userID + "/") == true
					|| uri.endsWith(collectionsUrl + userID) == true) {
				return true;
			} else {
				return false;
			}
		}
	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public boolean isMemberOf(String userID, String path) {
		boolean ismember = false;
		String grupoFinal = "";

		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		String[] pathSplited= new String[path.split("/").length];
		pathSplited = path.split("/");
		int longitud = pathSplited.length;
		if (longitud >= 1) {
			if (pathSplited[0].equals("content")) {
				pathSplited[0] = "gad";
			}
			for (int i = 0; i < longitud - 1; i++) {
				if (grupoFinal == "") {
					grupoFinal = pathSplited[i];
				} else {
					grupoFinal = grupoFinal + "|" + pathSplited[i];
				}
			}
			if ((pathSplited[1].equals("catalogo")) && (longitud>=2)) {
				//user is trying to access to revised, pending or denied content
				grupoFinal=grupoFinal+"|"+pathSplited[2];
				
			}
			ismember = isMemberOfGroup(userID, grupoFinal);

		}
		if (log.isInfoEnabled())
			log.info("isMemberOf(" + userID + "," + grupoFinal + "):" + ismember);
		return ismember;
	}

	/*
	 * @see org.iavante.sling.commons.services.LDAPConnector
	 */
	public void setProperties(String default_ldap_server,
			String default_ldap_rootdn, String default_ldap_rootpassword,
			String defualt_ldap_context, String defualt_ldap_people,
			String defualt_ldap_groups) {

		DEFAULT_LDAP_SERVER = default_ldap_server;
		DEFAULT_LDAP_ROOTDN = default_ldap_rootdn;
		DEFAULT_LDAP_ROOTPASSWORD = default_ldap_rootpassword;
		DEFAULT_LDAP_CONTEXT = defualt_ldap_context;
		DEFAULT_LDAP_PEOPLE = defualt_ldap_people;
		DEFAULT_LDAP_GROUPS = defualt_ldap_groups;
	}

	// ------------------ internal ----------------------
	/**
	 * Replace a target value.
	 * 
	 * @param target
	 *          is the original string
	 * @param from
	 *          is the string to be replaced
	 * @param to
	 *          is the string which will used to replace
	 * @return a new string
	 */
	private static String replace(String target, String from, String to) {

		int start = target.indexOf(from);
		if (start == -1)
			return target;
		int lf = from.length();
		char[] targetChars = target.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int copyFrom = 0;
		while (start != -1) {
			buffer.append(targetChars, copyFrom, start - copyFrom);
			buffer.append(to);
			copyFrom = start + lf;
			start = target.indexOf(from, copyFrom);
		}
		buffer.append(targetChars, copyFrom, targetChars.length - copyFrom);
		return buffer.toString();
	}

	/**
	 * This method returns an ArrayList with all the LDAP groups that the user is
	 * included or null if the user is not included in any group.
	 * 
	 * @param user
	 * @return List with groups
	 */
	private List getGroupsOf(String user) {

		List groupsList = Collections.synchronizedList(new ArrayList());

		Hashtable<String, String> myenv = new Hashtable<String, String>();

		myenv.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		myenv.put(Context.PROVIDER_URL, "ldap://" + DEFAULT_LDAP_SERVER + "/"
				+ DEFAULT_LDAP_GROUPS);
		myenv.put(Context.SECURITY_AUTHENTICATION, "simple");
		myenv.put(Context.SECURITY_PRINCIPAL, DEFAULT_LDAP_ROOTDN);
		myenv.put(Context.SECURITY_CREDENTIALS, DEFAULT_LDAP_ROOTPASSWORD);

		try {
			LdapContext consulta_ctx = new InitialLdapContext(myenv, null);
			// Create the search controls
			SearchControls searchCtls = new SearchControls();
			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			// specify the LDAP search filter
			String searchFilter = "(objectClass=groupOfNames)";
			// Specify the Base for the search
			String searchBase = "";
			// initialize counter to total the group members
			int totalResults = 0;
			// Specify the attributes to return
			String returnedAtts[] = { "member" };
			searchCtls.setReturningAttributes(returnedAtts);
			// Search for objects using the filter
			NamingEnumeration answer = consulta_ctx.search(searchBase, searchFilter,
					searchCtls);
			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String groupName = sr.getName();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							if (attr.getID().equals("member")) {
								for (NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
									String thismember = (String) e.next();
									if (thismember.contains("uid=" + user)) {
										String subName = groupName.substring(3);
										groupsList.add(subName);
									}
								}
							}
						}
					} catch (NamingException e) {
						log.error("Problem listing members: " + e);
					}
				}
			}
		} catch (NamingException e) {
			log.error("Failed to start LDAP Connection DS :" + e);
			e.printStackTrace();
		}
		return groupsList;
	}

	/**
	 * Return list of groups for a specific user
	 * 
	 * @param user
	 * @return list of groups
	 */
	private ArrayList<String> memberOf(String user) {
		Hashtable<String, String> myenv = new Hashtable<String, String>();

		myenv.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		myenv.put(Context.PROVIDER_URL, "ldap://" + DEFAULT_LDAP_SERVER + "/"
				+ DEFAULT_LDAP_PEOPLE);
		myenv.put(Context.SECURITY_AUTHENTICATION, "simple");
		myenv.put(Context.SECURITY_PRINCIPAL, DEFAULT_LDAP_ROOTDN);
		myenv.put(Context.SECURITY_CREDENTIALS, DEFAULT_LDAP_ROOTPASSWORD);
		try {
			PeopleServerConnection = new InitialLdapContext(myenv, null);
		} catch (NamingException e) {
			e.printStackTrace();
		}

		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration results = null;
			results = PeopleServerConnection.search("",
					"(&(objectClass=person) (uid=" + user + "))", controls);
			while (results.hasMore()) {
				SearchResult searchResult = (SearchResult) results.next();
				Attributes attributes = searchResult.getAttributes();
				Attribute attr = attributes.get("memberOf");
				String cn = (String) attr.get();
			}
		} catch (NamingException ex) {
			log.error("Bind wrong");
		}

		ArrayList<String> myArr = new ArrayList<String>();

		myArr.add("gad|asd");
		return (myArr);
	}

	/**
	 * This method returns true if the userID is included in the member attribute
	 * of the group. The user must be declared as uid="theUserIdUsed".
	 * 
	 * @param userID
	 * @param group
	 * @return if user is member of group
	 */
	private boolean isMemberOfGroup(String userID, String group) {
		boolean ismember = false;
		List userGroupList = getGroupsOf(userID);
		int mySize = userGroupList.size();
		int j = 0;
		while ((j < mySize) && (ismember == false)) {
			String thisgroup = (String) userGroupList.get(j);
			if (group.startsWith(thisgroup)) {
				ismember = true;
			}
			j++;

		}
		return ismember;
	}

	@Override
	/**
	 * This method returns a List of chanels authorized for the user
	 * @param userID
	 * @return The List of chanels for this userID
	 */
	public ArrayList<String> getAuthorizedChannels(ServletRequest request,
			ServletResponse response) {

		ArrayList<String> ListaSalida = new ArrayList<String>();

		// Extract authentication info.
		SlingHttpServletRequest req = (SlingHttpServletRequest) request;
		AuthenticationInfo credentials = authTools.extractAuthentication(req);
		SimpleCredentials authInfo = null;

		if (credentials != null) {
			authInfo = (SimpleCredentials) credentials.getCredentials();

			List listaGrupos = getGroupsOf(authInfo.getUserID());
			Iterator iterator = listaGrupos.iterator();
			while (iterator.hasNext()) {
				String canal = (String) iterator.next();
				if (canal.startsWith("gad|canales|")) {
					String mycanal = canal.substring(12);
					if (mycanal != null) {
						ListaSalida.add(mycanal);
					}
				}
			}
		}

		return (ArrayList<String>) ListaSalida;
	}

}
