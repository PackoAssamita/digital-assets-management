/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.Version;
import javax.jcr.version.VersionException;
import javax.jcr.version.VersionIterator;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.GADContentCreationService
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE - GAD Content Creation Service Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 *    interface="org.iavante.sling.commons.services.GADContentCreationService"
 */
public class GADContentCreationServiceImpl implements GADContentCreationService {

	/** Transformations folder. */
	final String TRANSFORMATIONS_FOLDER = "transformations";

	/** Transformation resource type. */
	final String TRANSFORMATION_RESOURCETYPE = "gad/transformation";
	
	/** @scr.reference */
	private SlingRepository repository;
	
	/** Schemas folder */
	final String SCHEMAS_FOLDER = "schemas";
	
	/** Description property */
	final String DESCRIPTION_PROP = "description";

	/** Log node. */
	private final String LOG_NODE = "log";

	/** Tags node. */
	private final String TAGS_NODE = "tags";

	/** Tags counter prop. */
	private final String TAGS_COUNTER_PROP = "count";

	/** Tag title prop. */
	private final String TAG_TITLE_PROP = "title";

	/** Tag resource type. */
	private final String TAG_RESOURCE_TYPE = "gad/tag";

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/** @scr.reference */
	private PortalSetup portal_setup;
	
	/** Sources folder. */
	private final String SOURCES_FOLDER = "sources";

	public GADContentCreationServiceImpl() {
	}

	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService
	 */
	public String createTransformation(Node source, String name, String mimetype) {

		String transformationPath = "";

		// Create transformations node if not exists
		Node transformationsNode = null;

		try {
			if (!source.hasNode(TRANSFORMATIONS_FOLDER)) {
				transformationsNode = source.addNode(TRANSFORMATIONS_FOLDER,
						"nt:unstructured");
				source.save();
			} else {
				transformationsNode = source.getNode(TRANSFORMATIONS_FOLDER);
			}
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		// Create preview node into transformations folder if not exists
		Node transNode = null;
		try {
			if (!transformationsNode.hasNode(name)) {
				transNode = transformationsNode.addNode(name, "nt:unstructured");
				source.save();
			} else {
				transNode = transformationsNode.getNode(name);
			}
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchNodeTypeException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		try {
			transNode.setProperty("sling:resourceType", TRANSFORMATION_RESOURCETYPE);
			transNode.setProperty("title", name);
			transNode.setProperty("file", "");
			transNode.setProperty("file", "");
			transNode.setProperty("size", "");
			transNode.setProperty("length", "");
			transNode.setProperty("mimetype", mimetype);
			transNode.setProperty("encoding", "");
			transNode.setProperty("bitrate", "");
			transNode.setProperty("audio_encoding", "");
			transNode.setProperty("video_encoding", "");
			transNode.setProperty("jcr:created", "");
			transNode.setProperty("jcr:createdBy", "");
			transNode.setProperty("jcr:lastModified", "");
			transNode.setProperty("jcr:lastModifiedBy", "");
			transNode.save();
		} catch (ValueFormatException e1) {
			e1.printStackTrace();
		} catch (VersionException e1) {
			e1.printStackTrace();
		} catch (LockException e1) {
			e1.printStackTrace();
		} catch (ConstraintViolationException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		try {
			transformationPath = transNode.getPath();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("Transformation path: " + transformationPath);
		return transformationPath;
	}

	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService
	 */
	public void updateLogNode(Node content, String state, String destPath) {

		Node logNode;
		String currentVersion = null;

		try {
			logNode = content.getNode(LOG_NODE);
			logNode.checkout();
			logNode.setProperty("state", state);
			logNode.setProperty("url", destPath);

			if (log.isInfoEnabled())
				log.info("Log node saved");

			// Get the reference node version
			if (content.hasProperty("version")) {
				currentVersion = content.getProperty("version").getValue().getString();
			}

			else {
				VersionIterator vi = content.getVersionHistory().getAllVersions();

				while (vi.hasNext()) {
					Version v = vi.nextVersion();
					if (v.getSuccessors().length == 0) {
						currentVersion = v.getName();
					}
				}
			}

			// Update and save the log node
			logNode.setProperty("version", currentVersion);
			logNode.save();
			logNode.checkin();

			if (log.isInfoEnabled())
				log.info("New status : " + state + " Dest : " + destPath
						+ " Version: " + currentVersion);

		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService
	 */
	public void updateTags(Node node, String tag) throws PathNotFoundException,
			RepositoryException {

		Node tagsNode = node.getNode(TAGS_NODE);
		Node tagNode = null;

		String tagSlug = generateSlug(tag);
		// The tag exits yet so we have to update the tag counter
		if (tagsNode.hasNode(tagSlug)) {

			tagNode = tagsNode.getNode(tagSlug);
			int count = Integer.parseInt(tagNode.getProperty(TAGS_COUNTER_PROP)
					.getString());
			if (log.isInfoEnabled())
				log.info("Tags count for " + tagSlug + " incremented to " + count);
			tagNode.setProperty(TAGS_COUNTER_PROP, Integer.toString(count + 1));
			tagNode.save();

		}

		// the tag doesn't exist so we have to create it
		else {
			tagNode = tagsNode.addNode(tagSlug);
			tagNode.setProperty("sling:resourceType", TAG_RESOURCE_TYPE);
			tagNode.setProperty(TAGS_COUNTER_PROP, "1");
			tagNode.setProperty(TAG_TITLE_PROP, tag);
			if (log.isInfoEnabled())
				log.info("Tag " + tagSlug + " created");
		}
		node.save();
	}

	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService
	 */
	public String generateSlug(String s) {

		s = s.toLowerCase();
		s = stripAccents(s);
		return s.replaceAll("[^a-z0-9']+", "-");
	}

	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService
	 */
	public String normalizeTag(String tagName) {

		tagName = tagName.toLowerCase();
		tagName = stripAccents(tagName);
		return tagName;
	}

	//----------------- Internal ---------------------------
	
	/**
	 * Strip accents
	 * 
	 * @param s
	 *          The string
	 * @return The string without accents
	 */
	private String stripAccents(String s) {

		String PLAIN_ASCII = "AaEeIiOoUu" // grave
				+ "AaEeIiOoUuYy" // acute
				+ "AaEeIiOoUuYy" // circumflex
				+ "AaOoNn" // tilde
				+ "AaEeIiOoUuYy" // umlaut
				+ "Aa" // ring
				+ "Cc" // cedilla
				+ "OoUu" // double acute
		;

		String UNICODE = "\u00C0\u00E0\u00C8\u00E8\u00CC\u00EC\u00D2\u00F2\u00D9\u00F9"
				+ "\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00DD\u00FD"
				+ "\u00C2\u00E2\u00CA\u00EA\u00CE\u00EE\u00D4\u00F4\u00DB\u00FB\u0176\u0177"
				+ "\u00C3\u00E3\u00D5\u00F5\u00D1\u00F1"
				+ "\u00C4\u00E4\u00CB\u00EB\u00CF\u00EF\u00D6\u00F6\u00DC\u00FC\u0178\u00FF"
				+ "\u00C5\u00E5" + "\u00C7\u00E7" + "\u0150\u0151\u0170\u0171";

		if (s == null)
			return null;
		StringBuilder sb = new StringBuilder();
		int n = s.length();
		for (int i = 0; i < n; i++) {
			char c = s.charAt(i);
			int pos = UNICODE.indexOf(c);
			if (pos > -1) {
				sb.append(PLAIN_ASCII.charAt(pos));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	/*
	 * @see org.iavante.sling.commons.services.GADContentCreationService#getSchemas(String)
	 */
	public Map<String, String> getSchemas(String content_type) {

		Session session = null;
		Node root_node = null;
		Map<String, String> content_schemas = new HashMap<String, String>();
		
		String base_repo_dir = portal_setup.get_config_properties("/sling").get("base_repo_dir");
		
		
		try {
			session = repository.loginAdministrative(null);		
			root_node = session.getRootNode();
		} catch (Exception e) {
			log.error("cannot start");
		}
		
		NodeIterator schemas = null;
		try {
			schemas = 
				root_node.getNode(base_repo_dir + "/" + SCHEMAS_FOLDER + "/" + content_type).getNodes();
		
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		while(schemas.hasNext()){
			Node schema = schemas.nextNode();
			String schema_name;
			String schema_description = "";
			try {
				schema_name = schema.getName();
				if (schema.hasProperty(DESCRIPTION_PROP)) {
					schema_description = schema.getProperty(DESCRIPTION_PROP).getValue().getString();
				}
				
			  content_schemas.put(schema_name, schema_description);
			} catch (RepositoryException e) {
				e.printStackTrace();
			}			
		}		
		return content_schemas;		
	}
	
  /*
   * @see org.iavante.sling.commons.services.GADContentCreationService#getFileNameFromNode(Node node)
   */
	public String getFilenameFromNode(Node node, String format) {
		
		if ("".equals(format)) format = "preview";
		
		String fileName = null;
		String resourcetype = null;

		try {
			resourcetype = node.getProperty("sling:resourceType").getString();
		} catch (Exception e) {
			log
					.error("attempting to get the filename " +
							"from a node without a sling:resourceType");
			return "";
		}

		assert resourcetype != null;
		if (resourcetype.compareToIgnoreCase("gad/content") == 0
				|| resourcetype.compareToIgnoreCase("gad/revision") == 0) {
			try {
				fileName = node.getNode(SOURCES_FOLDER).getNode("fuente_default")
						.getNode("transformations").getNode(format).getProperty("file")
						.getString();
			} catch (RepositoryException e) {
				log.error("Getting content url: " + e.toString());
				return "";
			}
		}

		else if (resourcetype.compareToIgnoreCase("gad/source") == 0) {
			try {
				fileName = node.getNode("transformations").getNode(format)
						.getProperty("file").getString();
			} catch (RepositoryException e) {
				log.error("Getting source url: " + e.toString());
				return "";
			}
		} else if (resourcetype.compareToIgnoreCase("gad/transformation") == 0) {
			try {
				fileName = node.getProperty("file").getString();
			} catch (RepositoryException e) {
				log.error("Getting transformation url: " + e.toString());
				return "";
			}
		} else {
			log.error("resourceType without a file field");
			return "";
		}

		return fileName;
	}
}
