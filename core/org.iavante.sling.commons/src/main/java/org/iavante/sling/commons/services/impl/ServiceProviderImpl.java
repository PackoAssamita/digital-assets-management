/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.iavante.sling.commons.services.EncryptionService;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.commons.services.ServiceProvider;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.ServiceProvider
 * @scr.component
 * @scr.property name="service.description" value="IAVANTE - Service Provider Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.commons.services.ServiceProvider"
 */
public class ServiceProviderImpl implements ServiceProvider, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default logger. */
	private static final Logger log = LoggerFactory
			.getLogger(PortalSetupImpl.class);

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private EncryptionService encryptionTool;

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
	}

	/*
	 * @see org.iavante.sling.commons.services.ServiceProvider
	 */
	public String get_service_ticket(String type) {

		Map<String, String> componentProperties = this.portalSetup
				.get_config_properties("/" + type);
		String token = componentProperties.get("token");
		String ticket = token + ":" + new Date().getTime();
		String encTicket = "";

		try {
			encTicket = encryptionTool.encryptMessage(ticket);
		} catch (Exception e) {
			log("Problem encrypting");
			e.printStackTrace();
		}
		return encTicket;
	}

	/*
	 * @see org.iavante.sling.commons.services.ticket_is_valid
	 */
	public boolean ticket_is_valid(String component, String ticket) {

		// Get downloader properties
		String comp_token = this.portalSetup.get_config_properties("/" + component).get("token");
		String validPeriodTime = 
			this.portalSetup.get_config_properties("/" + component).get("expiration_time");
		

		// Desencrypt the ticket
		String ticket_encoded = new String(Base64.decodeBase64(ticket.getBytes()));

		String decryptTicket = "";
		try {
			decryptTicket = encryptionTool.decryptMessage(ticket_encoded);
		} catch (Exception e) {
			// The ticket is not valid. Desencryption exception
			e.printStackTrace();
			log.error("Ticket not valid. Desencryption error:" + e.toString());
			return false;
		}

		String[] campo = decryptTicket.split(":");
		String token = campo[0];
		String timestamp = campo[1];

		// Check token correct
		if (!token.equals(comp_token)) {
			return false;
		}

		// Time spent since the ticket was created in millisencods
		Long time_spent = new Date().getTime() - Long.parseLong(timestamp);

		// Current time in milliseconds
		Long valid_period_milliseconds = Long.parseLong(validPeriodTime) * 1000;

		// Compare times
		if (time_spent < valid_period_milliseconds) {
			log.error("PERIOD: " + valid_period_milliseconds.toString());
			log.error("TIME SPENT: " + time_spent);
			return true;
		}

		log.error("Ticket out of date");
		return false;
	}
}
