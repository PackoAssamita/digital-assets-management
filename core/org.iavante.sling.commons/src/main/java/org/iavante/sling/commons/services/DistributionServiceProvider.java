/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.util.List;

import org.iavante.sling.commons.distributionserver.IDistributionServer;

/**
 * Methods for distribution services
 * 
 * @scr.property name="service.description"
 *               value="IAVANTE - Distribution Service Provider"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public interface DistributionServiceProvider {

	/**
	 * Return an implementation of distribution service
	 * 
	 * @param server
	 * @return the object of the implementation
	 */
	public IDistributionServer get_distribution_server(String server);

	/**
	 * List the available distribution servers
	 * 
	 * @return a list of strings with the name of servers
	 */
	public List<String> list_distribution_servers();
}
