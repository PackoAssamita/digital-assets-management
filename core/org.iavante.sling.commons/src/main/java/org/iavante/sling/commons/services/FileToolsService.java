/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.io.InputStream;

/**
 * Commons tools about files file names generate output file names.
 * 
 * @scr.property name="service.description" value="IAVANTE - FilesToolsService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface FileToolsService {

	/**
	 * Generate the output filename for a transformation.
	 * 
	 * @param inputFileName
	 * @param String that normally determine the name of the transformation
	 * @param conversionType
	 *          for example: preview, mobile, etc
	 * @return The output filename for the specified transformation
	 */
	public String genTransformationFilename(String inputFileName, String suffix, 
			String conversionType);

	/**
	 * Converts a filestream to a file in the file system.
	 * 
	 * @param inputStream
	 * @param path
	 * @param fileName
	 * @return the filename
	 */
	public String inputStream2file(InputStream inputStream, String path,
			String fileName);
}
