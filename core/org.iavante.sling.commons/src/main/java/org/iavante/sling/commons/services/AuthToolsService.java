/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.sling.engine.auth.AuthenticationInfo;

/**
 * Commons tools about Authentication.
 * 
 * @scr.property name="service.description" value="IAVANTE - AuthToolsService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface AuthToolsService {

	/**
	 * Extract the Base64 authentication string from the request.
	 * 
	 * @param request
	 * @return object with extracted credentials
	 */
	public AuthenticationInfo extractAuthentication(HttpServletRequest request);
	
	/**
	 * Extract the Base64 authentication string from the request.
	 * 
	 * @param request
	 * @return object with extracted credentials
	 */
	public String extractUserPass(HttpServletRequest request);
}
