/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.iavante.sling.commons.services.FileToolsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.FileToolsService
 * @scr.component
 * @scr.property name="service.description"
 *               value="IAVANTE - Files Tools Service Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.commons.services.FileToolsService"
 */
public class FileToolsServiceImpl implements FileToolsService, Serializable {
	private static final long serialVersionUID = 1L;

	/** Blank space separator. */
	final String SEPARATOR = "_";

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/*
	 * @see org.iavante.sling.commons.services.FileToolsService
	 */
	public String genTransformationFilename(String inputFileName, String suffix, 
			String extension) {

		String outputFileName = "";
		String inputExtension = "";

		if (inputFileName.contains(".")) {
			inputExtension = inputFileName.split("\\.")[1];
			outputFileName = inputFileName.replace(".", SEPARATOR);
		}
		

		if (extension.equals("")) {
			outputFileName = outputFileName + SEPARATOR + suffix + "."
					+ inputExtension;
		} else {
			outputFileName = outputFileName + SEPARATOR + suffix + "."
					+ extension;
		}
		
		outputFileName = outputFileName.replaceAll("/", "_");
		
		if (log.isInfoEnabled())
			log.info("Input: " + inputFileName + " Output file name: "
					+ outputFileName);
		return outputFileName;
	}

	/*
	 * @see org.iavante.sling.commons.services.FileToolsService
	 */
	public String inputStream2file(InputStream inputStream, String path,
			String fileName) {

		if (path.equals(""))
			path = ".";

		if (!path.endsWith(File.separator))
			path = path + File.separator;

		String absPath = path + fileName;

		File f = new File(absPath);
		byte buf[] = new byte[1024];
		int len;

		OutputStream out;
		try {
			out = new FileOutputStream(f);
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("File created at: " + absPath);
		return absPath;
	}

}
