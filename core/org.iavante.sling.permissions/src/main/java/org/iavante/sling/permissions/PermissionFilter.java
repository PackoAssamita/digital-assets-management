/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.permissions;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.LDAPConnector;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PermissionFilter.
 * 
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="IAV Permissions Request Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="-20474836" type="Integer"
 *               private="true"
 * @scr.service
 */
public class PermissionFilter implements Filter, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private LDAPConnector ldapService;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Admin suffix. */
	private final String ADMIN_SUFFIX = ".admin";

	/** Admin prefix. */
	private final String ADMIN_PREFIX = "administration";

	/** Search prefix. */
	private final String SEARCH_PREFIX = "/content/search";

	/** Revised folder. */
	private final String REVISED_FOLDER = "/catalogo/revisados";

	/** Channel url. */
	private final String CHANNELS_URL = "/content/canales";

	/** Authentication properties. */
	private Map<String, String> authProperties = null;

	/** Global properties. */
	private Map<String, String> globalProperties = null;

	/** Downloader properties. */
	private Map<String, String> downloaderProperties = null;

	/** Uploader properties. */
	private Map<String, String> uploaderProperties = null;

	/** Searcher properties. */
	private Map<String, String> searcherProperties = null;

	/** Collections relative path. */
	private String collectionsUrl = "";

	/** Base repository directory. */
	private String baseRepoDir = "";

	/** Downloader url. */
	private String downloaderUrl = "";

	/** Downloader relative path. */
	private String downloaderPath = "";

	/** Uploader url. */
	private String uploaderUrl = "";

	public void init(FilterConfig filterConfig) {

		// Get properties
		authProperties = portalSetup.get_config_properties("/authentication");
		globalProperties = portalSetup.get_config_properties("/sling");
		downloaderProperties = portalSetup.get_config_properties("/downloader");
		uploaderProperties = portalSetup.get_config_properties("/uploader");
		searcherProperties = portalSetup.get_config_properties("/searcher");

		try {
			collectionsUrl = authProperties.get("memberof_bypath");
		} catch (Exception e) {
			collectionsUrl = "/colecciones/";
		}

		baseRepoDir = globalProperties.get("base_repo_dir");
		downloaderUrl = downloaderProperties.get("url");

		String[] downloaderPathSplited = downloaderUrl.split("://")[1].split("/");

		for (int i = 1; i < downloaderPathSplited.length; i++) {
			downloaderPath += "/" + downloaderPathSplited[i];
		}

		uploaderUrl = uploaderProperties.get("uploader_upload_api");
		ldapService.connectLdapServer();

	}

	/*
	 * Checks the request Authorization Headers and use this information to check
	 * if the users has permisions to access the requested url.
	 * @see javax.servlet.Filter
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		AuthenticationInfo credentials = authTools.extractAuthentication(request);

		SimpleCredentials authInfo = null;
		if (credentials != null)
			authInfo = (SimpleCredentials) credentials.getCredentials();

		String requestedPath = request.getRequestURI();

		if (!requestedPath.startsWith("/" + baseRepoDir)) {
			requestedPath = "/" + baseRepoDir + requestedPath;
		}

		// search permission filter
		try {
			
			if (requestedPath.contains(SEARCH_PREFIX)) {
				String searpath = request.getParameter("path");
				if (searpath != null) {
					requestedPath = searpath;
				} else {
					assert searcherProperties != null;
					assert authInfo != null;
					requestedPath = searcherProperties.get("scope") + "/"
							+ authInfo.getUserID();
				}
			}
		} catch (Exception e) {
			log.error("Error getting search permissions");
		}

		boolean allowed = false;

		// If it is a webdav access we delegate the authorization to the
		// WebDavServlet
		if (portalSetup.isWebDavMethod(request.getMethod())) {
			if (log.isInfoEnabled())
				log.info("Web dav access");
			allowed = true;
		} else {
			if (credentials != null) {
				try {
					String userId = authInfo.getUserID();
					// Is admin user?
					if  (ldapService.isMemberOf(userId, "gad/admin/cualquiercosa")) {
						allowed = true;
						
						if (log.isInfoEnabled())
							log.info("Is admin user");
						log.info("1");
					}
					// Admin. Delegate to AdminFilter
					
					else if  ((requestedPath.endsWith(ADMIN_SUFFIX))
							|| (requestedPath.startsWith("/" + baseRepoDir + "/"
									+ ADMIN_PREFIX))
							|| (requestedPath.startsWith("/" + baseRepoDir + "/apps/"
									+ ADMIN_PREFIX))){
						log.info("2");
						if (log.isInfoEnabled())
							log.info("Administration url");
						allowed = true;
					}
					// Collections
					else if (requestedPath.contains(collectionsUrl)) {
						log.info("3");
						log.info("requestedPath.contains(collectionsUrl)"+collectionsUrl+";");
						allowed = ldapService.checkUserByPath(userId, requestedPath);
						if (log.isInfoEnabled())
							log.info("Collection path: " + requestedPath);
					}
					// Channel publication
					else if (isPublicationRequest(request)) {
						log.info("4");
						log.info("Is a isPublicationRequest");
						String destChannel = request.getParameter(":dest");
						if (log.isInfoEnabled())
							log.info("Publishig from: " + requestedPath + " to: "
									+ destChannel);
						allowed = ldapService.isMemberOf(userId, requestedPath)
								&& ldapService.isMemberOf(userId, destChannel);
					}					
					
					// Rest of the repository. Checks if the user is member of
					if (!allowed) {
						allowed = ldapService.isMemberOf(userId,
								requestedPath);
						if (log.isInfoEnabled()) log.info("Check permission by path allowed: " + allowed);
					}
				} catch (java.lang.NullPointerException e) {
					log.error("Error1:"+e.getLocalizedMessage());
					log.error("Error2:"+e.getMessage());
					log.error("Error3:"+e.toString());
					log.error("Error4:"+e.getCause());
					log.error("Error5:"+e.getClass());
					log.error("Error6:"+e.getStackTrace().toString());
					log.error("doFilter, access denied");
					log.error("RequestedPath: " + requestedPath);
				
					allowed = false;
				}

			}
			// Maybe the user has a admin session cookie
			else {
				// Session cookie
				if ((requestedPath.endsWith(ADMIN_SUFFIX))
						|| (requestedPath
								.startsWith("/" + baseRepoDir + "/" + ADMIN_PREFIX))
						|| (requestedPath.startsWith("/" + baseRepoDir + "/apps/"
								+ ADMIN_PREFIX))
						|| (requestedPath.startsWith("/content/upload"))) {
					if (portalSetup
							.isBundleInstalled("org.iavante.sling.gad.administration")) {
						if (log.isInfoEnabled())
							log.info("Administration path with session cookie: "
									+ requestedPath);
						allowed = true;
					}
				}
				// Or it is a Downloader url
				else if (requestedPath.contains("/" + baseRepoDir + downloaderPath)) {
					if (portalSetup.isBundleInstalled("org.iavante.sling.gad.downloader")) {
						if (log.isInfoEnabled())
							log.info("Downloader access: " + requestedPath);
						allowed = true;
					}
				}
				// Or it is a Uploader url
				else if (requestedPath.contains("/" + baseRepoDir + uploaderUrl)) {
					if (log.isInfoEnabled())
						log.info("Uploader access: " + requestedPath);
					if (portalSetup.isBundleInstalled("org.iavante.uploader.osgi")) {
						allowed = true;
					}
				}
			}
		}
		
		/* TODO: delete, only for tests purposes */
		if (requestedPath.endsWith("crossdomain.xml") || requestedPath.endsWith("playlist.xml") || 
				requestedPath.endsWith("smil.xml") || requestedPath.endsWith("xspf.xml") ){			
			allowed = true;
			log.info("ACCEDIENDO CON UN PERMISO TEMPORAL");
		}
		
	
		
		if (allowed) { // Authorized
			if (log.isInfoEnabled())
				log.info("User with permissions to access");
			chain.doFilter(request, response);
		} else { // Unauthorized
			log.warn("User without permissions to access");
			response.setStatus(401);
			response.sendError(401);
		}
	}

	/*
	 * Does nothing. This filter has not destroyal requirement.
	 * @see javax.servlet.Filter
	 */
	public void destroy() {
	}

	// -------------------- Internal ---------------------------

	/**
	 * True or false if is a publication request Ej: curl -F":operation=copy"
	 * -F"replace=true" -F":dest=/content/canales/ies/contents/"
	 * http://<user>:<passwd
	 * >@localhost:8888/content/catalogo/revisados/contenido_test_1
	 * 
	 * @param request
	 * @return if it is a publication request or not
	 */
	private boolean isPublicationRequest(SlingHttpServletRequest request) {
		String operation = request.getParameter(":operation");
		String dest = request.getParameter(":dest");
		if ((operation==null) || (dest==null)){
			return false;
		}
		boolean temp = ((request.getRequestURI().contains(REVISED_FOLDER)))
		&& (!operation.equals("") && (dest.contains(CHANNELS_URL)));
		return ((request.getRequestURI().contains(REVISED_FOLDER)))
				&& (!operation.equals("") && (dest.contains(CHANNELS_URL)));
	}
}
