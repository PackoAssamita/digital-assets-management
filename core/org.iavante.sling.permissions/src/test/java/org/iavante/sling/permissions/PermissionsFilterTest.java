/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.permissions;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import static org.junit.Assert.*;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test search bundle
 */
public class PermissionsFilterTest {

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";
	private String ERROR1_URL = "/content/colecciones/admin";
	private String OK1_URL = "/content/colecciones/admin/";
	private String OK2_URL = "/content/colecciones/admin";
	private String ERROR2_URL = "/content/colecciones/admina";
	private String ERROR3_URL = "/content/colecciones/admina/";
	private String SEARCH_URL_OK = "/content/search?key=test&path=/content/colecciones/tests";
	private String SEARCH_URL_FAIL = "/content/search?key=test&path=/content/colecciones/tes";

	private final String CROSSDOMAIN_URL = "/crossdomain.xml";
	private Credentials defaultcreds;
	private List<String> authPrefs = new ArrayList<String>(2);

	HttpClient client;

	@org.junit.Before
	public void setUp() {
		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

	}

	@org.junit.After
	public void tearDown() {

	}

	@org.junit.Test
	public void test_Path_Credentials_OK1() {

		// Ok access

		GetMethod okaccess = new GetMethod(SLING_URL + OK1_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue(okaccess.getStatusCode() == 200
				|| okaccess.getStatusCode() == 404);
		okaccess.releaseConnection();

	}

	@org.junit.Test
	public void test_Path_Credentials_OK2() {

		// Ok access

		GetMethod okaccess = new GetMethod(SLING_URL + OK2_URL);

		okaccess.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(okaccess);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue(okaccess.getStatusCode() == 200
				|| okaccess.getStatusCode() == 404);
		okaccess.releaseConnection();

	}

	@org.junit.Test
	public void test_Path_Credentials_no_permission() {

		/*
		 * Error accessing to a user collection beginning with same user name from a
		 * external user
		 */

		GetMethod err_access = new GetMethod(SLING_URL + ERROR1_URL);

		err_access.setDoAuthentication(true);
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(err_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(401, err_access.getStatusCode());
		err_access.releaseConnection();

	}

	@org.junit.Test
	public void test_Path_Credentials_w_o_credentials() {

		/*
		 * Error accessing to a user collection without credentials
		 */

		GetMethod err_access = new GetMethod(SLING_URL + ERROR2_URL);

		err_access.setDoAuthentication(true);

		try {
			client.executeMethod(err_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue(err_access.getStatusCode() == 401
				|| err_access.getStatusCode() == 403);
		err_access.releaseConnection();

	}

	@org.junit.Test
	public void test_Path_Credentials_user_no_authenticated() {

		/*
		 * Error accessing to a user collection beginning with same user name
		 */

		GetMethod err_access = new GetMethod(SLING_URL + ERROR3_URL);

		err_access.setDoAuthentication(true);
		defaultcreds = new UsernamePasswordCredentials("asdadsa", "dasdasdfa");
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(err_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertTrue(err_access.getStatusCode() == 401
				|| err_access.getStatusCode() == 403);
		err_access.releaseConnection();

	}

	@org.junit.Test
	public void test_Search_OK() {
		GetMethod err_access = new GetMethod(SLING_URL + SEARCH_URL_OK);

		err_access.setDoAuthentication(true);
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(err_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Search OK test", 200, err_access.getStatusCode());
		err_access.releaseConnection();
	}

	@org.junit.Test
	public void test_Search_Fail() {
		GetMethod err_access = new GetMethod(SLING_URL + SEARCH_URL_FAIL);

		err_access.setDoAuthentication(true);
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(err_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Search FAIL test", 401, err_access.getStatusCode());
		err_access.releaseConnection();
	}

	@org.junit.Test
	public void test_CrossDomainAccess() {
		GetMethod cross_access = new GetMethod(SLING_URL + CROSSDOMAIN_URL);

		cross_access.setDoAuthentication(true);
		defaultcreds = new UsernamePasswordCredentials("tests", "tests");
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(cross_access);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Crossdomain access", 200, cross_access.getStatusCode());
		cross_access.releaseConnection();
	}
}