/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.cpsies.services.impl;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.cpsies.services.RegisterCategoriesService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener implementation.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Register Categories listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class RegisterCategoriesServiceImpl implements
		RegisterCategoriesService, EventListener, Serializable {

	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(RegisterCategoriesServiceImpl.class);

	/** Registered path **/
	private final String REGISTERED_PATH = "/content/canales/iesweb/contents";

	/** Categories prop **/
	private final String CAT_PROP = "categories";

	/** Save the created content rpath **/
	private String CPS_CONTENT_RPATH = "cps_rpath";

	/** CPS User */
	private String user = "";

	/** CPS Pass */
	private String pass = "";

	/** CPS Server xmlrpc entry point */
	private String cps_server_proxy = "";

	/** Root node. */
	private Node rootNode = null;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	protected void activate(ComponentContext context) {
		try {
			session = repository.loginAdministrative(null);
			rootNode = session.getRootNode();

			// Properties
			Map<String, String> cpsiesProperties = portalSetup
					.get_config_properties("/cpsies");
			this.user = cpsiesProperties.get("cpsUser");
			this.pass = cpsiesProperties.get("cpsPass");
			String url = cpsiesProperties.get("url");

			if (!url.endsWith("/")) {
				url = url + "/";
			}

			this.cps_server_proxy = url + "portal_remote_controller";

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager
						.addEventListener(this, Event.PROPERTY_ADDED
								| Event.PROPERTY_REMOVED, REGISTERED_PATH, true, null, types,
								false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {

		Node modifiedNode = null;

		while (eventIterator.hasNext()) {
			Event event = eventIterator.nextEvent();
			String propPath = "";
			try {
				propPath = event.getPath();
			} catch (RepositoryException e1) {

				e1.printStackTrace();
			}

			// Content edited
			if (event.getType() == Event.PROPERTY_REMOVED
					| event.getType() == Event.PROPERTY_ADDED) {
				if (log.isInfoEnabled())
					log.info("Content edited");
				try {

					// Property categories edited
					if (propPath.endsWith("categories")) {

						// Map ies web ategories with Ies categories
						Map<String, String> cat_cps_def = new HashMap<String, String>();
						cat_cps_def.put("Infancia", "Childhood");
						cat_cps_def.put("Jóvenes", "Youngs");
						cat_cps_def.put("Adultos", "Adults");
						cat_cps_def.put("Mayores", "Seniors");
						cat_cps_def.put("Nutrición", "Nutrition");
						cat_cps_def.put("Corazón", "Heart");
						cat_cps_def.put("Deportes", "Sport");
						cat_cps_def.put("Diabetes", "Diabetes");
						cat_cps_def.put("Embarazo", "Pregnancy");
						cat_cps_def.put("Cancer", "Cancer");
						cat_cps_def.put("Tabaquismo", "Tobacco");
						cat_cps_def.put("Obesidad", "Obesity");
						cat_cps_def.put("Sabía que...", "DidYouKnow");
						cat_cps_def.put("Recetas", "Prescription");
						cat_cps_def.put("Campañas ", "Campaign");
						cat_cps_def.put("Consejos", "Advices");
						cat_cps_def.put("Quiz salud", "HealthQuiz");
						cat_cps_def.put("Novedades", "Recent");
						cat_cps_def.put("Reportajes", "Documentaries");
						cat_cps_def.put("Ejercicio físico", "PhysicalExercise");
						cat_cps_def.put("Cuidados del bebé", "BabyCare");
						cat_cps_def.put("Vacunas", "Vaccine");
						cat_cps_def.put("Cuestión de salud", "HealthQuestion");
						cat_cps_def.put("Recetas equilibradas", "BalancedRecipes");
						cat_cps_def.put("Emergencias", "Emergencies");
						cat_cps_def.put("Relajación", "Relaxation");
						cat_cps_def.put("Salud Bucodental", "OralHealth");
						cat_cps_def.put("Arte corporal", "CorporealArt");
						cat_cps_def.put("Vida saludable", "HealthyLife");
						cat_cps_def.put("VIH", "VIH");
						cat_cps_def.put("Donación", "Donation");
						cat_cps_def.put("Genética", "Genetica");
						cat_cps_def.put("Salud en verano", "HealthSummer");
						cat_cps_def.put("Código europeo contra el cáncer",
								"EuropeanCodeAgainstCancer");
						cat_cps_def.put("A tu salud", "ToYourHealthness");

						if (log.isInfoEnabled())
							log.info("Categories edited");
						int pos = propPath.lastIndexOf('/');
						String nodePath = propPath.substring(0, pos);
						modifiedNode = rootNode.getNode(nodePath.substring(1));
						String content_rpath = modifiedNode.getProperty(CPS_CONTENT_RPATH)
								.getValue().getString();

						String[] cat_titles = new String[0];

						if (modifiedNode.hasProperty(CAT_PROP)) {
							Value[] categories = modifiedNode.getProperty(CAT_PROP)
									.getValues();
							cat_titles = new String[categories.length];
							for (int i = 0; i < categories.length; i++) {
								String category_path = categories[i].getString();
								if (log.isInfoEnabled())
									log.info("Category edited: " + category_path);
								String cat_title = rootNode.getNode(category_path.substring(1))
										.getProperty("title").getValue().getString();
								cat_titles[i] = cat_cps_def.get(cat_title);
							}
						}

						// Configure the xmlrpcclient
						XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
						config.setBasicUserName(user);
						config.setBasicPassword(pass);

						try {
							if (log.isInfoEnabled())
								log.info("Proxy: " + cps_server_proxy);
							config.setServerURL(new URL(cps_server_proxy));
						} catch (MalformedURLException e) {

							e.printStackTrace();
						}

						XmlRpcClient client = new XmlRpcClient();
						client.setConfig(config);

						// Edit the content with modified categories
						Map<String, Object> doc_def = new HashMap<String, Object>();
						doc_def.put("Subject", cat_titles);

						Object[] params = new Object[] { content_rpath, doc_def };

						try {
							String result = (String) client.execute("editDocument", params);
							if (log.isInfoEnabled())
								log.info("Content edited: " + content_rpath);
						} catch (XmlRpcException e) {

							e.printStackTrace();
						}

						// Publish the content
						Map<String, String> rpaths_to_publish = new HashMap<String, String>();
						rpaths_to_publish.put("sections/videos", "");

						Object[] param_publish = new Object[] { new String(content_rpath),
								rpaths_to_publish };
						try {
							if (log.isInfoEnabled())
								log.info("Publishing content");
							String rpath_published_content = (String) client.execute(
									"publishDocument", param_publish);
							if (log.isInfoEnabled())
								log.info("Content published at " + rpath_published_content);
						} catch (XmlRpcException e) {

							e.printStackTrace();
						}
					}
				} catch (RepositoryException e) {

					e.printStackTrace();
				}
			}
		}
	}
}
