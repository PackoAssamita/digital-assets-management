/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.catalog.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.gad.catalog.PendingService;
import org.iavante.sling.gad.content.ContentTools;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Catalog Pending listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class PendingServiceImpl implements PendingService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private ContentTools contentTools;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** Schema file name. */
	private final String SCHEMA_FILE_NAME = "schema.smil";

	/** Sources folder name. */
	private final String SOURCES_FOLDER = "sources";

	/** Schema file name. */
	private final String SCHEMA_FILE = "schema.smil";

	/** Registered JCR path. */
	private final String REGISTERED_PATH = "/content/catalogo/pendientes";

	/** Content sling:resourceTypes. */
	private final String CONTENT_RESOURCE_TYPE = "gad/content";
	private final String SOURCE_RESOURCE_TYPE = "gad/source";
	private final String REVISION_RESOURCE_TYPE = "gad/revision";

	/** Property to save the original content path. */
	private final String PATH_PROP = "jcr_path";

	/** Log node. */
	private final String LOG_NODE = "log";

	/** Property to save the referencial content. */
	private final String CONTENT_REF_PROPERTY = "content_ref";

	/** State of the log node. */
	private final String STATE = "pending";

	/** Mark for playlist in the schema. */
	private String methodPlaylist = "#@playlist@#";

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(PendingServiceImpl.class);

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED,
						REGISTERED_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));

					log("Node added: " + addedNode.getPath().toString());
					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getString().equals(
								CONTENT_RESOURCE_TYPE)) {

							// Change sling:resourceType to revision
							addedNode.setProperty("sling:resourceType",
									REVISION_RESOURCE_TYPE);
							// Add votes property
							String[] votes = {};
							addedNode.setProperty("votes", votes);
							// Add tvicon and ending flags
							addedNode.setProperty("needEnding", "1");
							addedNode.setProperty("needTvicon", "1");
							addedNode.save();

							// Get log node in reference content
							log("Added node: " + addedNode.getPath());
							String dest_path = addedNode.getPath();
							String ref_content_path = addedNode.getProperty(PATH_PROP)
									.getString();
							Node ref_content_node = session.getRootNode().getNode(
									ref_content_path.substring(1));

							// Update the log node with the new state
							gadContentTool.updateLogNode(ref_content_node, STATE, dest_path);
							session.save();

							// Set static smil if the associated schema is a playlist
							// Get sources in schema and remove if is not in schema or is not
							// a playlist

							if (schemaIsPlaylist(addedNode) == true) {

								Node schemanode = addedNode.getNode(SOURCES_FOLDER).getNode(
										"schema.smil");

								InputStream is = null;
								try {
									is = new ByteArrayInputStream(contentTools.getPlaylist(
											addedNode).getBytes("UTF-8"));
								} catch (UnsupportedEncodingException e) {
									e.printStackTrace();
									log.error(e.toString());
								}
								schemanode.getNode("jcr:content").setProperty("jcr:data", is);
							} else {

								ArrayList<String> sources_in_schema = getSourcesinSchema(addedNode);

								for (Iterator iter = addedNode.getNode(SOURCES_FOLDER)
										.getNodes(); iter.hasNext();) {
									Node source = (Node) iter.next();
									if (source.hasProperty("sling:resourceType")) {
										if (source.getProperty("sling:resourceType").getString()
												.equals(SOURCE_RESOURCE_TYPE)) {
											String source_name = SOURCES_FOLDER + "/"
													+ source.getName();

											if (!sources_in_schema.contains(source_name)) {
												source.remove();
												log.error("Remove source not in schema");
											}
										}
									}
								}
							}
							session.save();
						}
					}

				} catch (RepositoryException e) {
					e.printStackTrace();
					log.error(e.toString());
				}
			}
		}
	}

	// --------------------- Internal ------------------------

	/**
	 * Returns the sources present in the schema file
	 * 
	 * @param node
	 * @return Sources present in the schema
	 */
	private ArrayList<String> getSourcesinSchema(Node node) {

		String schemaFile = "";
		ArrayList<String> sourcesInSchema = new ArrayList<String>();

		try {
			Node schema = node.getNode(SOURCES_FOLDER).getNode(SCHEMA_FILE).getNode(
					"jcr:content");

			InputStream fileStream = null;
			fileStream = schema.getProperty("jcr:data").getStream();
			schemaFile = convertStreamToString(fileStream);

		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Pattern sourcePattern = Pattern.compile("\\[\\[.*\\]\\]");
		Matcher sourceMatcher = sourcePattern.matcher(schemaFile);

		while (sourceMatcher.find()) {
			String sourcePath = sourceMatcher.group().split(":")[0].substring(2);
			sourcesInSchema.add(sourcePath);
		}

		return sourcesInSchema;
	}

	/**
	 * Converts a Stream to String
	 * 
	 * @param is
	 *          The input stream
	 * @return The converted string
	 */
	private String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	/**
	 * @param node
	 *          the node to get the schema
	 * @return true if the schema of node has the playlist tag
	 */
	private boolean schemaIsPlaylist(Node node) {
		log.info("CONTENT_TOOLS");
		String schema = contentTools.getContentSchema(node);

		log.info(schema);

		return schema.contains(methodPlaylist);
	}
}
