/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.authentication;

import java.io.IOException;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.engine.auth.AuthenticationHandler;
import org.apache.sling.engine.auth.AuthenticationInfo;

import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.PortalSetup;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The <code>GADAuthenticationHandler</code> class implements the authentication
 * steps based on the authentication header of the HTTP request.
 * 
 * @scr.component immediate="false" label="%auth.http.name"
 *                description="%auth.http.description"
 * @scr.property name="service.description"
 *               value="IAVANTE HTTP Header Authentication Handler"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property nameRef="AuthenticationHandler.PATH_PROPERTY" value="/"
 * @scr.service
 * 
 * @author IAVANTE
 */
public class GADAuthenticationHandler implements AuthenticationHandler {

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	// ----------- GAD needed variables -----------------------------

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Downloader properties. */
	Map<String, String> slingProperties = null;

	// ----------- AuthenticationHandler variables -----------------------------
	/** @scr.property valueRef="DEFAULT_REALM" */
	public static final String PAR_REALM_NAME = "auth.http.realm";

	private static final String HEADER_WWW_AUTHENTICATE = "WWW-Authenticate";
	private static final String HEADER_AUTHORIZATION = "Authorization";
	private static final String AUTHENTICATION_SCHEME_BASIC = "Basic";
	private static final String DEFAULT_REALM = "GAD2 (Development)";

	private String realm = DEFAULT_REALM;

	/**
	 * The special user, whose credentials are always ignored to enable logging
	 * the user out (value is "__forced_logout_user__"). This user id is sent by
	 * the {@link LoginServlet} to indicate the request to log out (only from
	 * non-IE browsers).
	 */
	static final String NOT_LOGGED_IN_USER = "__forced_logout_user__";

	// ----------- Constructors -----------------------------
	public GADAuthenticationHandler() {
		if (log.isInfoEnabled())
			log.info("GADAuthenticationHandler creado");
	}

	// ---------- SCR Integration ----------------------------------------------
	protected void activate(ComponentContext componentContext) {
		if (log.isInfoEnabled())
			log.info("activate, ini");
		slingProperties = portalSetup.get_config_properties("/sling");
		if (log.isInfoEnabled())
			log.info("activate, fin");
	}

	// ----------- AuthenticationHandler interface -----------------------------
	/**
	 * Extracts credential data from the request if at all contained. This check
	 * is only based on the original request object, no URI translation has taken
	 * place yet.
	 * <p>
	 * The method returns any of the following values :
	 * <table>
	 * <tr>
	 * <th>value
	 * <th>description
	 * </tr>
	 * <tr>
	 * <td><code>null</code>
	 * <td>no user details were contained in the request
	 * </tr>
	 * <tr>
	 * <td>{@link AuthenticationInfo#DOING_AUTH}
	 * <td>the handler is in an ongoing authentication exchange with the client.
	 * The request handling is terminated.
	 * <tr>
	 * <tr>
	 * <td>valid credentials
	 * <td>The user sent credentials.
	 * </tr>
	 * </table>
	 * <p>
	 * The method must not request credential information from the client, if they
	 * are not found in the request.
	 * <p>
	 * Note : The implementation should pay special attention to the fact, that
	 * the request may be for an included servlet, in which case the values for
	 * some URI specific values are contained in javax.servlet.include.* request
	 * attributes.
	 * 
	 * @param request
	 *          The request object containing the information for the
	 *          authentication.
	 * @param response
	 *          The response object which may be used to send the information on
	 *          the request failure to the user.
	 * @return A valid Credentials instance identifying the request user,
	 *         DOING_AUTH if the handler is in an authentication trasaction with
	 *         the client or null if the request does not contain authentication
	 *         information. In case of DOING_AUTH, the method must have sent a
	 *         response indicating that fact to the client.
	 */
	public AuthenticationInfo authenticate(HttpServletRequest request,
			HttpServletResponse response) {

		// extract credentials and return
		AuthenticationInfo info = authTools.extractAuthentication(request);
		if (info != null) {

			SimpleCredentials sc = (SimpleCredentials) info.getCredentials();
			String slingUser = slingProperties.get("sling_user");
			String gadUser = slingProperties.get("gad_user");
			String gadPass = slingProperties.get("gad_pass");

			// if 'admin' or 'anonymous' do nothing, otherwise do swap for 'gaduser'
			if (slingUser.compareTo(sc.getUserID()) != 0
					&& "anonymous".compareTo(sc.getUserID()) != 0) {
				if (log.isInfoEnabled())
					log.info("authenticate, user=" + sc.getUserID()
							+ " is logging in JCR with user=" + gadUser);
				return new AuthenticationInfo(HttpServletRequest.BASIC_AUTH,
						new SimpleCredentials(gadUser, gadPass.toCharArray()));
			}

			if (log.isInfoEnabled())
				log.info("authenticate, user: " + sc.getUserID());

			return info;
		}

		/*
		 * THIS SECTION WAS ADDED IN SLING5 // no credentials, check whether the
		 * client wants to login if (forceAuthentication(request, response)) {
		 * return AuthenticationInfo.DOING_AUTH; }
		 */

		if (log.isInfoEnabled())
			log.info("authenticate, no credentials, setting user: anonymous");
		return new AuthenticationInfo(HttpServletRequest.BASIC_AUTH,
				new SimpleCredentials("anonymous", "anonymous".toCharArray()));

	}

	/**
	 * Sends back the form to log into the system.
	 * 
	 * @param request
	 *          The request object
	 * @param response
	 *          The response object to which to send the request
	 * @return <code>true</code> is always returned by this handler
	 * @throws IOException
	 *           if an error occurrs sending back the form.
	 */
	public boolean requestAuthentication(HttpServletRequest arg0,
			HttpServletResponse arg1) throws IOException {

		// if the response is already committed, we have a problem !!
		if (!arg1.isCommitted()) {

			arg1.setHeader(HEADER_WWW_AUTHENTICATE, AUTHENTICATION_SCHEME_BASIC
					+ " realm=\"" + this.realm + "\"");

			arg1.sendError(HttpServletResponse.SC_FORBIDDEN);

		} else {
			log.error("requestAuthentication: Response is committed, cannot request authentication");
		}

		return true;
	}

}
