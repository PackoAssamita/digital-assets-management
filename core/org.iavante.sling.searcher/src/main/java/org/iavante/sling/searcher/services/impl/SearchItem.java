/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services.impl;

import java.io.Serializable;

/**
 * Represents a search item.
 */
public class SearchItem implements Serializable {
	private static final long serialVersionUID = 1L;

	/** The search string of this item */
	public String searchString = null;

	/** If is a search in a property */
	private int field = -1;

	/** If contains a literal between " */
	private int literal = -1;

	// ---------------- Constructors ----------------
	public SearchItem() {
	}

	public SearchItem(String searchstr) {
		searchString = searchstr;
	}

	// ------------ Getters and setters -------------
	public void setField(boolean bool) {
		if (bool == true)
			field = 1;
		else
			field = 0;
	}

	public void setLiteral(boolean bool) {
		if (bool == true)
			literal = 1;
		else
			literal = 0;
	}

	public boolean getField() {
		if (field == 1)
			return true;
		else {
			if (field == 0) {
				return false;
			} else {
				/* if is not defined */
				if ((searchString.indexOf(":") != -1
						&& searchString.indexOf(":") < searchString.indexOf("\"") && searchString
						.indexOf("\"") != -1)
						|| //
						(searchString.indexOf(":") != -1 && searchString.indexOf("\"") == -1)) {
					field = 1;
					return true;
				} else {
					field = 0;
					return false;
				}
			}
		}

	}

	public boolean getLiteral() {
		if (literal == 1)
			return true;
		else {
			if (literal == 0) {
				return false;
			} else {
				if (searchString.indexOf("\"") != -1) {
					literal = 1;
					return true;
				} else {
					literal = 0;
					return false;
				}

			}
		}
	}

	@Override
	public String toString() {
		return searchString;
	}
}
