/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services.impl;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.iavante.sling.searcher.services.SearchPrintService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.searcher.services.SearchPrintService
 * @scr.component
 * @scr.service 
 *              interface="org.iavante.sling.searcher.services.SearchPrintService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="service.description" value="Search print service"
 * 
 */
public class SearchPrintServiceImpl implements SearchPrintService, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	// -------------- Default constructor ---------------
	public SearchPrintServiceImpl() {
		if (log.isInfoEnabled())
			log.info("SearcherPrintService: constructor");
	}

	/**
	 * Activate the component in the OSGI.
	 * 
	 * @param context
	 *          component framework execution context
	 */
	protected void activate(ComponentContext context) {
		log.info("SearchPrintService: Service activated");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.iavante.sling.searcher.services.SearchPrintService#writeOutput(java
	 * .lang.String, java.util.ArrayList, java.io.PrintWriter, int)
	 */
	public void writeOutput(String format,
			ArrayList<HashMap<String, String>> result, PrintWriter printer, int rows) {

		if (format.contentEquals("json")) {
			SearcherJsonWriter jwriter = new SearcherJsonWriter(result, printer, rows);
			jwriter.writeOutput();

		} else if (format.contentEquals("xml")) {
			SearcherXmlWriter xwriter = new SearcherXmlWriter(result, printer, rows);
			xwriter.writeOutput();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.iavante.sling.searcher.services.SearchPrintService#writeSimpleOutput
	 * (java.lang.String, java.util.ArrayList, java.io.PrintWriter, int)
	 */
	public void writeSimpleOutput(String format,
			ArrayList<HashMap<String, String>> result, PrintWriter printer, int rows) {

		if (format.contentEquals("json")) {
			SearcherJsonWriter jwriter = new SearcherJsonWriter(result, printer, rows);
			jwriter.writeSimpleOutput();

		} else if (format.contentEquals("xml")) {
			SearcherXmlWriter xwriter = new SearcherXmlWriter(result, printer, rows);
			xwriter.writeOutput();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.iavante.sling.searcher.services.SearchPrintService#getContentType(java
	 * .lang.String)
	 */
	public String getContentType(String format) {
		if (format.contentEquals("json")) {
			return "application/json";

		} else if (format.contentEquals("xml")) {
			return "text/xml";
		}
		return "";
	}
}
