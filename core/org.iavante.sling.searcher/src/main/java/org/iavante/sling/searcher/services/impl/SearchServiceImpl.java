/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.content.ContentTools;
import org.iavante.sling.searcher.services.SearchService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.searcher.services.SearchService
 * @scr.component
 * @scr.service interface="org.iavante.sling.searcher.services.SearchService"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="service.description" value="Search service"
 * 
 */
public class SearchServiceImpl implements SearchService, Serializable {
	private static final long serialVersionUID = 1L;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private ContentTools contentTools;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** Search clause. */
	public static final String STATEMENT = "statement";

	/** Query type, sql or xpath. */
	private final String queryType = Query.SQL;

	/** Property to append to the result */
	public static final String PROPERTY = "property";

	/** Excerpt lookup path. */
	public static final String EXCERPT_PATH = "excerptPath";

	/** Keymap for keys in search. */
	private static Map<String, String> mapkeys = null;

	/** Search query foot. */
	private final String QUERYFOOT = " ) ORDER BY jcr:score DESC";

	/** Search query head. */
	private String queryhead = null;

	/** Offset of result. */
	private int offsetDefault = 0;

	/** Array with the scope of search. */
	private String[] scopes;

	/** Array with the hidden properties of nodes. */
	private String[] hiddenproperties;

	/** Max results in fullinfo mode. */
	private int maxFullinfoElements;

	/** TEMPORAL */
	private String scope;

	/** Config properties. */
	private Map<String, String> properties;

	// -------------- Default constructor ---------------
	public SearchServiceImpl() {
		if (log.isInfoEnabled())
			log.info("SearchServiceImpl:  constructor");
	}

	/**
	 * Activate the component in the OSGI.
	 * 
	 * @param context
	 *          component framework execution context
	 */
	protected void activate(ComponentContext context) {
		log.info("SearchServiceImpl: Servicio activandose");

		/* get properties from config */
		properties = portalSetup.get_config_properties("/searcher");
		assert properties.containsKey("scope") == true;
		scope = properties.get("scope");
		assert properties.containsKey("hiddenproperties") == true;
		hiddenproperties = properties.get("hiddenproperties").split(",");
		maxFullinfoElements = Integer.valueOf(
				properties.get("max_fullinfo_elements")).intValue();
		scopes = scope.split(",");
		log.info("SearchServiceImpl: Servicio activado");
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo) throws ValueFormatException,
			IllegalStateException, RepositoryException {
		String[] paths = { path };

		return internalSearch(searchstring, paths, null, fullinfo, 0, -1, null,
				null, null, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String[] path, boolean fullinfo) throws ValueFormatException,
			IllegalStateException, RepositoryException {

		return internalSearch(searchstring, path, null, fullinfo, 0, -1, null,
				null, null, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String[] path, boolean fullinfo, String operator)
			throws ValueFormatException, IllegalStateException, RepositoryException {

		return internalSearch(searchstring, path, null, fullinfo, 0, -1, null,
				null, operator, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo, int offset, int items, String order,
			String order_by) throws ValueFormatException, IllegalStateException,
			RepositoryException {
		String[] paths = { path };
		return internalSearch(searchstring, paths, null, fullinfo, offset, items,
				order, order_by, null, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo, int offset, int items, String order,
			String order_by, String operator, double related)
			throws ValueFormatException, IllegalStateException, RepositoryException {
		String[] paths = { path };
		return internalSearch(searchstring, paths, null, fullinfo, offset, items,
				order, order_by, operator, related);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring)
			throws ValueFormatException, IllegalStateException, RepositoryException {

		return internalSearch(searchstring, scopes, null, false, -1, -1, null,
				null, null, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String operator) throws ValueFormatException, IllegalStateException,
			RepositoryException {

		return internalSearch(searchstring, scopes, null, false, -1, -1, null,
				null, operator, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType, boolean fullinfo)
			throws ValueFormatException, IllegalStateException, RepositoryException {
		return internalsearchFilteredByResourceType(searchstring, scopes,
				resourceType, fullinfo, null);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType) throws ValueFormatException,
			IllegalStateException, RepositoryException {
		return internalsearchFilteredByResourceType(searchstring, scopes,
				resourceType, false, null);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String path, String resourceType, boolean fullinfo)
			throws ValueFormatException, IllegalStateException, RepositoryException {
		String[] paths = { path };
		return internalsearchFilteredByResourceType(searchstring, paths,
				resourceType, fullinfo, null);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType, int fullinfo)
			throws ValueFormatException, IllegalStateException, RepositoryException {
		return internalsearchFilteredByResourceType(searchstring, scopes,
				resourceType, true, fullinfo, -1, null, null, null, -1);
	}

	/*
	 * @see org.iavante.sling.searcher.services.SearchService
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String path, String resourceType, boolean fullinfo,
			int offset, int items, String order, String orderBy, String operator,
			double related) throws ValueFormatException, IllegalStateException,
			RepositoryException {
		String[] paths = { path };
		return internalsearchFilteredByResourceType(searchstring, paths,
				resourceType, fullinfo, offset, items, order, orderBy, operator,
				related);
	}

	// ------------------ Internal ------------------------

	/**
	 * Make the query to JCR. order_by not implemented
	 * 
	 * @param object
	 *          string to parse to get the query
	 * @return a ArrayList<HashMap<String,String>> with the query results
	 * @throws RepositoryException
	 */
	private ArrayList<HashMap<String, String>> internalSearch(String object,
			String[] path, String filter, boolean fullinfo, int exoffset, int items,
			String order, String order_by, String logical_operator, double related)
			throws RepositoryException {

		// Get resolver component to execute the query
		Session session = null;
		boolean ordering = false;
		boolean ascending = false;
		int offset = 0;
		int maxItems = maxFullinfoElements;
		session = repository.loginAdministrative(null);

		if (items >= 0) {
			maxItems = items;
		}

		if (exoffset < 0)
			offset = offsetDefault;
		else
			offset = exoffset;

		assert path.length > 0;

		if (order != null) {
			ordering = true;
			if (order.compareToIgnoreCase("ascending") == 0)
				ascending = true;
		} else
			ordering = false;

		queryhead = makeQueryHead();

		/*
		 * while the query doesn't support the path with multiple paths we make a
		 * query for each scope and merge then.
		 */
		ArrayList<String> scopestring = new ArrayList<String>();
		for (String scp : path) {
			scopestring.add(makeQueryString(queryhead + getScopeString(scp) + " ( ",
					object, logical_operator, related));
		}
		// create query string, getScopeString return the concern to paths

		// execute the querys
		ArrayList<RowIterator> rowit = new ArrayList<RowIterator>();
		ArrayList<String[]> rowcolumns = new ArrayList<String[]>();
		QueryResult qresults;
		for (String q : scopestring) {
			qresults = session.getWorkspace().getQueryManager().createQuery(q,
					queryType).execute();
			rowit.add(qresults.getRows());
			rowcolumns.add(qresults.getColumnNames());
		}

		// merge results
		ArrayList<HashMap<String, String>> querysout = new ArrayList<HashMap<String, String>>();
		RowIterator row = null;
		String[] col = null;

		Row r = null;

		/* for iterate throw the result rows to merge */
		Iterator<RowIterator> itr = rowit.iterator();
		/* for iterate throw the columns of results to merge */
		Iterator<String[]> itc = rowcolumns.iterator();

		HashMap<String, String> resultcount = new HashMap<String, String>();

		while (itr.hasNext()) {

			row = itr.next();
			col = itc.next();
			while (row.hasNext()) {

				HashMap<String, String> dict = new HashMap<String, String>();
				r = row.nextRow();
				for (int k = 0; k < col.length; k++) {
					try {
						if (hiddenproperty(col[k]) == true)
							continue;

						if ("sling:resourceType".compareToIgnoreCase(col[k]) == 0
								&& r.getValue("sling:resourceType") == null) {
							if (!dict.containsKey("resourceType"))
								dict.put(renameparam(col[k]), "gad/generic-data");
						} else if ("jcr:path".compareToIgnoreCase(col[k]) == 0
								&& r.getValue("jcr:path").getString().endsWith("jcr:content")) {
							dict.put(renameparam(col[k]), r.getValue(col[k]).getString()
									.replace("/body.txt/jcr:content", ""));
							if (dict.containsKey("resourceType")) {
								dict.remove("resourceType");
							}
							dict.put("resourceType", "gad/source");
						} else {
							dict.put(renameparam(col[k]), r.getValue(col[k]).getString());
						}
					} catch (java.lang.NullPointerException ex) {
						ex.printStackTrace();
						log.error("SearchServiceImpl: "+ex.getMessage());
					}
				}
				querysout.add(dict);
			}
		}

		/* results_out will save the output results */
		ArrayList<HashMap<String, String>> resultsOut = null;

		if (filter == null) {
			/*
			 * has no filter in resourceType
			 */
			resultsOut = querysout;
		} else {

			String pat = null;
			String res = null;
			ArrayList<String> paths = new ArrayList<String>();

			ArrayList<HashMap<String, String>> listout = new ArrayList<HashMap<String, String>>();
			Iterator<HashMap<String, String>> qiet = querysout.iterator();

			while (qiet.hasNext()) {
				HashMap<String, String> rowet = qiet.next();

				res = rowet.get("resourceType");
				pat = rowet.get("path");

				if (res.contentEquals(filter)) {
					/*
					 * the result has the same resourceType of the filter and can't be
					 * repeated
					 */
					if (isInList(paths, pat) == false) {
						listout.add(rowet);
						paths.add(pat);

					}
				} else {
					String sub = pat.substring(0, pat.lastIndexOf("/")).substring(1);

					/*
					 * the next node is the parent of path returned in the row
					 */
					Node nodete = session.getRootNode().getNode(sub);

					for (int i = 0; i <= sub.split("/").length - 1; i++) {
						try {
							if (nodete.getProperty("sling:resourceType").getString()
									.contentEquals(filter) == true) {
								if (isInList(paths, nodete.getPath()) == false) {
									HashMap<String, String> dict = new HashMap<String, String>();

									dict.put("resourceType", nodete.getProperty(
											"sling:resourceType").getString());
									dict.put("path", nodete.getPath());
									dict.put("extract", rowet.get("extract"));
									listout.add(dict);
									paths.add(nodete.getPath());

								}
								break;
							} else {
								nodete = nodete.getParent();
							}
						} catch (ValueFormatException e) {
							log.error("SearchServiceImpl: ValueFormat exception: " + e.getMessage());

						} catch (PathNotFoundException e) {
							log.error("SearchServiceImpl: PathNotFound exception: " + e.getMessage());
							nodete = nodete.getParent();

						} catch (RepositoryException e) {
							log.error("SearchServiceImpl: Repository exception: " + e.getMessage());
						}
					}
				}
			}

			resultsOut = listout;
		}

		/* filter the number os results */
		int begin = offset;
		int end = 0;
		int totalcounter = 0;

		if (resultsOut.size() - (begin + maxItems) <= 0) {
			end = resultsOut.size();
		} else {
			end = begin + maxItems;
		}
		resultsOut = new ArrayList<HashMap<String, String>>(resultsOut.subList(
				begin, end));
		totalcounter = end - begin;

		/* if is set fullinfo: */

		if (fullinfo) {
			/* results_out will save the output results with fullinfo */
			ArrayList<HashMap<String, String>> resultsOutFull = new ArrayList<HashMap<String, String>>();
			Iterator<HashMap<String, String>> fullit = resultsOut.iterator();

			while (fullit.hasNext()) {
				HashMap<String, String> result = fullit.next();

				String pathfull = result.get("path");
				String sub = pathfull.substring(1);
				Node nodefull = session.getRootNode().getNode(sub);

				PropertyIterator propit = nodefull.getProperties();
				HashMap<String, String> prop = new HashMap<String, String>();
				/* add path */
				prop.put(renameparam("path"), pathfull);
				
				/* add thumbnail */
				try {
					String thumburl = contentTools.getThumbUrl(nodefull);
					prop.put(renameparam("thumbnail"), thumburl);
				} catch (Exception e) {
				}
				
				/* add GIF url */
				try{
					String urlGif = contentTools.getExternalUrl(nodefull, "thumbnail_preview_3");
					prop.put(renameparam("urlGif"), urlGif);
				}catch (Exception e){
					log.error("Searcher: internalSearch, getting GIF external url: "+e.getMessage());
				}
				
				boolean isrevision = false;
				while (propit.hasNext()) {
					Property pr = (Property) propit.next();
					@SuppressWarnings("unused")
					int l = 1;
					boolean mixintype = false;
					if (hiddenproperty(pr.getName()) == true)
						continue;
					try {
						if (pr.getDefinition().isMultiple())
							mixintype = true;
						else
							mixintype = false;
					} catch (Exception e) {
						mixintype = false;
					}
					if (mixintype == true) {
						try {
							Value[] v = pr.getValues();
							String vs = v[0].getString();
							for (int k = 1; k < v.length; k++)
								vs += "," + v[k].getString();
							prop.put(renameparam(pr.getName()), vs);
						} catch (Exception io) {
						}
					} else {
						prop.put(renameparam(pr.getName()), pr.getString());
						/* for publishing status save the resourcetype */
						if ("sling:resourceType".compareToIgnoreCase(pr.getName()) == 0
								&& "gad/revision".compareToIgnoreCase(pr.getString()) == 0)
							isrevision = true;
					}
				}
				/* add publishing status */
				try {
					if (isrevision) {
						String id = nodefull.getProperty("gaduuid").getString();
						String channels = "";
						NodeIterator chit = nodefull.getSession().getRootNode().getNode(
								"content").getNode("canales").getNodes();
						Node channel;
						while (chit.hasNext()) {
							channel = chit.nextNode();
							NodeIterator conit = channel.getNode("contents").getNodes();
							while (conit.hasNext()) {
								if (conit.nextNode().getProperty("gaduuid").getString()
										.compareTo(id) == 0) {
									channels = "&&" + "/content/canales/" + channel.getName()
											+ "&" + channel.getProperty("title").getString();
									break;
								}
							}
						}
						prop.put("channels", channels);
					}

				} catch (Exception e) {
					log.error("SearchServiceImpl: Exception accessing to revision publishing state: "
							+ e.getMessage());
				}
				resultsOutFull.add(prop);
			}
			resultsOut = resultsOutFull;
		}

		/* ordering ascending descending */
		if (ordering && ascending)
			resultsOut = get_ordering_ascending(resultsOut);

		/* add at beginning the number of results */
		resultcount.put("count", Integer.valueOf(totalcounter).toString());
		resultsOut.add(0, resultcount);
		return resultsOut;
	}

	/**
	 * Make an internal search filtered by resource type.
	 * 
	 * @param searchstring
	 * @param path
	 * @param resourceType
	 * @param fullinfo
	 * @param offset
	 * @param items
	 * @param order
	 * @param order_by
	 * @return
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	private ArrayList<HashMap<String, String>> internalsearchFilteredByResourceType(
			String searchstring, String[] path, String resourceType,
			boolean fullinfo, int offset, int items, String order, String order_by,
			String operator, double related) throws ValueFormatException,
			IllegalStateException, RepositoryException {
		return internalSearch(searchstring, path, resourceType, fullinfo, offset,
				items, order, order_by, operator, related);
	}

	/**
	 * Make an internal search filtered by resource type.
	 * 
	 * @param searchstring
	 * @param path
	 * @param resourceType
	 * @param fullinfo
	 * @return
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	private ArrayList<HashMap<String, String>> internalsearchFilteredByResourceType(
			String searchstring, String[] path, String resourceType,
			boolean fullinfo, String operator) throws ValueFormatException,
			IllegalStateException, RepositoryException {
		return internalSearch(searchstring, path, resourceType, fullinfo, -1, -1,
				null, null, operator, -1);
	}

	/**
	 * Returns the inverse array.
	 * 
	 * @param resultsOut
	 *          the Array to inverse
	 * @return an inverse array of original
	 */
	private ArrayList<HashMap<String, String>> get_ordering_ascending(
			ArrayList<HashMap<String, String>> resultsOut) {

		ArrayList<HashMap<String, String>> resultsOutAscending = new ArrayList<HashMap<String, String>>();
		int length = resultsOut.size();
		for (int i = 1; i <= length; i++)
			resultsOutAscending.add(resultsOut.get(length - i));

		return resultsOutAscending;
	}

	/**
	 * Returns true if the property must not be showed
	 * 
	 * @param property
	 *          the key of the value
	 * @return true or false depending if the parameter has to be showed
	 */
	private boolean hiddenproperty(String property) {
		for (int i = 0; i < hiddenproperties.length; i++) {
			if (hiddenproperties[i].compareToIgnoreCase(property) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Rename the JCR name to a new humanize name
	 * 
	 * @param name
	 *          to change
	 * @return the changed or not name
	 */
	private String renameparam(String name) {
		String rename = name;
		if (name.compareToIgnoreCase("jcr:path") == 0) {
			rename = "path";
		} else if (name.compareToIgnoreCase("sling:resourceType") == 0) {
			rename = "resourceType";
		} else if (name.contains("excerpt") == true) {
			rename = "extract";
		} else if (name.contains("jcr:") == true) {
			rename = name.split(":")[1];
		}

		return rename;
	}

	/**
	 * Returns the beginning of the search query, just before the where statements
	 * 
	 * @return
	 */
	private String makeQueryHead() {
		String queryhead = null;
		queryhead = "select excerpt(.),sling:resourceType from nt:base where ";

		return queryhead;
	}

	/**
	 * Generate the substring for query depending of the defined scope.
	 * 
	 * @param path
	 * @return a substring of the query
	 */
	private String getScopeString(String path) {
		String scopestring = "", lpath = path;
		if (!lpath.endsWith("/"))
			lpath += "/";
		scopestring += "jcr:path LIKE '" + lpath + "%'";
		return "(" + scopestring + ") AND ";
	}

	/**
	 * Extract from a string the search tokens prepared to insert into the query
	 * string.
	 * 
	 * @param phrases
	 *          the string with the literals
	 * @return a list of search tokens prepared to insert into query string
	 */
	private List<SearchItem> getLiteralsKeyPhrases(String phrases) {
		List<SearchItem> list = new ArrayList<SearchItem>();

		String rest = phrases;
		int index = -1, index2 = -1;
		SearchItem item = null;
		try {
			while (true) {
				rest = rest.trim();

				if (rest.startsWith("\"") == true) {
					index = rest.trim().indexOf("\"") + 1;
					index2 = rest.indexOf("\"", index + 1);
					if (index2 == -1) {
						item = new SearchItem(rest.substring(index, rest.length() - 1)
								+ "\"");
						item.setField(false);
						item.setLiteral(true);
						list.add(item);
						break;
					} else {
						item = new SearchItem("\"" + rest.substring(index, index2) + "\"");
						item.setField(false);
						item.setLiteral(true);
						list.add(item);
						rest = rest.substring(index2 + 2);
					}
				}
				/* if doesn't start with " but can has got it inside */
				else {
					/* if the first literal hasn't got " */
					if (rest.split(" ")[0].indexOf("\"") == -1) {
						String lit = rest.split(" ")[0];
						item = new SearchItem(lit);
						item.setLiteral(false);
						list.add(item);
						rest = rest.substring(rest.indexOf(lit) + lit.length());
					} else {
						/* if the literal has got a " */
						String lit = rest.substring(0, rest.indexOf("\"", rest
								.indexOf("\"") + 1) + 1);
						item = new SearchItem(lit);
						item.setField(true);
						item.setLiteral(true);
						list.add(item);
						rest = rest.substring(rest.indexOf(lit) + lit.length());
					}
				}

				if (rest.trim().length() <= 0)
					break;
			}
		} catch (java.lang.Exception e) {
			log.error("SearchServiceImpl: "+e.toString());
		}

		if (list.isEmpty()) {
			list.add(new SearchItem(phrases));
		}
		return list;
	}

	/**
	 * If we defined synonyms, return a string that will get synonyms in other
	 * case, we return a string that will get only the keyword
	 * 
	 * @param key
	 *          the keyword to search
	 * @param related
	 *          if has to search related keys
	 * @return a substring to insert into query string
	 */
	private String containKey(SearchItem key, double related) {

		if (related != -1 && 0 < related && related < 1)
			try {
				log.info("SearchServiceImpl: related value de: " + String.valueOf(related));
				return " CONTAINS(*,'" + key + "~" + String.valueOf(related) + "') ";
			} catch (Exception e) {
				log
						.info("SearchServiceImpl: excepcion obteniendo related search value, cambiando por 0.8: "
								+ e.getMessage());
				return " CONTAINS(*,'" + key + "~0.8') ";
			}
		else
			return " CONTAINS(*,'" + key + "') ";

	}

	/**
	 * Generate the final query string for the JCR
	 * 
	 * @param head
	 *          the first part of the query
	 * @param key
	 *          the string of the keys in raw
	 * 
	 * @param operator
	 *          the logical operator to use, default: AND
	 * @return a string with the query
	 */
	private String makeQueryString(String head, String key, String operator,
			double related) {
		String q;
		String intoperator = " " + operator + " ";
		if (intoperator == null)
			intoperator = " AND ";
		List<SearchItem> list = getLiteralsKeyPhrases(key);
		List<String> params = new ArrayList<String>();
		for (SearchItem param : list) {
			try {

				if (param.getField() == true) {
					params.add(specialKeyword(param.toString()));
				} else {
					params.add(containKey(param, related));
				}

			} catch (Exception ex) {
				log.warn("SearchServiceImpl: parseQuery excepcion: " + ex.toString());
			}
		}
		String query = params.get(0);

		for (int index = 1; index < params.size(); index++) {
			query = query + operator + params.get(index);
		}

		q = head + query + QUERYFOOT;
		return q;
	}

	/**
	 * If keyword contain s special key like autor or title, we return a string
	 * prepared to insert into query string
	 * 
	 * @param keyword
	 * @return a string prepared to insert into query string
	 */
	private String specialKeyword(String keyword) {

		String key = keyword.split(":")[0];
		String value = keyword.split(":")[1];
		String param = "";
		mapkeys = portalSetup.get_config_properties("/searcher/keywords");

		if (mapkeys.containsKey(key)) {
			param = (String) mapkeys.get(key);
		}
		return param + " like '%" + value + "%'";
	}

	/**
	 * Check if a path is in a list
	 * 
	 * @param list
	 * @param path
	 * @return
	 */
	private boolean isInList(ArrayList<String> list, String path) {
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			if (it.next().contains(path))
				return true;
		}
		return false;
	}

	/**
	 * Return the same literal if is not a number, in other cases, return a
	 * "<number>"
	 * 
	 * @param lit
	 *          literal to analize
	 * @return the same literal or a "<number>"
	 */
	private String parseNumber(String lit) {
		try {
			@SuppressWarnings("unused")
			Integer in = new Integer(lit);
			return "\"" + lit + "\"";
		} catch (NumberFormatException e) {
			try {
				@SuppressWarnings("unused")
				Float fl = new Float(lit);
				return "\"" + lit + "\"";
			} catch (NumberFormatException i) {
				return lit;
			}
		}
	}
}
