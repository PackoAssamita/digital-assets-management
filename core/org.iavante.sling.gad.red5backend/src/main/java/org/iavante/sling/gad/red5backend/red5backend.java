/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.red5backend;

import java.io.NotActiveException;

import org.iavante.sling.commons.distributionserver.IDistributionServer;

/**
 * Manage Red5 connections and services.
 * 
 * @scr.property name="service.description" value="IAVANTE - Red5backend"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface red5backend extends IDistributionServer {

	/**
	 * Get a url to access to rooms, download or streaming a media object.
	 * Depending of the media server the URL can work or not by credentials. If
	 * the server does'nt support streaming must raise a NotActiveException.
	 * 
	 * @param media
	 *          is a string that reference the media object at the content server
	 * @return a well-formed URL to download/streaming the media object
	 */
	public String getUrl(String media) throws NotActiveException;

}
