<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="javax.jcr.Node"%>
<%@ include file="/apps/gad/util.jsp" %>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>
<body>
<%@ include file="/apps/administration/header/header.jsp" %>
<div id="maincontainer">						
  <div id="container">
  
  <div id="maintitle">Tags</div><br />
  
    <div id="tag_cloud">
	<%
	NodeIterator it = currentNode.getNodes();	
	while(it.hasNext()){
	  Node node = it.nextNode(); %>	  
	  <a href="<%=node.getPath()%>.admin"><span><%=node.getProperty("title").getValue().getString()%></span></a>
	<%}%>
	</div>
  </div><!-- End container -->  
</div><!-- End maincontainer -->
<%@ include file="/apps/administration/footer/footer.jsp" %>	
</body>
</html>