<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>
<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
    <div id="data">
				<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">			
						title<br />
						<input class="input" type="text" id="title" />
										
						<br />picture<br />
						<input class="input" type="text" id="picture" />
						
						<br />description<br />
						<input class="input" type="text" id="description" />
								
						<br />author<br />
						<input class="input" type="text" id="author" />
								
						<br />extern_storage<br />
						<select class="input" type="text" id="extern_storage">
							<option value="on" selected="selected">on</option>
							<option value="off">off</option>
						</select>
							
						<br />subtitle<br />
						<input class="input" type="text" id="subtitle" />
							
						<br />schema<br />
						<select class="input" type="text" id="schema">
						<% 
						Map<String, String> schema_types = sling.getService(org.iavante.sling.commons.services.GADContentCreationService.class).getSchemas("collection");
						Iterator it = schema_types.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();%>
							<option value="<%=e.getKey()%>"><%=e.getKey() + " - " + e.getValue()%></option>							
						<%}%>
						</select>
						
						<br />supported schemas<br />
						<select multiple class="input" type="text" id="schemas">
						<% 
						schema_types = null;
						schema_types = sling.getService(org.iavante.sling.commons.services.GADContentCreationService.class).getSchemas("content");
						it = schema_types.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();%>
							<option value="<%=e.getKey()%>"><%=e.getKey() + " - " + e.getValue()%></option>							
						<%}%>
						</select>
					
						<!-- jcr:created -->
						<input class="input" type="hidden" id="jcr:created" value="" disabled="disabled" />
								
						<!-- jcr:lastModified -->
						<input class="input" type="hidden" id="jcr:lastModified" value="" disabled="disabled" />
									
		
						<!-- jcr:lastModifiedBy -->
						<input class="input" type="hidden" id="jcr:lastModifiedBy" value="" disabled="disabled" />
								

						<!-- sling:resourceType -->
						<input class="input" type="hidden" id="sling:resourceType" value="gad/collection" disabled="disabled" />
									
						<!-- jcr:createdBy -->
						<input class="input" type="hidden" id="jcr:createdBy" value="" disabled="disabled" />	
						
						<input class="input" type="hidden" id="_charset_" value="utf-8" />
								
				</form>

					
	<input type="button" class="guardar" onclick="javascript:crear('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>
			
	</div>
</div>

	<%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>

