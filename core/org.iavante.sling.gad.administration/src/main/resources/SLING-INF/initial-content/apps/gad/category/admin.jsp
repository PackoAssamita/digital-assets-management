<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
<%@ include file="/apps/administration/header/html_header.jsp" %>
<link rel="stylesheet" href="/apps/administration/static/css/file.css">
</head>

<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
	<div id="container">
		<div id="data">

			<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">

				<%
				ArrayList etiquetas = new ArrayList();
				etiquetas.add("title");
					
				for(int i=0; i<etiquetas.size(); i++){
				   
					String tagName = (String) etiquetas.get(i);%>
				
					<%= tagName%><br />
					<input class="input" type="text" id="<%= tagName%>" value="<% if(currentNode.hasProperty(tagName)){ %><%=currentNode.getProperty(tagName).getValue().getString()%><%}%>" /><br />
				<%}%>
				
				
				sling:resourceType<br />
				<input class="input" type="text" id="sling:resourceType" value="<% if(currentNode.hasProperty("sling:resourceType")) { %><%=currentNode.getProperty("sling:resourceType").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				
				created<br />
				<input class="input" type="text" id="jcr:created" value="<% if(currentNode.hasProperty("jcr:created")) { %><%=currentNode.getProperty("jcr:created").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				createdBy<br />
				<input class="input" type="text" id="jcr:createdBy" value="<% if(currentNode.hasProperty("jcr:createdBy")) { %><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				lastModified<br />
				<input class="input" type="text" id="jcr:lastModified"value="<% if(currentNode.hasProperty("jcr:lastModified")) { %><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				lastModifiedBy<br />
				<input class="input" type="text" id="jcr:lastModifiedBy" value="<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

				<input class="input" type="hidden" id="_charset_" value="utf-8" />

			</form>

			<input class="guardar" type="button" onclick="javascript:guardar('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>
	
	  	<div id="caja">
				<div class="corner-up">Subcategor&iacute;as:</div>	
					<% if(currentNode.hasNodes()) {
						NodeIterator node_iter_subcats = currentNode.getNodes();
						while (node_iter_subcats.hasNext()) {
							Node node_subcat = node_iter_subcats.nextNode();%>
							<% if(node_subcat.hasProperty("title")) {%>
							&nbsp;&nbsp;&nbsp;<%=node_subcat.getProperty("title").getValue().getString() %>
							<%}%>
							<a class="delete" href="javascript:eliminar('<%=node_subcat.getPath()%>')" >eliminar</a><br/>
							 
							 
							<%
							String abs_path0 = node_subcat.getPath();
							String channel0 = abs_path0.split("cats")[0];
							QueryManager qm0 = currentNode.getSession().getWorkspace().getQueryManager();
							String quer0 = "select * from nt:base where jcr:path LIKE '" + channel0 + "contents/%' AND CONTAINS (categories,'"+ abs_path0 +"')";
							Query query = qm0.createQuery(quer0, Query.SQL);
							NodeIterator it0 = query.execute().getNodes();
							while(it0.hasNext()){
								Node nodeContent = it0.nextNode();
								if(nodeContent.hasProperty("title")){
							%>
								&nbsp;&nbsp;&nbsp;<a class="urlcat" href="<%=nodeContent.getPath()%>.admin"><%=nodeContent.getProperty("title").getValue().getString()%></a><br/>
							<%}
							}
							%>
						 
					 <%}		 
				   }
				%>
				<div class="corner-down"></div>
			</div>	<!-- fin caja -->
			<br/>
			<div id="caja">
				<div class="corner-up">Revisiones asociadas a la categor&iacute;a:</div>	
				<%
				String abs_path1 = currentNode.getPath();
				String channel1 = abs_path1.split("cats")[0];
				%>
				<!--abs_path<%=currentNode.getPath()%><br/>-->
				<%
				QueryManager qm1 = currentNode.getSession().getWorkspace().getQueryManager();
				//String quer = "select * from nt:base where jcr:path LIKE '/content/canales/ies/contents/%' AND CONTAINS (categories,'/content/canales/ies/cats/cat1')";
				String quer1 = "select * from nt:base where jcr:path LIKE '" + channel1 + "contents/%' AND CONTAINS (categories,'"+ abs_path1 +"')";
				Query query1 = qm1.createQuery(quer1, Query.SQL);
				NodeIterator it1 = query1.execute().getNodes();
				while(it1.hasNext()){
					Node nodeContent = it1.nextNode();
					if(nodeContent.hasProperty("title")){
				%>
					&nbsp;&nbsp;&nbsp;<a class="urlcat" href="<%=nodeContent.getPath()%>.admin"><%=nodeContent.getProperty("title").getValue().getString()%></a><br/>
				<%}
				}
				%>
				
					
				<div class="corner-down"></div>
			</div>	<!-- End caja -->

		</div> <!-- End data-->
	</div><!-- End container -->  

	<div id="menu">
		<div id="actmenu">
			<ul>	
				<li class="shadow"><h4>Acciones</h4></li>
				
				
				<li><a class="delete" href="javascript:show('divsubcategoria')" >&nbsp;&nbsp;&nbsp;nueva subcategor&iacute;a<a/>
					<div id="divsubcategoria" style="display:none;">
					  <input type="text" id="subcat" size="19" name="subcat" />	
					  <input type="button" class="btnewsubcat" onclick="javascript:crearSubcategoria('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value="" />	
					</div>
				</li>
				<li class="finli">&nbsp</li>			
			
			
			</ul>
		</div>
	</div>

</div><!-- End maincontainer -->  


	<%@ include file="/apps/administration/footer/footer.jsp" %>

</body>
</html>
