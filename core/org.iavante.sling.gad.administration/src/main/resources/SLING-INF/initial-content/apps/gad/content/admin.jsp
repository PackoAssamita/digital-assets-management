<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.lang.String"%>
<%@ page import="java.lang.Integer"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.commons.services.PortalSetup"%>
<%@ page import="org.iavante.sling.gad.source.SourceTools"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="org.apache.jackrabbit.value.StringValue"%>
<%@ page import="javax.jcr.Value" %>
<%@ page import="javax.jcr.Node"%>
<%@ include file="/apps/commons/util.jsp" %>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String url_playlist ="";
try{
	url_playlist=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getXSPFUrl(currentNode.getPath());
	//url_playlist = request.getRequestURL().toString().split(".admin")[0]+".xspf.xml";
	
}catch(Exception e){}

String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.Calendar"%><html>
<head>

	<%@ include file="/apps/administration/header/html_header.jsp" %>
	<script type="text/javascript" src="/apps/administration/static/js/prototype.js"></script>
	<script type="text/javascript" src="/apps/administration/static/js/ajaxupload.3.5.js"></script> 

	<script type="text/javascript">
	document.observe("dom:loaded", function() {
		/* example 1 */
		var button = $('button1'), interval, status=$('status');
		new Ajax_upload(button,{
			action: '/content/uploadservice',
			name: 'data',
			// Additional data to send
			data: {
//				content : '/content/colecciones/colecci_n_de_trol/contents/contentido_1/sources/*',
//				filename : 'IES'
			},
			onSubmit : function(file, ext){
				// change button text, when user selects file
				button.update('Uploading');

				var sourcename = $('sourcename').value;
				//alert('filename:' + filename);
				var content = $('sourcecontent').value;
				//alert('content:' + content);
				var filename = file;
				//alert('filename:' + file);
				
				if(sourcename != ''){
					this.setData({'filename':filename, 'content':content, 'sourcename':sourcename});
				}else{
					this.setData({'filename':filename, 'content':content, 'sourcename':filename});
				}
				
				// If you want to allow uploading only 1 file at time,
				// you can disable upload button
				this.disable();
				
				// Animating upload button
				// Uploding -> Uploading. -> Uploading...
				interval = window.setInterval(function(){
					var text = button.innerHTML;
					if (text.length < 13){
						button.update(text + '.');
					} else {
						button.update('Uploading');
					}
				}, 200);
			},
			onComplete: function(file, response){

				//console.log(response);
				button.update('Upload');
				window.clearInterval(interval);
				// enable upload button
				this.enable();

				if(response.startsWith("Location")){
					alert("Fichero subido correctamente");
				}else{
					alert("Se ha producido un error, " + response);
				}
				document.location.href= document.location.href;
			}
		}); 
	});
	</script>

</head>

<!-- END HEAD -->
<!-- BODY -->

<body onload="javascript:inicio_content('<%=url_playlist%>', 'no_logo_in_revision')">
<%@ include file="/apps/administration/header/header.jsp" %>
<div id="maincontainer">						
<div id="container">
    <div id="data">
   		
<%
if(currentNode.hasProperty("finish")&&!(currentNode.getProperty("finish").getDefinition().isMultiple())) { 
	String value = currentNode.getProperty("finish").getValue().getString();
	if("1".compareTo(value)==0){%>
	<img align="right" width="20" height="20" src="/apps/administration/static/images/green.png">	
<%}else{%>
	<img align="right" width="20" height="20" src="/apps/administration/static/images/red.png">	
<%}
}
%><br />
   		
	<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">
									
<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("description");
	etiquetas.add("author");
	etiquetas.add("origin");
	etiquetas.add("tags");
	//etiquetas.add("copies");

	String tagName = "";
	
 	for(int i=0; i<etiquetas.size(); i++){
   
 		tagName = (String) etiquetas.get(i);
		String string_value = "";
		Property prop = null;
		if(currentNode.hasProperty(tagName)){
			prop = currentNode.getProperty(tagName);
			if (prop.getDefinition().isMultiple()) {
				Value[] values = prop.getValues();		
				for (int j=0; j<values.length;j++) {
					string_value = string_value + values[j].getString() + ", ";
				}		
				if (string_value.length() != 0) {
			    	string_value = string_value.substring(0, string_value.length() - 2);
			    	prop = null;
				}
			}	
			else {
				string_value = prop.getValue().getString();
				prop = null;
			}%>	
		<%}	 %>
<%= tagName%>
<input class="input" type="text" id="<%= tagName%>" value="<%=string_value%>" /><br />
<%}%>

lang
<%
String lang = "es_ES";
if(currentNode.hasProperty("lang") && !(currentNode.getProperty("lang").getDefinition().isMultiple())) {
	lang = currentNode.getProperty("lang").getValue().getString();
}
%>
<%=ComboLang.buildComboLang("lang", lang, "input")%><br />

schema
<input class="input" type="text" id="schema" value="<% if(currentNode.hasProperty("schema")&&!(currentNode.getProperty("schema").getDefinition().isMultiple())) { %><%=currentNode.getProperty("schema").getValue().getString()%><%}%>" disabled="disabled" /><br />


version
<input class="input" type="text" id="version" value="<% if(currentNode.hasProperty("version")&&!(currentNode.getProperty("version").getDefinition().isMultiple())) { %><%=currentNode.getProperty("version").getValue().getString()%><%}%>" disabled="disabled" /><br />

validated
<input class="input" type="text" id="validated" value="<% if(currentNode.hasProperty("validated")&&!(currentNode.getProperty("validated").getDefinition().isMultiple())) { %><%=currentNode.getProperty("validated").getValue().getString()%><%}%>" disabled="disabled" /><br />

gaduuid
<input class="input" type="text" id="gaduuid" value="<% if(currentNode.hasProperty("gaduuid")&&!(currentNode.getProperty("gaduuid").getDefinition().isMultiple())) { %><%=currentNode.getProperty("gaduuid").getValue().getString()%><%}%>" disabled="disabled" /><br />

hits
<input class="input" type="text" id="hits" value="<% if(currentNode.hasProperty("hits")&&!(currentNode.getProperty("hits").getDefinition().isMultiple())) { %><%=currentNode.getProperty("hits").getValue().getString()%><%}%>" disabled="disabled" /><br />

thumbnail
<img src="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode) %>" /><br/>

sling:resourceType
<input class="input" type="text" id="sling:resourceType" value="<% if(currentNode.hasProperty("sling:resourceType")) { %><%=currentNode.getProperty("sling:resourceType").getValue().getString()%><%}%>" disabled="disabled" /><br />

created
<input class="input" type="text" id="jcr:created" value="<% if(currentNode.hasProperty("jcr:created")) { %><%=currentNode.getProperty("jcr:created").getValue().getString()%><%}%>" disabled="disabled" /><br />

createdBy
<input class="input" type="text" id="jcr:createdBy" value="<% if(currentNode.hasProperty("jcr:createdBy")) { %><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModified
<input class="input" type="text" id="jcr:lastModified"value="<% if(currentNode.hasProperty("jcr:lastModified")) { %><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModifiedBy
<input class="input" type="text" id="jcr:lastModifiedBy" value="<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%>" disabled="disabled" /><br />
<input class="input" type="hidden" id="tags@TypeHint" value="String[]" />
<input class="input" type="hidden" id="_charset_" value="utf-8" />



</form>
<input type="button" class="guardar" onclick="javascript:guardar('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/><hr />

<div>
<%@ include file="/apps/administration/widgets/sources.jsp" %>
</div>    

</div><!-- End data -->

    <div id="video"><script type="text/javascript" src="/apps/administration/static/flash/swfobject.js"></script><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</div>
</div><!-- fin container -->   
     
	<div id="menu">
		<div id="crear">
			<input type="button" class="newsource" onclick="javascript:document.getElementById('dataupload').style.display='block';" value="" />
		</div>
		
		<div class="clear"></div>

	<div id="dataupload" class="example">
		<div class="wrapper">
			<label>[Nombre]:</label><br/>
			<input type="text" id="sourcename" name="sourcename" value=""></input><br/>
			<input type="hidden" id="sourcecontent" name="sourcecontent" value="<%=currentNode.getPath()%>/sources/*"></input>
			<div id="button1" class="button">Upload</div>
			<span id="status"></span>
		</div>
	</div>
	
	
	
	<div class=	"clear"></div>
    
	<div id="actmenu2">
		<ul>	
			<li class="shadow"><h4>Acciones</h4></li>
			<%if ((currentNode.hasProperty("validated")) && ("1".equals(currentNode.getProperty("validated").getValue().getString()))) { %>
			  <li><span id="spanok"></span><a href="javascript:pendiente('<%=currentNode.getPath()%>')">&nbsp;&nbsp;&nbsp;enviar a revisi&oacute;n</a>&nbsp;&nbsp;&nbsp;</li>
			<%} else {%>
			  <li><span id="spanok"></span><span class="deactivated">&nbsp;&nbsp;&nbsp;no validado</span>&nbsp;&nbsp;&nbsp;</li>	
			<%}%>		
			<li class="finli">&nbsp;</li>
			
			<%if ((currentNode.hasProperty("validated")) && ("1".equals(currentNode.getProperty("validated").getValue().getString()))) { %>
				<li><a class="delete" href="javascript:show('divpublicar')" >&nbsp;&nbsp;&nbsp;publicar<a/>
					<div id="divpublicar" style="display:none;">
					  &nbsp;&nbsp;&nbsp;
					  <select multiple id="channels" name="channels" class="combo"> <!-- Rellenamos con los canales existentes -->
					  <%
					  QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
					  Query query = queryManager.createQuery("/jcr:root//content/canales/element(*, nt:unstructured)[@sling:resourceType = 'gad/channel'] order by @title descending", "xpath");
					  
					  NodeIterator itchannels = query.execute().getNodes();
					  while(itchannels.hasNext()){
						  Node node = itchannels.nextNode();
						  if(node.hasProperty("title")){
						  	String [] splitPath = node.getPath().split("/");
					  %>
					  <option value="<%=splitPath[splitPath.length-1]%>"><%=node.getProperty("title").getString()%></option>
					  <%}} %>
					  </select>	
					  <input type="button" class="btpublicar" onclick="javascript:directPublication('<%= currentNode.getPath() %>','false')" value="" />	
					</div>
				</li>
				<li class="finli">&nbsp</li>
			<%}%>
			
		<li><a class="delete" href="javascript:show('divtraducir')" >&nbsp;&nbsp;&nbsp;traducir<a/>
			<div id="divtraducir" style="display:none;">
			  &nbsp;&nbsp;&nbsp;
			  <%=ComboLang.buildComboLang("traduction_lang", "en_EN", "")%><br />
			  <input type="button" class="bttraducir" onclick="javascript:copyContent('<%=currentNode.getPath()%>','true')" value="" />	
			</div>
		</li>
		<li class="finli">&nbsp</li>
		
		<li><a class="delete" href="javascript:show('divcopiarfuentes')" >&nbsp;&nbsp;&nbsp;copiar fuentes<a/>
			<div id="divcopiarfuentes" style="display:none;">
			  &nbsp;&nbsp;&nbsp;dest:&nbsp;
			  <select id="contentToCopy" name="contentToCopy" class="combo"> <!-- Rellenamos con los contenidos de esta colección -->
			  <%
			  String contentsPath = currentNode.getPath().substring(0,currentNode.getPath().lastIndexOf("/"));
			  QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
			  Query query = queryManager.createQuery("/jcr:root/"+contentsPath+"/element(*, nt:unstructured)[@sling:resourceType = 'gad/content'] order by @title descending", "xpath");
			  
			  NodeIterator itcontents = query.execute().getNodes();
			  while(itcontents.hasNext()){
				  Node node = itcontents.nextNode();
				  if(node.hasProperty("title")){
				  	String [] splitPath = node.getPath().split("/");
			  %>
			  <option value="<%=contentsPath+"/"+splitPath[splitPath.length-1]%>"><%=node.getProperty("title").getString()%></option>
			  <%}} %>
			  </select>		
			  <input type="button" class="btcopiar" onclick="javascript:copySources('<%=currentNode.getPath()%>','true')" value="" />	
			</div>
		</li>
		<li class="finli">&nbsp</li>
		
		<%if(currentNode.hasNode("sources/fuente_default/transformations")){ %>
			<li><a class="delete" href="javascript:show('divselectdefaultthumb')" >&nbsp;&nbsp;&nbsp;seleccionar autothumbnail<a/>
				<div id="divselectdefaultthumb" style="display:none;">
				  <%
				  String transPath = currentNode.getPath()+"/sources/fuente_default/transformations";
				  QueryManager queryManager2 = currentNode.getSession().getWorkspace().getQueryManager();
				  Query query2 = queryManager2.createQuery("/jcr:root/"+transPath+"/element(*, nt:unstructured)[@sling:resourceType = 'gad/transformation'] order by @title descending", "xpath");
				  
				  NodeIterator ittrans = query2.execute().getNodes();
				  while(ittrans.hasNext()){
					  Node node = ittrans.nextNode();
					  if(node.getName().startsWith("thumbnail_preview_")
					  		&& node.hasProperty("file")
					  		&& !"".equals(node.getProperty("file").getString())){
					  	String [] splitPath = node.getPath().split("/");
					  	char [] nodeName = node.getName().toCharArray();
					  	int transIndex = Integer.parseInt(""+nodeName[nodeName.length-1]); 
				  %>
				  <br><center><a href="javascript:copyTransformation('<%=node.getPath()%>','<%=currentNode.getPath()+"/sources/thumbnail_default"%>');">
				  	<img width="100" src="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbPreviewUrl(currentNode.getNode("sources/fuente_default"),transIndex) %>"></img>
				  </a></center>
				  <%}} %>
	
				</div>
			</li>
			<li class="finli">&nbsp</li>
		<%}%>
		</ul>
	</div>

<div>
<%@ include file="/apps/administration/widgets/tags.jsp" %>
</div>
<div>
<%@ include file="/apps/administration/widgets/lognode.jsp" %>
</div>
</div><!-- End menu -->
</div><!-- End maincontainer -->
<%@ include file="/apps/administration/footer/footer.jsp" %>	
</body>
</html>