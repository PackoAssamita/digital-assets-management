<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="javax.jcr.Node"%>

<%@ include file="/apps/gad/util.jsp" %>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String urlInternal = sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode);
String urlExternal = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode);

String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>


<%
String VERSION = "ver";
String version = "";
boolean version_exists = true;

if (request.getParameter(VERSION) != null) {
    version = request.getParameter(VERSION);    
    version_exists = false;
    VersionIterator vi = currentNode.getVersionHistory().getAllVersions();
    while(vi.hasNext()) {
    	Version v = vi.nextVersion();
	String name = v.getName(); %>
        <%if (name.equals(version)) {
			currentNode = v.getNode("jcr:frozenNode");  
      	    version_exists = true;   
		}
    }
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>

<body onload="javascript:inicio('<%=urlInternal%>')">

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">						
<div id="container">
    <div id="data">
   		
	<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">

<% if (version_exists) {

	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("description");
	etiquetas.add("author");
	etiquetas.add("schema");
	etiquetas.add("origin");
	etiquetas.add("tags");
	etiquetas.add("lang"); 
	etiquetas.add("version");
	etiquetas.add("validated");
	etiquetas.add("gaduuid");
	etiquetas.add("sling:resourceType");
	etiquetas.add("jcr:created");
	etiquetas.add("jcr:createdBy");
	etiquetas.add("jcr:lastModified");
	etiquetas.add("jcr:lastModifiedBy");

	String tagName = "";
	
 	for(int i=0; i<etiquetas.size(); i++){
   
 		tagName = (String) etiquetas.get(i);
		String string_value = "";
		Property prop = null;
		if(currentNode.hasProperty(tagName)){
			prop = currentNode.getProperty(tagName);
			if (prop.getDefinition().isMultiple()) {
				Value[] values = prop.getValues();		
				for (int j=0; j<values.length;j++) {
					string_value = string_value + values[j].getString() + ", ";
				}		
				if (string_value.length() != 0) {
			    	string_value = string_value.substring(0, string_value.length() - 2);
			    	prop = null;
				}
			}	
			else {
				string_value = prop.getValue().getString();
				prop = null;
			}%>	
		<%}	 %>
<%= tagName%>
<input class="input" type="text" id="<%= tagName%>" value="<%=string_value%>" disabled="disabled" /><br />
<%}%>

</form>

    </div><!-- End data -->

     	<div id="video">
		<script type="text/javascript" src="/apps/administration/static/flash/swfobject.js"></script>
		<a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.	
	</div>

   </div><!-- End container -->  


     <div id="menu">

<%
 String getUrl = request.getPathInfo();
 String NewUrl[] = getUrl.split("version");
 getUrl = NewUrl[0] + "admin";
%>

<div id="actmenu2">
	<ul>	
		<li class="shadow"><h4>Acciones</h4></li>
		<li>
		<a href="<%=getUrl%>">&nbsp;&nbsp;&nbsp;volver a la versi&oacute;n actual<a/>	
		</li>
		<li class="finli">&nbsp</li>
	</ul>
</div>

<%
if (request.getParameter(VERSION) == null) {%>
	<div id="versions">
	<div class="vers"></div>
<%
VersionIterator vi = currentNode.getNode("log").getVersionHistory().getAllVersions();
while(vi.hasNext()) {
	Version v = vi.nextVersion();	
	Node n = v.getNode("jcr:frozenNode");%>

      <div class="vers"><%=DateUtils.format(v.getCreated())%>
    	<%=n.hasProperty("state") ? n.getProperty("state").getString() : ""%>
	<a href="<%=currentNode.getPath()%>.admin?ver=<%=n.hasProperty("version") ? n.getProperty("version").getString() : ""%>">   	
	<%=n.hasProperty("version") ? n.getProperty("version").getString() : ""%></a></div>
  
<%}}%>
</div>
<%}else{ %>
	<h1>Version name doesn't exits</h1>
<%}%>

</div>

      </div><!-- End menu -->

    </div><!-- End maincontainer -->

	<%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>





