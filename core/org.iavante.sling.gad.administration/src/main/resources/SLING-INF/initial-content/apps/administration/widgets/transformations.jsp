<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
Node transformations_node = null;

if (currentNode.hasNode("transformations")) {
	transformations_node = currentNode.getNode("transformations");    			
%>
	  

<div id="cajaTransformations">
		<div id="transformations" class="corner-up"><b>Transformations:</b></div>
   	<%
		NodeIterator it_t1 = transformations_node.getNodes();
		Integer x=0;
		while(it_t1.hasNext()){
			Node nodeTransformation = it_t1.nextNode(); // .../transcriptions/preview i.e.
			String nodeName =  nodeTransformation.getName();
			String mimeType = "";
			String state = "";
			if (nodeTransformation.hasProperty("mimeType")){
			mimeType = nodeTransformation.getProperty("mimeType").getValue().getString();
			}
			if (nodeTransformation.hasProperty("state")){
			state = nodeTransformation.getProperty("state").getValue().getString();
			}
			%>
			<div class="objeto">
			<% if ( nodeTransformation.hasProperty("file") && "".compareTo(nodeTransformation.getProperty("file").getValue().getString())!=0 ) { %>
								<div class="source_files">
								<p> <%=nodeName%>Status : <%=state%></p>
								
								<p><%= nodeTransformation.getProperty("file").getValue().getString() %>&nbsp;
								<a href="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getOriginalUrl(nodeTransformation)%>"><img class="middle" title="Download" alt="Download" src="/apps/administration/static/images/download_icon.png" /></a></p>
								</div>
								<%}%>	
			</div>
			
		<%}%>
	<div class="corner-down"></div>
</div><!-- fin cajaTransformations -->
<%}%>
