<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="application/smil; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.source.SourceService"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<% 
if (request.getParameter("dest") != null) {
	int returncode = response.SC_BAD_REQUEST;
	String message = "";
	String destParam = request.getParameter("dest");
	String newTransNameParam = request.getParameter("newtransname");
	String [] destParamSplit = destParam.split("/");
  if(destParam.startsWith("/content/colecciones/")
  		&& "sources".equals(destParamSplit[destParamSplit.length-2])){
  	int ecode = sling.getService(org.iavante.sling.gad.source.SourceService.class).copyTransformation(request,response,currentNode.getPath());
	  
	  switch(ecode){
	  	case 0:
	  		returncode = response.SC_ACCEPTED;
	  		message = "Processing";
	  		break;
	  	case 1:
	  		returncode = response.SC_UNAUTHORIZED;
	  		message = "You haven't privileges in collections";
	  		break;
	  	default:
	  		returncode = response.SC_BAD_REQUEST;
	  		message = "Bad request";
	  		break;
	  }
  }
  %><%=message%><%
	response.setStatus(returncode);
}
else{
	response.setStatus(response.SC_FORBIDDEN);
	%><b>dest</b> parameter is missing <%
}

%>
