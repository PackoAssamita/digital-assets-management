<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="org.iavante.sling.gad.downloader.services.IDownloader"%>
<%@ include file="/apps/commons/util.jsp" %>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%

String preview_flv="";
String preview_ogv="";
String type="";
String preview_img="";

try{
	type= currentNode.getProperty("type").getValue().getString().trim();
}catch(Exception e){}

if( type.compareToIgnoreCase("image") == 0){
	try{
		preview_img = sling.getService(org.iavante.sling.gad.downloader.services.IDownloader.class).getUrl(currentNode.getProperty("file").getValue().getString());
	}catch(Exception e){}
}
else{
	try{
		Node trans_flv = currentNode.getNode("transformations").getNode("preview");
		preview_flv = sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,trans_flv);
	}catch(Exception e){}
	try{
		Node trans_ogv = currentNode.getNode("transformations").getNode("preview_ogv");
		preview_ogv = sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,trans_ogv);
	}catch(Exception e){}
}
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
	<script type="text/javascript" src="/apps/administration/static/js/prototype.js"></script>
	<script type="text/javascript" src="/apps/administration/static/js/ajaxupload.3.5.js"></script> 

	<script type="text/javascript">
		document.observe("dom:loaded", function() {
			/* example 1 */
			var buttonA = $('button1'), interval;
			new Ajax_upload(buttonA,{
				action: '/content/uploadservice',
				name: 'data',
				// Additional data to send
				data: {
	//				content : '/content/colecciones/colecci_n_de_trol/contents/contentido_1/sources/*',
	//				filename : 'IES'
				},
				onSubmit : function(file, ext){
					// change button text, when user selects file
					buttonA.update('Uploading');
	
					var sourcename = $('sourcename').value;
					//alert('filename:' + filename);
					var content = $('sourcecontent').value;
					//alert('content:' + content);
					var rt = $('rt').value;
					//alert('resourceType2:' + rt);
					var filename = file;
					//alert('filename:' + file);
					
					if(sourcename != ''){
						this.setData({'filename':filename, 'content':content, 'sourcename':sourcename, 'rt':rt});
					}else{
						this.setData({'filename':filename, 'content':content, 'sourcename':filename, 'rt':rt});
					}
					
					// If you want to allow uploading only 1 file at time,
					// you can disable upload button
					this.disable();
					
					// Animating upload button
					// Uploding -> Uploading. -> Uploading...
					interval = window.setInterval(function(){
						var text = buttonA.innerHTML;
						if (text.length < 13){
							buttonA.update(text + '.');
						} else {
							buttonA.update('Uploading');
						}
					}, 200);
				},
				onComplete: function(file, response){
	
					//console.log(response);
					buttonA.update('Upload');
					window.clearInterval(interval);
					// enable upload button
					this.enable();
	
					if(response.startsWith("Location")){
						alert("Fichero subido correctamente");
					}else{
						alert("Se ha producido un error, " + response);
					}
					document.location.href= document.location.href;
				}
			}); 

			var buttonB = $('button2'), interval2;
			new Ajax_upload(buttonB,{
				action: '/content/uploadservice',
				name: 'data',
				// Additional data to send
				data: {
	//				content : '/content/colecciones/colecci_n_de_trol/contents/contentido_1/sources/*',
	//				filename : 'IES'
				},
				onSubmit : function(file, ext){
					// change button text, when user selects file
					buttonB.update('Uploading');
	
					var sourcename = $('sourcename2').value;
					//alert('filename:' + filename);
					var content = $('sourcecontent2').value;
					//alert('content:' + content);
					var rt2 = $('rt2').value;
					//alert('resourceType2:' + rt2);
					var filename = file;
					//alert('filename:' + file);
					var lang = $('languages').options[$('languages').selectedIndex].value;
					var dtype = $('document_type').options[$('document_type').selectedIndex].value;
					content += '/' + lang + '/' + dtype + '/*';
					alert('content:' + content);
					
					if(sourcename != ''){
						this.setData({'filename':filename, 'content':content, 'sourcename':sourcename, 'rt':rt2});
					}else{
						this.setData({'filename':filename, 'content':content, 'sourcename':filename, 'rt':rt2});
					}
					
					// If you want to allow uploading only 1 file at time,
					// you can disable upload button
					this.disable();
					
					// Animating upload button
					// Uploding -> Uploading. -> Uploading...
					interval2 = window.setInterval(function(){
						var text = buttonB.innerHTML;
						if (text.length < 13){
							buttonB.update(text + '.');
						} else {
							buttonB.update('Uploading');
						}
					}, 200);
				},
				onComplete: function(file, response){
	
					//console.log(response);
					buttonB.update('Upload');
					window.clearInterval(interval2);
					// enable upload button
					this.enable();
	
					if(response.startsWith("Location")){
						alert("Fichero subido correctamente");
					}else{
						alert("Se ha producido un error, " + response);
					}
					document.location.href= document.location.href;
				}
			}); 
		});
	</script>
</head>
<% if( type.compareToIgnoreCase("video") == 0){%>
<body onload="javascript:inicio('<%= preview_flv%>','<%= preview_ogv%>')">

<%}
else
	if (type.compareToIgnoreCase("image") == 0){%>
		<body onload="javascript:inicio_img('<%= preview_img%>')">
		<%
	}
%>
<%@ include file="/apps/administration/header/header.jsp" %>
							
<div id="maincontainer">
	<div id="container">
		<div id="data">
	   		
			<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">
		
				<%
				ArrayList etiquetas = new ArrayList();
				etiquetas.add("title");
				etiquetas.add("author");
				etiquetas.add("file");
				etiquetas.add("mimetype");
				etiquetas.add("text_encoding");
				etiquetas.add("type");
				etiquetas.add("bitrate");
				etiquetas.add("tags");
				etiquetas.add("tracks_number");
				etiquetas.add("track_1_type");
				etiquetas.add("track_1_encoding");
				etiquetas.add("track_1_features");
				etiquetas.add("track_2_type");
				etiquetas.add("track_2_encoding");
				etiquetas.add("track_2_features");
				etiquetas.add("lengths");
				etiquetas.add("size");
			
			 	for(int i=0; i<etiquetas.size(); i++){
					String tagName = (String) etiquetas.get(i);%>
				
					<%= tagName%><br />
					<input class="input" type="text" id="<%= tagName%>" value="<% if(currentNode.hasProperty(tagName)){ %><%=currentNode.getProperty(tagName).getValue().getString()%><%}%>" /><br />
				<%}%>
				
				lang<br />
				<%
				String lang = "es_ES";
				if(currentNode.hasProperty("lang") && !(currentNode.getProperty("lang").getDefinition().isMultiple())) {
					lang = currentNode.getProperty("lang").getValue().getString();
				}
				%>
				<%=ComboLang.buildComboLang("lang", lang, "input")%><br />
				
				
				<% // Get subtitles and audiodescriptions
				List subLangs = new ArrayList(); //langs of transcriptions/subtitles supported
				List audioLangs = new ArrayList(); //langs of transcriptions/audiodescription supported
				
				if(currentNode.hasNode("transcriptions")){ // filling up 'subLangs'
					String cPathSub = currentNode.getPath().substring(0);
					QueryManager qMSub = currentNode.getSession().getWorkspace().getQueryManager();
				  Query qSub = qMSub.createQuery("/jcr:root/"+cPathSub+"/transcriptions//*[@sling:resourceType = 'gad/source']", "xpath");
				  
				  NodeIterator itLangs = qSub.execute().getNodes();
				  while(itLangs.hasNext()){
				  	Node node = itLangs.nextNode();
				  	String [] nodeSplit = node.getPath().split("/");
				  	if("subtitle".equals(node.getParent().getName())){
				  		//subLangs.add(node.getParent().getParent().getName());
				  		subLangs.add("/"+nodeSplit[nodeSplit.length-4]+"/"+nodeSplit[nodeSplit.length-3]+"/"+nodeSplit[nodeSplit.length-2]+"/"+nodeSplit[nodeSplit.length-1]);
				  	}
				  	else if("audiodescription".equals(node.getParent().getName())){
				  		audioLangs.add("/"+nodeSplit[nodeSplit.length-4]+"/"+nodeSplit[nodeSplit.length-3]+"/"+nodeSplit[nodeSplit.length-2]+"/"+nodeSplit[nodeSplit.length-1]);
				  	}
				  }
				}%>
				
				default_subtitle<br />
				<%
				String default_subtitle = "es_ES";
				if(currentNode.hasProperty("default_subtitle") && !(currentNode.getProperty("default_subtitle").getDefinition().isMultiple())) {
					default_subtitle = currentNode.getProperty("default_subtitle").getValue().getString();
				}
				%>
				<%=ComboLang.buildComboLang("default_subtitle", default_subtitle, "input", subLangs, " --- Adjunte algún subtítulo --- ")%><br />
				
				default_audiodescription<br />
				<%
				String default_audiodescription = "es_ES";
				if(currentNode.hasProperty("default_audiodescription") && !(currentNode.getProperty("default_audiodescription").getDefinition().isMultiple())) {
					default_audiodescription = currentNode.getProperty("default_audiodescription").getValue().getString();
				}
				%>
				<%=ComboLang.buildComboLang("default_audiodescription", default_audiodescription, "input", audioLangs, " --- Adjunte alguna audiodescripción --- ")%><br />
				
				sling:resourceType<br />
				<input class="input" type="text" id="sling:resourceType" value="<% if(currentNode.hasProperty("sling:resourceType")) { %><%=currentNode.getProperty("sling:resourceType").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				created<br />
				<input class="input" type="text" id="jcr:created" value="<% if(currentNode.hasProperty("jcr:created")) { %><%=currentNode.getProperty("jcr:created").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				createdBy<br />
				<input class="input" type="text" id="jcr:createdBy" value="<% if(currentNode.hasProperty("jcr:createdBy")) { %><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				lastModified<br />
				<input class="input" type="text" id="jcr:lastModified"value="<% if(currentNode.hasProperty("jcr:lastModified")) { %><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%><%}%>" disabled="disabled" /><br />
				
				lastModifiedBy<br />
				<input class="input" type="text" id="jcr:lastModifiedBy" value="<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%>" disabled="disabled" /><br />
			
			</form>
	
			<input type="button" class="guardar" onclick="javascript:guardar('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>

			<div>
			<%@ include file="/apps/administration/widgets/transcriptions.jsp" %>
			</div>  

			<div>
			<%@ include file="/apps/administration/widgets/transformations.jsp" %>
			</div>  	
		</div> <!-- End data -->
		
		<div id="video"><script type="text/javascript" src="/apps/administration/static/flash/swfobject.js"></script><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</div>

	</div><!--End container-->
	
		<div id="menu">
			<div id="crear">
				<input type="button" class="updatesource" onclick="javascript:document.getElementById('dataupload').style.display='block';" value="" />
			</div>
			
			<div class="clear"></div>
		
			<div id="dataupload" class="example">
				<div class="wrapper">
					<label>[Nombre]:</label><br/>
					<input type="text" id="sourcename" name="sourcename" value=""></input><br/>
					<input type="hidden" id="sourcecontent" name="sourcecontent" value="<%=currentNode.getPath()%>"></input>
					<input type="hidden" id="rt" name="rt" value="gad/source" />
					<div id="button1" class="button">Upload</div>
				</div>
			</div>
			
			<div class=	"clear"></div>
			
			<div id="actmenu2">
				<ul>	
					<li class="shadow"><h4>Acciones</h4></li>					
					<li><a class="delete" href="javascript:show('divattach')" >&nbsp;&nbsp;&nbsp;adjuntar<a/>
						<br/><br/>
						<div id="divattach" style="display:none;">
						  &nbsp;&nbsp;&nbsp;<%=ComboLang.buildComboLang("languages", "es_ES", "combo")%><br/>
						  &nbsp;&nbsp;&nbsp;<select id="document_type" name="document_type" class="combo"> 
						  	<option value="subtitle" selected="selected">Subt&iacute;tulo</option>
						  	<option value="audiodescription">Audio</option>
						  </select>
						  <div id="dataupload2">
								<div class="wrapper">
									<label>[Nombre]:</label><br/>
									<input type="text" id="sourcename2" name="sourcename2" value=""></input><br/>
									<input type="hidden" id="sourcecontent2" name="sourcecontent2" value="<%=currentNode.getPath()%>/transcriptions"></input>
									<input type="hidden" id="rt2" name="rt2" value="gad/source" />
									<div id="button2" class="button">Upload</div>
								</div>
							</div>	
						</div>
					</li>
					<li class="finli">&nbsp;</li>
					
					<li><a class="delete" href="javascript:show('divcopiarfuente')" >&nbsp;&nbsp;&nbsp;copiar fuente<a/>
						<div id="divcopiarfuente" style="display:none;">
						  &nbsp;&nbsp;&nbsp;dest:&nbsp;
						  <input type="text" id="copyName" name="copyName" value=""></input><br/>
						  <select id="contentToCopy" name="contentToCopy" class="combo"> <!-- Rellenamos con los contenidos de esta colección -->
						  <%
						  String contentsPath = currentNode.getPath().substring(0,currentNode.getPath().lastIndexOf("/")); //...sources
						  contentsPath = contentsPath.substring(0,contentsPath.lastIndexOf("/")); //...c1
						  contentsPath = contentsPath.substring(0,contentsPath.lastIndexOf("/")); //contents
						  QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
						  Query query = queryManager.createQuery("/jcr:root/"+contentsPath+"/element(*, nt:unstructured)[@sling:resourceType = 'gad/content'] order by @title descending", "xpath");
						  
						  NodeIterator itcontents = query.execute().getNodes();
						  while(itcontents.hasNext()){
							  Node node = itcontents.nextNode();
							  if(node.hasProperty("title")){
							  	String [] splitPath = node.getPath().split("/");
						  %>
						  <option value="<%=contentsPath+"/"+splitPath[splitPath.length-1]%>"><%=node.getProperty("title").getString()%></option>
						  <%}} %>
						  </select>		
						  <input type="button" class="btcopiar" onclick="javascript:copySource('<%=currentNode.getPath()%>','true')" value="" />	
						</div>
					</li>
					<li class="finli">&nbsp</li>
				</ul>
			</div>
		
		</div><!-- End menu -->
	
	</div><!--End maincontainer-->

	<%@ include file="/apps/administration/footer/footer.jsp" %>

</body>
</html>
