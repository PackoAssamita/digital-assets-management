<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
Node transcriptions_node = null;

if (currentNode.hasNode("transcriptions")) {
	transcriptions_node = currentNode.getNode("transcriptions");    			
%>
	  

<div id="caja">
		<div id="transcriptions" class="corner-up"><b>Transcriptions:</b></div>
   	<%
		NodeIterator it_t1 = transcriptions_node.getNodes();
		Integer x=0;
		while(it_t1.hasNext()){
			Node nodeLang = it_t1.nextNode(); // .../transcriptions/es_ES
			NodeIterator it_t2 = nodeLang.getNodes();
			while(it_t2.hasNext()){
				Node nodeType = it_t2.nextNode(); // .../transcriptions/es_ES/subtitle
				NodeIterator it_t3 = nodeType.getNodes();
				while(it_t3.hasNext()){
//					Node nodeFormat = it_t3.nextNode(); // .../transcriptions/es_ES/subtitle/srt
//					NodeIterator it_t4 = nodeFormat.getNodes();
//					while(it_t4.hasNext()){
						Node nodeT = it_t3.nextNode(); // .../transcriptions/es_ES/subtitle/srt/the_listener_01x01_i
						String [] parts = nodeT.getPath().split("/");
						%>
						<div class="objeto">
						<%if (nodeT.hasProperty("sling:resourceType") && "gad/source".compareTo(nodeT.getProperty("sling:resourceType").getValue().getString())==0) {%>
							<a class="delete" href="javascript:eliminar('<%=nodeT.getPath()%>')" >eliminar<a/>
							<a class="url" href="<%=nodeT.getPath()%>.admin">
							&nbsp;&nbsp;>&nbsp;<%=parts[parts.length-3]%>&nbsp;>&nbsp;<%=parts[parts.length-2]%>&nbsp;>&nbsp;<%=parts[parts.length-1]%><br />
							</a>
							<% if ( nodeT.hasProperty("file") && "".compareTo(nodeT.getProperty("file").getValue().getString())!=0 ) { %>
								<div class="source_files">
								<p><%= nodeT.getProperty("file").getValue().getString() %>&nbsp;
								<a href="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getOriginalUrl(nodeT)%>"><img class="middle" title="Download" alt="Download" src="/apps/administration/static/images/download_icon.png" /></a></p>
								</div>
							<%}%>
						<%}%>
						</div>
						<%
//					}
				}
			}
		}
		%>
	<div class="corner-down"></div>
</div><!-- fin caja -->
<%}%>
