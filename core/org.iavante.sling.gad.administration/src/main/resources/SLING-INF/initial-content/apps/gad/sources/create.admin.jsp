<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ include file="/apps/commons/util.jsp" %>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>

<body>
<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
    <div id="data">


					<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">
						title<br />
						<input class="input" type="text" id="title" />
									
						<br />file<br />
						<input class="input" type="text" id="file" />
									
						<br />mimetype<br />
						<input class="input" type="text" id="mimetype" />

						<br />text_encoding<br />
						<input class="input" type="text" id="text_encoding" />
								
						<br />type<br />
						<input class="input" type="text" id="type" />
									
						<br />bitrate<br />
						<input class="input" type="text" id="bitrate" />
									
						<br />tags<br />
						<input class="input" type="text" id="tags" />
									
						<br />tracks_number<br />
						<input class="input" type="text" id="tracks_number" />
									
						<br />track_1_type<br />
						<input class="input" type="text" id="track_1_type" />
								
						<br />track_1_encoding<br />
					        <input class="input" type="text" id="track_1_encoding" />
					
						<br />track_1_features<br />
						<input class="input" type="text" id="track_1_features" />
									
						<br />track_2_type<br />
						<input class="input" type="text" id="track_2_type" />

						<br />track_2_encoding<br />
						<input class="input" type="text" id="track_2_encoding" />
					
						<br />track_2_features<br />
						<input class="input" type="text" id="track_2_features" />
					
						<br />lang<br />
						<%=ComboLang.buildComboLang("lang", "es_ES", "input")%>
							
						<br />length<br />
						<input class="input" type="text" id="lengths" />
						
						<br />size<br />
						<input class="input" type="text" id="size" />
					
						<!--		jcr:lastModifiedBy  -->
						<input class="input" type="hidden" id="jcr:lastModifiedBy" value="" disabled="disabled" />
					
						<!--		sling:resourceType  -->
						<input class="input" type="hidden" id="sling:resourceType" value="gad/source"  />
				
						<!--		jcr:createdBy -->
						<input class="input" type="hidden" id="jcr:createdBy" value="" disabled="disabled" />
	
						<!--		jcr:lastModified -->
						<input class="input" type="hidden" id="jcr:lastModified"  disabled="disabled" />
					</form>

      <input type="button" class="guardar" onclick="javascript:crear('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>
				
    </div>
</div>

	<%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>
