<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>

<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">

	  	<div id="container">

   	<div id="propiedades">	
	<% PropertyIterator it2 = currentNode.getProperties(); 
		while(it2.hasNext()){
			Property p = it2.nextProperty();
			if (!p.getDefinition().isMultiple()) {%>
			<input type="text" id="<%=p.getName()%>" value="<%=p.getValue().getString()%>" disabled="disabled" /><br /><%}}%>
	</div>
		<div id="subcontainer">
		<div id="maintitle">

<%
String[] xdir = currentNode.getPath().split("/"); 
String primera = xdir[3].substring(0,1);
String resto = xdir[3].substring(1);
String todo = primera.toUpperCase() + resto.toLowerCase();
String section = xdir[2];
%>

<b><%= todo %></b></div>	

<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String PathInfo = request.getPathInfo();

PathInfo = PathInfo.replace(".admin", "");


String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "descending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/revision'] order by @" + order_by + " " + order, "xpath");
NodeIterator it = query.execute().getNodes();

if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}

Integer x=0;

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
} %>

<div id="separador">

<%
while (it.hasNext() && count != 0) {
Node node = it.nextNode(); 
x++; %>

 <% if(x!=1){%><div id="separador2"></div><%}%>

    <a class="url2" href="<%=node.getPath() %>.admin">&nbsp;&nbsp;&nbsp;
 	<%
 	String content_id = "";
 	if (node.hasProperty("title")) { content_id = node.getProperty("title").getValue().getString(); }
 	else { content_id = node.getName(); }
 	%>
 	<b><%=content_id%></b></a><br />

<% if(todo.equals("Pendientes")){%>
	<a class="delete" href="javascript:denegar('<%= node.getPath() %>', 'true')" >denegar la revisi&oacute;n<a/>
	<a class="delete" href="javascript:revisar('<%= node.getPath() %>', 'true')" >aceptar la revisi&oacute;n<a/><% }%>

<% if(todo.equals("Revisados")){%>
	<a class="delete" href="javascript:denegar('<%= node.getPath() %>', 'true')" >denegar la revisi&oacute;n<a/><% }%>

<% if(todo.equals("Denegados")){%>
	<a class="delete" href="javascript:eliminar('<%= node.getPath() %>')" >eliminar<a/>
	<a class="delete" href="javascript:revisar('<%= node.getPath() %>', 'true')" >aceptar la revisi&oacute;n<a/><% }%>
	
<% if(section.equals("canales") && currentNode.getPath().indexOf("unpublished")==-1){%>
	<a class="delete" href="javascript:unpublish('<%= node.getPath() %>', 'true')" >Despublicar<a/><% }%>

<br/><br/>


<% count--; %>
<%}%>
</div>

	</div><!-- End sources -->
     </div><!-- End data -->

</div><!-- End maincontainer-->

	<%@ include file="/apps/administration/footer/footer.jsp" %>
</body>
</html>
