<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ include file="/apps/gad/util.jsp" %>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="org.apache.jackrabbit.value.StringValue"%>

<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />
<%

String url_playlist ="";
try{
	url_playlist=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getXSPFUrl(currentNode.getPath());
}catch(Exception e){}

String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);

String tviconUrl = "no_logo_in_revision";
if( currentNode.hasProperty("needTvicon") 
		&& "1".compareTo(currentNode.getProperty("needTvicon").getValue().getString().trim())==0
		&& currentNode.getParent().getParent().hasNode("multimedia_assets")){
	Node assets = currentNode.getParent().getParent().getNode("multimedia_assets");
	if (assets.hasNode("tvicon/transformations/image_preview")) {
		tviconUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getTviconUrl(currentNode, "internal");
	}
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
	<script type="text/javascript">

	function prepararPublicar(url, aux){

		var indexChannel = document.getElementById("channel").selectedIndex;
		var dest = document.getElementById("channel").options[indexChannel].value;

		publicar(url, dest, aux);
	}

	function actualizarCategoriasCanal(){

		var indexChannel = document.getElementById("channel").selectedIndex;
		var dest = document.getElementById("channel").options[indexChannel].value;

		var url = dest + "/cats.json";

		//Get categories of tests channel

		var xmlHttp = getXmlHttpObject();
		xmlHttp.open("GET", url, true);

		//Send the proper header information along with the request
		xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlHttp.setRequestHeader("Connection", "close");

		xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlHttp.readyState == 4) {
				alert(xmlHttp.responseText);
				document.location.href=document.location.href;
			}
		}
		xmlHttp.send(null);
	}
	
</script>
</head>

<body onload="javascript:inicio_content('<%=url_playlist%>', '<%=tviconUrl %>')">

<%@ include file="/apps/administration/header/header.jsp" %>
							
<div id="maincontainer">						
<div id="container">
    <div id="data">
	<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">

<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("description");
	etiquetas.add("author");
	etiquetas.add("origin");
	etiquetas.add("lang");
	etiquetas.add("tags");

	String tagName = "";
	String string_value = "";
	
 	for(int i=0; i<etiquetas.size(); i++){
   
 		tagName = (String) etiquetas.get(i);
		string_value = "";
		Property prop = null;
		if(currentNode.hasProperty(tagName)){
			prop = currentNode.getProperty(tagName);
			if (prop.getDefinition().isMultiple()) {
				Value[] values = prop.getValues();		
				for (int j=0; j<values.length;j++) {
					string_value = string_value + values[j].getString() + ", ";
				}		
				if (string_value.length() != 0) {
			    	string_value = string_value.substring(0, string_value.length() - 2);
			    	prop = null;
				}
			}	
			else {
				string_value = prop.getValue().getString();
				prop = null;
			}%>	
		<%}	 %>
<%= tagName%>
<input class="input" type="text" id="<%= tagName%>" value="<%=string_value%>" 
	<%if ("tags".compareTo(tagName)!=0){%>disabled="disabled"<%}%>
	/><br />
<%}%>

<%
	tagName = "categories";
	string_value = "";
	if(currentNode.hasProperty(tagName)){ 
		Property prop = null;
		prop = currentNode.getProperty(tagName);
		if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
				string_value = string_value + values[j].getString() + ", ";
			}		
			if (string_value.length() != 0) {
		    	string_value = string_value.substring(0, string_value.length() - 2);
		    	prop = null;
			}
		}
	}
%>
categories
<input class="input" type="text" id="<%= tagName%>" value="<%=string_value%>" disabled="disabled" /><br />



<%
	tagName = "votes";
	string_value = "";
	if(currentNode.hasProperty(tagName)){ 
		Property prop = null;
		prop = currentNode.getProperty(tagName);
		if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
				string_value = string_value + values[j].getString() + ", ";
			}		
			if (string_value.length() != 0) {
		    	string_value = string_value.substring(0, string_value.length() - 2);
		    	prop = null;
			}
		}
	}
%>
votes
<input class="input" type="text" id="<%= tagName%>" value="<%=string_value%>" disabled="disabled" /><br />

schema
<input class="input" type="text" id="schema" value="<% if(currentNode.hasProperty("schema")){ %><%=currentNode.getProperty("schema").getValue().getString()%><%}%>" disabled="disabled" /><br />

version
<input class="input" type="text" id="version" value="<% if(currentNode.hasProperty("version")&&!(currentNode.getProperty("version").getDefinition().isMultiple())){ %><%=currentNode.getProperty("version").getValue().getString()%><%}%>" disabled="disabled" /><br />

path
<input class="input" type="text" id="path" value="" disabled="disabled" /><br />

urlInternal
<input class="input" type="text" id="urlInternal" value="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode)%>" disabled="disabled" /><br />

urlExternal
<input class="input" type="text" id="urlExternal" value="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode)%>" disabled="disabled" /><br />

sling:resourceType<br />
<input class="input" type="text" id="sling:resourceType" value="<% if(currentNode.hasProperty("sling:resourceType")) { %><%=currentNode.getProperty("sling:resourceType").getValue().getString()%><%}%>" disabled="disabled" /><br />

created<br />
<input class="input" type="text" id="jcr:created" value="<% if(currentNode.hasProperty("jcr:created")) { %><%=currentNode.getProperty("jcr:created").getValue().getString()%><%}%>" disabled="disabled" /><br />

createdBy<br />
<input class="input" type="text" id="jcr:createdBy" value="<% if(currentNode.hasProperty("jcr:createdBy")) { %><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModified<br />
<input class="input" type="text" id="jcr:lastModified"value="<% if(currentNode.hasProperty("jcr:lastModified")) { %><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModifiedBy<br />
<input class="input" type="text" id="jcr:lastModifiedBy" value="<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

needEnding<br />
<input class="input" type="text" id="needEnding" value="<% if(currentNode.hasProperty("needEnding")) { %><%=currentNode.getProperty("needEnding").getValue().getString()%><%}%>" /><br />

needTvicon<br />
<input class="input" type="text" id="needTvicon" value="<% if(currentNode.hasProperty("needTvicon")) { %><%=currentNode.getProperty("needTvicon").getValue().getString()%><%}%>" /><br />

<input class="input" type="hidden" id="tags@TypeHint" value="String[]" />
<input class="input" type="hidden" id="votes@TypeHint" value="String[]" />

</form>
<input type="button" class="guardar" onclick="javascript:saveRevision('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/><hr />

<div>
<%@ include file="/apps/administration/widgets/sources.jsp" %>
</div>

</div><!-- End data -->
  <div id="video">
	<script type="text/javascript" src="/apps/administration/static/flash/swfobject.js"></script>
	<a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.	
  </div>
</div><!-- End container -->  

<div id="menu">
<%
String[] xdir = currentNode.getPath().split("/"); 
String primera = xdir[3].substring(0,1);
String resto = xdir[3].substring(1);
String todo = primera.toUpperCase() + resto.toLowerCase();
%>

<div id="actmenu2">

<ul>	
  <li class="shadow"><h4>Acciones</h4></li>

<% if(!todo.equals("Denegados") && !todo.equals("Revisados") && !todo.equals("Pendientes")){%>
		
<li><a class="delete" href="javascript:show('divvotar')" >&nbsp;&nbsp;&nbsp;votar<a/>
	<div id="divvotar" style="display:none;">
	  &nbsp;&nbsp;&nbsp;<input type="text" id="addvote" size="19" name="addvote" /><br />	
	  &nbsp;&nbsp;&nbsp;<input type="button" class="btnewvote" onclick="javascript:votar('<%=currentNode.getPath()%>')" value="" />	
	</div>
</li>
<li><a class="delete" href="javascript:show('divcategorias')" >&nbsp;&nbsp;&nbsp;categor&iacute;as<a/>
	<!-- Checks con todas las categorias de un canal -->
	<div id="divcategorias" style="display:none;">
		<%
		//Fill up a list with categories already assigned
		List listCategories = new ArrayList();
		if (currentNode.hasProperty("categories")) {
			Property prop = currentNode.getProperty("categories");
			if (prop.getDefinition().isMultiple()) {
				Value[] values = prop.getValues();
				for(int i=0; i<values.length; i++){
					listCategories.add(values[i].getString());
				}
			}
		}		
		
		//Get the channel
		//String pathCats = currentNode.getPath().replaceFirst("contents/.+","cats");
		String pathCats = xdir[0]+"/"+xdir[1]+"/"+xdir[2]+"/"+xdir[3]+"/cats" ;
		Node node = currentNode.getSession().getRootNode().getNode(pathCats.replaceFirst("/",""));
		NodeIterator itCat = node.getNodes();
		while(itCat.hasNext()){
			Node nodeCat = itCat.nextNode();
			String catChecked = listCategories.contains(nodeCat.getPath()) ? "checked=\"checked\"" : "";
			%>
			&nbsp;&nbsp;&nbsp;<input type="checkbox" id="<%=nodeCat.getPath()%>" name="<%=nodeCat.getPath()%>" size="12" <%=catChecked%>>
			<%=nodeCat.getProperty("title").getString()%>
			</input><br/>
			<%
			if(nodeCat.hasNodes()){
				NodeIterator itSubcat = nodeCat.getNodes();
				while(itSubcat.hasNext()){
					Node nodeSubcat = itSubcat.nextNode();
					String subcatChecked = listCategories.contains(nodeSubcat.getPath()) ? "checked=\"checked\"" : "";
					%>
					&nbsp;&nbsp;&nbsp;<input type="checkbox" id="<%=nodeSubcat.getPath()%>" name="<%=nodeSubcat.getPath()%>" size="12" <%=subcatChecked%>>
						<%=nodeCat.getProperty("title").getString()%>/<%=nodeSubcat.getProperty("title").getString()%>
					</input><br/>
					<%
				}
			}
		}
		%>
	  &nbsp;&nbsp;&nbsp;<input type="button" class="btasignar" onclick="javascript:asignarCategoria('<%=currentNode.getPath()%>')" value="" />	
	</div>
</li>

<% if(currentNode.getPath().indexOf("unpublished")==-1){%>
<li>
	&nbsp;&nbsp;&nbsp;<a class="delete" href="javascript:unpublish('<%= currentNode.getPath() %>', 'false')" >despublicar<a/>
</li>
<% }%>

<li class="finli">&nbsp</li>

<% }else if(todo.equals("Denegados")){%>

<li><a class="delete" href="javascript:revisar('<%= currentNode.getPath() %>','false')" >&nbsp;&nbsp;&nbsp;aceptar la revisi&oacute;n<a/></li>
<li class="sub"><a class="delete" href="javascript:eliminar('<%= currentNode.getPath() %>','false')" >&nbsp;&nbsp;&nbsp;eliminar<a/></li>
<li class="finli">&nbsp</li>

<% }else if(todo.equals("Revisados")){%>

<li><a class="delete" href="javascript:denegar('<%= currentNode.getPath() %>','false')" >&nbsp;&nbsp;&nbsp;denegar la revisi&oacute;n<a/></li>
<li><a class="delete" href="javascript:show('divpublicar')" >&nbsp;&nbsp;&nbsp;publicar la revisi&oacute;n<a/>
	<div id="divpublicar" style="display:none;">
	  <select id="channel" name="channel" class="combo"> <!-- Rellenamos con los canales existentes -->
	  <%
	  QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
	  Query query = queryManager.createQuery("/jcr:root//content/canales/element(*, nt:unstructured)[@sling:resourceType = 'gad/channel'] order by @title descending", "xpath");
	  
	  NodeIterator itchannels = query.execute().getNodes();
	  while(itchannels.hasNext()){
		  Node node = itchannels.nextNode();
		  if(node.hasProperty("title")){
	  %>
	  <option value="<%=node.getPath()%>"><%=node.getProperty("title").getString()%></option>
	  <%}} %>
	  </select>	
	  <input type="button" class="btpublicar" onclick="javascript:prepararPublicar('<%= currentNode.getPath() %>','false')" value="" />	
	</div>
</li>
<li class="finli">&nbsp</li>

<% }else if(todo.equals("Pendientes")){%>

<li><a class="delete" href="javascript:revisar('<%= currentNode.getPath() %>','false')" >&nbsp;&nbsp;&nbsp;aceptar la revisi&oacute;n<a/></li>	
<li class="sub"><a class="delete" href="javascript:denegar('<%= currentNode.getPath() %>','false')" >&nbsp;&nbsp;&nbsp;denegar la revisi&oacute;n<a/></li>
<li class="finli">&nbsp</li>

<% }%>
</ul>
</div>


<div>
<%@ include file="/apps/administration/widgets/tags.jsp" %>
</div>
<div>
<%@ include file="/apps/administration/widgets/lognode.jsp" %>
</div>

</div><!-- End menu -->
</div><!-- End maincontainer -->
<%@ include file="/apps/administration/footer/footer.jsp" %>
</body>
</html>
