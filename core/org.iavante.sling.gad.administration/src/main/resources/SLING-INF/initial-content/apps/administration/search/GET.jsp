<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="javax.jcr.*,
        org.apache.sling.api.resource.Resource"%>


<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
	String url = request.getRequestURL().toString();
	String uri = request.getRequestURI();
	String aux = url.replaceAll(uri,"");
	pageContext.setAttribute("urlBase", aux);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Interfaz de Admnistraci&oacute;n</title>
	<link rel="stylesheet" href="/apps/administration/static/css/file.css">
	<script type="text/javascript" src="/apps/administration/static/js/webtoolkit.base64.js"></script>
	
	<script type="text/javascript">

		//Initial content loader
		function inicio(basicUrl, key){
			if(key!=null){
				//var enc = Base64.encode(key);
				buscar(basicUrl+key);
			}
		}
		
		//Return a XmlHttp object
		function getXmlHttpObject(){
			var xmlHttp = null;
			// Mozilla, Opera, Safari, Internet Explorer 7
			if (typeof XMLHttpRequest != 'undefined') {
			    xmlHttp = new XMLHttpRequest();
			}
			if (!xmlHttp) {
			    // Internet Explorer 6
			    try {
			        xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
			    } catch(e) {
			        try {
			            xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
			        } catch(e) {
			            xmlHttp  = null;
			        }
			    }
			}
			return xmlHttp;
		}

		//Search a string in JCR
		function buscar(url){
			// now some ajax
			// the selector and extension '.-1.json' will return the complete thread in json format
			var xmlHttp = getXmlHttpObject();
		    xmlHttp.open("GET", url, true);
		    xmlHttp.onreadystatechange = function(){            
		    	switch(xmlHttp.readyState) {
		        	case 4:
		            	if(xmlHttp.status!=200) {
		                	alert("error buscar:"+xmlHttp.status); 
		                }else{ 
			                //document.getElementById("resultadosBusqueda").innerHTML = mostrarResultados(xmlHttp.responseText);
			                document.getElementById("search_results").innerHTML = mostrarResultados(xmlHttp.responseText);
		                }
		                break;
		        
		            default:
		                return false;
		            	break;     
		        }
		    };
		    xmlHttp.send(null);	
		}

		//Show search results
		function mostrarResultados(responseText){ 

			var resultados = '';
			var response = eval("(" + responseText + ")");
			var x=0;

			if (response.length!=0){
			resultados += '<div id="subcontainer">';}
			

			for(i=0; i<response.length; i++) {
				var tipo = response[i].resourceType.replace(/([a-z]*\/)+/,"").replace(/(^\W+)/,"").replace(/\W+$/,"");
				var showDelete = false;
				x++;
			
				if (x!=1){resultados += '<div id="separador2"></div>';}
				
				if(tipo=='collection' || tipo=='content' || tipo=='source' || tipo=='transformation' || tipo=="revision" || tipo == "channel"){
					showDelete = true;
					//resultados += '<a href="/administration/' + tipo + '?nodo=' + response[i].path + '&type=' + tipo + '&option=2">';
					resultados += '<a class="url" href="' + response[i].path + '.admin">';
				}
				else{

					resultados += '<a class="url" href="' + response[i].path + '">';
				}

					var name = response[i].path;
					var names = name.split("/");
					name = names[names.length-1];
					name = name.replace(/\W/, '');								
					
				resultados += '&nbsp;&nbsp;&nbsp;' + name + '</a>';
				if(showDelete){
					resultados += '    &nbsp;<a class="delete" href="javascript:eliminar(\'' +response[i].path+ '\',\'<%=pageContext.getAttribute("authorization")%>\')" style="color:#f00;">Eliminar</a>&nbsp;';
				}
				resultados += '<div id="res' + i + '" class="resNivel1" style="font-family: verdana; font-size: 10px; color: black">'+ response[i].extract + '</div>';
				
			}

			resultados += '</div>';
			
			return resultados;
		}

		//Send a DELETE
		function eliminar(url, auth){
			// now some ajax
			// the selector and extension '.-1.json' will return the complete thread in json format
			
			if (window.confirm("¿Está seguro de eliminar?")){
			
				var xmlHttp = getXmlHttpObject();
				xmlHttp.open("DELETE", url, true);
	
				//Send the proper header information along with the request
				xmlHttp.setRequestHeader("Authorization", auth);
			    
			    xmlHttp.onreadystatechange = function(){            
			    	switch(xmlHttp.readyState) {
			        	case 4:
			            	if(xmlHttp.status!=200 && xmlHttp.status!=204) {
			                	alert("error buscar:"+xmlHttp.status); 
			                }else{
				                alert("Eliminación satisfactoria");
			                	document.location.href=document.location.href; 
			                }
			                break;
			        
			            default:
			                return false;
			            	break;     
			        }
			    };
			    xmlHttp.send(null);	
		    }
		}
		
	</script>
	
</head>
<body onload="javascript:inicio('/administration/searchAPI?encoding=base64&type=gad/content&key=','<%=request.getParameter("qt")%>')">

<div id="header">
		<%@ include file="/apps/administration/header/header.jsp" %>
</div>


<div id="maincontainer">	
	<div id="container">
	  <div id="maintitle">Resultados</div>
	  <div id="search_results"></div>
	</div>	
</div>
	

	<div id="footer">
		<%@ include file="/apps/administration/footer/footer.jsp" %>
	</div>
</body>
</html>
