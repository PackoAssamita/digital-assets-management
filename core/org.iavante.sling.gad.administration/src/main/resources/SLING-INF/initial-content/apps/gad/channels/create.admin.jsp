<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.osgi.framework.Bundle" %>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.commons.services.ITranscoderService"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>
<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
    <div id="data">
				<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">			
						title<br />
						<input class="input" type="text" id="title" />
						
						<br />subtitle<br />
						<input class="input" type="text" id="subtitle" />
										
						<br />picture<br />
						<input class="input" type="text" id="picture" />
						
						<br />description<br />
						<input class="input" type="text" id="description" />
								
						<br />distribution_server<br />
						<select class="input" type="text" id="distribution_server" onchange="showDistributionServerConfig();">
						<% 
						List<String> dist_servers = sling.getService(org.iavante.sling.commons.services.DistributionServiceProvider.class).list_distribution_servers();
						Iterator it = dist_servers.iterator();
						while (it.hasNext()) {
							String service = (String)it.next();%>	
							<option value="<%=service%>"><%=service%></option>				
						<%}%>
						</select>
						<br />Configuraci&oacute;n personalizada</br>
						<textarea rows="5" cols="70" id="distribution_server_config">
						</textarea>

						<br />distribution_format_video<br />
						<select multiple class="input" type="text" id="distribution_format_video">
						<% 
						Map<String, String> conversion_types = sling.getService(org.iavante.sling.commons.services.ITranscoderService.class).getConversionTypes("video");
						it = conversion_types.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();%>
							<option value="<%=e.getKey()%>"><%=e.getKey() + " - " + e.getValue()%></option>							
						<%}%>
						</select>
						
						<br />distribution_format_image<br />
						<select multiple class="input" type="text" id="distribution_format_image">
						<% 
						conversion_types = sling.getService(org.iavante.sling.commons.services.ITranscoderService.class).getConversionTypes("image");
						it = conversion_types.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();%>
							<option value="<%=e.getKey()%>"><%=e.getKey() + " - " + e.getValue()%></option>							
						<%}%>
						</select>
						
						<br />schema<br />
						<!-- <input class="input" type="text" id="distribution_format" />-->
						<select class="input" type="text" id="schema">
						<% 
						Map<String, String> schema_types = sling.getService(org.iavante.sling.commons.services.GADContentCreationService.class).getSchemas("channel");
						it = schema_types.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();%>
							<option value="<%=e.getKey()%>"><%=e.getKey() + " - " + e.getValue()%></option>							
						<%}%>
						</select>
												
						<!-- <input class="input" type="text" id="schema" value="default" />-->
					
						<!-- jcr:created -->
						<input class="input" type="hidden" id="jcr:created" value="" disabled="disabled" />
								
						<!-- jcr:lastModified -->
						<input class="input" type="hidden" id="jcr:lastModified" value="" disabled="disabled" />
									
						<!-- jcr:lastModifiedBy -->
						<input class="input" type="hidden" id="jcr:lastModifiedBy" value="" disabled="disabled" />
								
						<!-- sling:resourceType -->
						<input class="input" type="hidden" id="sling:resourceType" value="gad/channel" disabled="disabled" />
									
						<!-- jcr:createdBy -->
						<input class="input" type="hidden" id="jcr:createdBy" value="" disabled="disabled" />	
						
						<input class="input" type="hidden" id="_charset_" value="utf-8" />
								
				</form>

					
	<input type="button" class="guardar" onclick="javascript:crear('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>
			
	</div>
</div>

	<%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>

