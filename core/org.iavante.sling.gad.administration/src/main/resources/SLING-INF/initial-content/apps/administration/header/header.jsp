<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<!-- ################################################################################################### Parte 1 -->
<div id="header">
<div id="mainheader">
	<%@ include file="/apps/administration/search/searchBox.jsp" %>
		<ul class="menuheader">
			<li><% out.println("<a href=\"/colecciones.admin\">Colecciones&nbsp;&nbsp;&nbsp;</a>");%></li>
			<li>|</li>
			<ul class="menu" onmouseover="tagle('sub');" onmouseout="tagle('sub');">
			<li><a href="/catalogo.admin">&nbsp;&nbsp;&nbsp;Cat&aacute;logo&nbsp;&nbsp;&nbsp;</a></li>
				<ul class="sub-menu" id="sub">
					<li><a href="/catalogo/pendientes.admin">Pendientes</a></li>
					<div class="sep"></div>
					<li><a href="/catalogo/revisados.admin">Revisados</a></li>
					<div class="sep"></div>
					<li class="ult"><a href="/catalogo/denegados.admin">Denegados</a></li>

				</ul>
			</ul>
			<li>|</li>
			<li><% out.println("<a href=\"/canales.admin\">&nbsp;&nbsp;&nbsp;Canales&nbsp;&nbsp;&nbsp;</a>");%></li>
			<li>|</li>
			<li><% out.println("<a href=\"/config.admin\">&nbsp;&nbsp;&nbsp;Configuraci&oacute;n</a>");%></li>
		</ul>
</div>
</div>


<div id="subheader">
	<div id="mainsubheader">
		<img class="logo" src="/apps/administration/static/images/logo_junta.jpg" />
		<img class= "banner" src="/apps/administration/static/images/baner.jpg" />
	</div>
</div>

<%

String[] dir = currentNode.getPath().split("/"); 
String cont="/";

if ((dir[2].equals("jcr:versionStorage")) || (dir[2].equals("search"))){
	out.println( "");	
}else{ %>

<div id="dir">

	<div id="maindir">
	

<%
out.println("<a href=\"/administration\">Gad&nbsp;&nbsp;|&nbsp;&nbsp;</a>");
for(int x=2;x<dir.length;x++){

	cont += dir[x];


	if((x+1)!=dir.length){
		out.println( "<a href=\"" + cont + ".admin\">" + dir[x] + "</a>");	

	}else{
		out.println( "<a class='final' href=\"" + cont + ".admin\">" + dir[x] + "</a>");
	}

	if((x+1)!=dir.length){
	out.println("&nbsp;&nbsp;|&nbsp;&nbsp;");
	cont += "/";}
	}}%>

</div>
</div>

<!-- ################################################################################################### -->

