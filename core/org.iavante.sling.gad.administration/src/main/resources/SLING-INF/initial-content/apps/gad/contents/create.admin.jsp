<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ include file="/apps/commons/util.jsp" %>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>
<body>
<%@ include file="/apps/administration/header/header.jsp" %>
<div id="maincontainer">
    <div id="data">
					<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">
						title<br />
						<input class="input" type="text" id="title" />					
						<br />origin<br />
						<input class="input" type="text" id="origin" />							
						<br />description<br />
						<input class="input" type="text" id="description" />									
						<!--	sling:resourceType -->
						<input class="input" type="hidden" id="sling:resourceType" value="gad/content"  />										
                        <br />schema<br />
						<select class="input" type="text" id="schema">
						<% 
						
						Node collection_node = currentNode.getParent();
						String supported_schemas = "";
						if (collection_node.hasProperty("schemas")) {
							supported_schemas = collection_node.getProperty("schemas").getValue().getString();
						}
						
						String[] supported_schemas_arr = supported_schemas.split(",");
						for (int i=0; i<supported_schemas_arr.length; i++) { 
							String schema_name = supported_schemas_arr[i];
							String schema_description = "";
						    Node schema = currentNode.getSession().getRootNode().getNode("content/schemas/content/" + schema_name);
						    if (schema.hasProperty("description")) {
						    	schema_description = schema.getProperty("description").getValue().getString();						    	
						    }
						    %>
						    <option value="<%=schema_name%>"><%=schema_name + " - " + schema_description%></option>							
							
						<% }%>						
						</select>
						<!--	jcr:created -->
						<input class="input" type="hidden" id="jcr:created" value="" disabled="disabled" />						
            <br />lang<br />
						<%=ComboLang.buildComboLang("lang", "es_ES", "input")%>								
						<br />author<br />
						<input class="input" type="text" id="author" />								
						<br />state<br />
						<input class="input" type="text" id="state" />								
						<!--	jcr:createdBy -->
						<input class="input" type="hidden" id="jcr:createdBy" disabled="disabled" />
						<br />tags<br />
						<input class="input" type="text" id="tags" />								
						<!--	jcr:lastModified -->
						<input class="input" type="hidden" id="jcr:lastModified" value="" disabled="disabled" />
						<!--	jcr:primaryType -->
						<input class="input" type="hidden" id="jcr:primaryType" value="nt:unstructured" disabled="disabled" />						
						<!--     tags declaration type -->
						<input class="input" type="hidden" id="tags@TypeHint" value="String[]" />
						<input class="input" type="hidden" id="_charset_" value="utf-8" />
					</form>					
    <input type="button" class="guardar" onclick="javascript:crear('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>					
	</div>
  </div>
<%@ include file="/apps/administration/footer/footer.jsp" %>
</body>
</html>