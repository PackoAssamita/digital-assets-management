/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Publishes a content directly in a list of channels.
 */
public class DirectPublication implements Serializable, Runnable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(ContentServiceImpl.class);

	/** Make everything in a thread. */
	private Thread t;
	/** Node to publish. */
	private String nodePath;
	/** Content name. */
	private String contentName;
	/** Channels where node has to be published. */
	private String channels;
	/** User and passw */
	private String userPass;

	/** Configuration variables */
	private String CHANNEL_URL = "/content/canales/";
	private String CATALOG_URL = "/content/catalogo/";
	private String PENDING_FOLDER = "pendientes";
	private String REVISED_FOLDER = "revisados";
	private String UNPUBLISHED_FOLDER = "unpublished";
	private String REVISION_RESOURCE_TYPE = "gad/revision";
	private String CATEGORY_RESOURCE_TYPE = "gad/categories";
	private String REVISIONS_RESOURCE_TYPE = "gad/revisions";

	/** Sling http basic url. */
	private String HTTP_BASE_URL = "";

	/** Http client. */
	private HttpClient client;
	/** Default credentials. */
	private Credentials defaultCreds;
	/** Admin credentials. */
	private Credentials userCreds;
	/** Authentication preferences. */
	private List authPrefs = new ArrayList(2);

	// ------------ Constructors -------------------

	public DirectPublication(String userPass, String httpBaseUrl,
			String nodePath, String channels) {

		this.nodePath = nodePath;
		this.channels = channels;
		this.userPass = userPass;
		this.HTTP_BASE_URL = httpBaseUrl;

		String[] aux = nodePath.split("/");
		this.contentName = aux[aux.length - 1];

		this.t = new Thread(this, "Thread for direct publication");

		setup();

		if (log.isInfoEnabled())
			log.info("nodePath=" + this.nodePath + ", channels=" + this.channels
					+ ", HTTP_BASE_URL=" + this.HTTP_BASE_URL);

		this.t.start();
	}

	/*
	 * @see java.lang.Runnable
	 */
	public void run() {

		if (log.isInfoEnabled())
			log.info("run, ini");

		try {

			// 1. Send content to revision.
			sendContentToRevision();
			
			Thread.sleep(2000);

			// 2. Accept the revision.
			acceptRevision();
			
			Thread.sleep(2000);

			// 3. Publish the revision in the channels.
			publishToChannels();

		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("run, end");
	}

	// ------------ Internal ---------------------
	/**
	 * Initial http client configuration.
	 */
	private void setup() {
		client = new HttpClient();
		// defaultCreds = new UsernamePasswordCredentials(authInfo.getUserID(),
		// authInfo.getPassword().toString());
		
		int colIdx = userPass.indexOf(':');
		if (colIdx < 0) {
			userCreds = new UsernamePasswordCredentials(userPass);
		} else {
			userCreds = new UsernamePasswordCredentials(userPass.substring(0, colIdx), userPass
					.substring(colIdx + 1));
		}

		defaultCreds = new UsernamePasswordCredentials("admin", "admin");
		
		authPrefs = new ArrayList(2);
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultCreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	}

	/**
	 * Sends a content to revision.
	 * 
	 * @throws IOException
	 * @throws HttpException
	 * @throws RepositoryException
	 */
	private void sendContentToRevision() throws HttpException, IOException {

		PostMethod post = new PostMethod(HTTP_BASE_URL + nodePath);
		String catalog_pending_url = CATALOG_URL + PENDING_FOLDER + "/";

		post.setDoAuthentication(true);

		NameValuePair[] data_send_to_revision = {
				new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest", catalog_pending_url),
				new NameValuePair(":replace", "true") };

		int responseCode = -1;
		post.setRequestBody(data_send_to_revision);

		try {
			client.getState().setCredentials(AuthScope.ANY, userCreds);
			client.executeMethod(post);
			responseCode = post.getStatusCode();
		} finally {
			post.releaseConnection();
		}

		if (responseCode != 201 && responseCode != 200) {
			throw new IOException("sendContentToRevision, code=" + responseCode);
		}

	}

	/**
	 * Accepts a revision and inserts it into the catalog.
	 * 
	 * @throws IOException
	 * @throws HttpException
	 */
	private void acceptRevision() throws HttpException, IOException {

		PostMethod post = new PostMethod(HTTP_BASE_URL + CATALOG_URL
				+ PENDING_FOLDER + "/" + contentName);
		String catalog_revised_url = CATALOG_URL + REVISED_FOLDER + "/";

		post.setDoAuthentication(true);

		NameValuePair[] data_revise_content = {
				new NameValuePair(":operation", "move"),
				new NameValuePair(":dest", catalog_revised_url),
				new NameValuePair(":replace", "true") };

		int responseCode = -1;
		post.setRequestBody(data_revise_content);

		try {
			client.getState().setCredentials(AuthScope.ANY, userCreds);
			client.executeMethod(post);
			responseCode = post.getStatusCode();
		} finally {
			post.releaseConnection();
		}

		if (responseCode != 201 && responseCode != 200) {
			throw new IOException("acceptRevision, code=" + responseCode);
		}

	}

	/**
	 * Publishes the revision to the specified channels.
	 * 
	 * @throws IOException
	 * @throws HttpException
	 */
	private void publishToChannels() throws HttpException, IOException {

		String[] channelNames = channels.trim().split(",");

		for (int i = 0; i < channelNames.length; i++) {

			PostMethod post = new PostMethod(HTTP_BASE_URL + CATALOG_URL
					+ REVISED_FOLDER + "/" + contentName);
			String channel = CHANNEL_URL + channelNames[i] + "/contents/";

			post.setDoAuthentication(true);

			NameValuePair[] data_publish = { new NameValuePair(":operation", "copy"),
					new NameValuePair(":dest", channel),
					new NameValuePair(":replace", "true") };

			int responseCode = -1;
			post.setRequestBody(data_publish);

			try {
				client.getState().setCredentials(AuthScope.ANY, userCreds);
				client.executeMethod(post);
				responseCode = post.getStatusCode();
			} finally {
				post.releaseConnection();
			}

			if (responseCode != 201 && responseCode != 200) {
				throw new IOException("publishToChannels, code=" + responseCode);
			}

		}
	}

}
