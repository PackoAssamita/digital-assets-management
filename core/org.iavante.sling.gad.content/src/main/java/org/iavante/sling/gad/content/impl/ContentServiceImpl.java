/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.lang.Boolean;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.version.Version;
import javax.jcr.version.VersionException;
import javax.jcr.version.VersionIterator;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.jackrabbit.value.StringValue;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.JcrTools;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.content.ContentService;
import org.iavante.sling.gad.content.ContentTools;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener implementation.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Content listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class ContentServiceImpl implements ContentService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private JcrTools jcrTool;

	/** @scr.reference */
	private ContentTools contentTool;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Authentication properties. */
	private Map<String, String> slingProperties = null;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(ContentServiceImpl.class);

	/** Sling http basic url. */
	private String HTTP_BASE_URL = "";

	/** JCR path to content shemas. */
	private final String SCHEMAS_PATH = "content/schemas/content";

	/** Path to register in the observer. */
	private final String REGISTERED_PATH = "/content/colecciones";

	/** Path to revisions in catalog. */
	private final String CATALOG_PATH = "/content/catalogo/revisados";

	/** Path to revisions in catalog. */
	private final String CHANNELS_PATH = "/content/canales";

	/** Content sling:resourceType. */
	private final String CONTENT_RESOURCE_TYPE = "gad/content";

	/** Contents sling:resourceType. */
	private final String CONTENTS_RESOURCE_TYPE = "gad/contents";

	/** Source sling:resourceType. */
	private final String SOURCE_RESOURCE_TYPE = "gad/source";

	/** Source sling:resourceType. */
	private final String REVISION_RESOURCE_TYPE = "gad/revision";

	/** Log node sling:resourceType. */
	private final String LOG_RESOURCE_TYPE = "gad/log";

	/** Path to save the new created node JCR path */
	private final String PATH_PROP = "jcr_path";

	/** Visited prop. */
	private final String VISITED_PROP = "hits";

	/** Title prop. */
	private final String TITLE_PROP = "title";

	/** From prop. */
	private final String FROM_PROP = "from";

	/** Name of the versionable log node. */
	private final String LOG_NODE = "log";

	/** Gad uuid property. */
	private final String GADUUID = "gaduuid";

	/** Tags property. */
	private final String TAG_PROP = "tags";

	/** Validated prop. */
	private final String VALIDATED_PROP = "validated";

	/** Finish prop. */
	private final String FINISH_PROP = "finish";

	/** Collections node name. */
	private final String COLLECTIONS_NODE_NAME = "colecciones";

	/** Root node. */
	private Node rootNode = null;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			rootNode = session.getRootNode();

			slingProperties = portalSetup.get_config_properties("/sling");
			HTTP_BASE_URL = slingProperties.get("sling_url");

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager
						.addEventListener(this, Event.NODE_ADDED | Event.NODE_REMOVED
								| Event.PROPERTY_ADDED | Event.PROPERTY_REMOVED,
								REGISTERED_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {

		boolean firstEdition = false;
		Node modifiedNode = null;

		while (eventIterator.hasNext()) {
			Event event = eventIterator.nextEvent();
			String propPath = "";
			try {
				propPath = event.getPath();
			} catch (RepositoryException e1) {
				e1.printStackTrace();
			}

			// New Content
			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = rootNode.getNode(event.getPath().substring(1));
					log.info("Node added: " + addedNode.getPath().toString());
					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getString().equals(
								CONTENT_RESOURCE_TYPE)) {

							Node from = isCloned(addedNode);

							if (from == null) { // not cloned

								log.info("if 1");

								// Add mixin node types form versioning
								addedNode.addMixin("mix:versionable");
								addedNode.addMixin("mix:referenceable");
								addedNode.save();

								// Associate schema
								createSchema(addedNode);

								// Add path property
								addedNode.setProperty(PATH_PROP, addedNode.getPath());
								if (log.isInfoEnabled())
									log.info("Property path created");

								// Add visited property
								addedNode.setProperty(VISITED_PROP, "0");
								if (log.isInfoEnabled())
									log.info("Property visited created");

								// Add gaduuid property
								addedNode.setProperty(GADUUID, addedNode.getUUID());

								log.info("Created versioning");

								// Add history node
								if (log.isInfoEnabled())
									log.info("Creating log node");
								createLogNode(addedNode);

								if (addedNode.hasProperty(TAG_PROP)) {

									Value[] tags = addedNode.getProperty(TAG_PROP).getValues();
									Value[] tagsNormal = new Value[tags.length];
									String collection = propPath.split("/")[3];
									Node collectionNode = rootNode.getNode("content/"
											+ COLLECTIONS_NODE_NAME + "/" + collection);
									for (int i = 0; i < tags.length; i++) {
										String tagNormal = gadContentTool.normalizeTag(tags[i]
												.getString());
										tagsNormal[i] = new StringValue(tagNormal);
										gadContentTool.updateTags(collectionNode, tagsNormal[i]
												.getString());
									}

									addedNode.setProperty(TAG_PROP, tagsNormal);
									addedNode.save();
								}

								addedNode.setProperty(FINISH_PROP, "1");
								addedNode.setProperty(VALIDATED_PROP, "0");
								addedNode.save();
								if (log.isInfoEnabled())
									log.info("Added node saved");
							} // No param ':operation'

							else {

								log.info("if 2");

								// Update gaduuid property
								String fromGaduuid = from.getProperty(GADUUID).getValue()
										.getString();
								String oldGaduuid = addedNode.getProperty(GADUUID).getValue()
										.getString();
								addedNode.setProperty(GADUUID, addedNode.getUUID());
								if (log.isInfoEnabled()) {
									log.info("Property 'gaduuid' updated, old value: "
											+ oldGaduuid);
									log.info("Property 'gaduuid' updated, new valued: "
											+ addedNode.getProperty(GADUUID).getValue().getString());
								}

								// Update from property
								addedNode.setProperty(FROM_PROP, fromGaduuid);
								if (log.isInfoEnabled())
									log.info("Property 'from' changed");

								// Update the 'copies' property in from node
								Value[] copies = new Value[1];
								if (!from.hasProperty("copies")) {
									copies[0] = new StringValue(addedNode.getProperty(GADUUID)
											.getValue().getString());
								} else {
									Value[] copiesBefore = from.getProperty("copies").getValues();
									copies = new Value[copiesBefore.length + 1];
									for (int i = 0; i < copiesBefore.length; i++) {
										copies[i] = copiesBefore[i];
									}
									copies[copies.length - 1] = new StringValue(addedNode
											.getProperty(GADUUID).getValue().getString());
								}
								from.setProperty("copies", copies);
								// from.save();

								// update child list
								updateFromChildList(from);

								from.save();

								if (log.isInfoEnabled())
									log.info("node " + addedNode.getPath()
											+ " is a copy from node " + from.getPath());

								// Update title
								String[] parts = addedNode.getPath().split("/");
								String newTitle = parts[parts.length - 1];
								addedNode.setProperty(TITLE_PROP, newTitle);

								// Update from property
								addedNode.setProperty(FROM_PROP, fromGaduuid);
								if (log.isInfoEnabled())
									log.info("Property 'from' changed");

								// Update path property
								addedNode.setProperty(PATH_PROP, addedNode.getPath());
								if (log.isInfoEnabled())
									log.info("Property 'path' changed");

								// Delete log node
								addedNode.getNode("log").remove();
								if (log.isInfoEnabled())
									log.info("Log node removed");

								// Create log node
								createLogNode(addedNode);

								addedNode.save();

							}
						}
					}

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}

			// node removed
			else if ((event.getType() == Event.NODE_REMOVED)) {

				Node parentNode;
				try {

					// log.info("removedNode: " + event.getPath().substring(1));

					String[] cads = event.getPath().substring(1).split("/");

					if (cads.length == 5) {
						String parentPath = cads[0] + "/" + cads[1] + "/" + cads[2] + "/"
								+ cads[3];

						parentNode = rootNode.getNode(parentPath);

						// log.info("parentNode: " + parentNode.getPath());

						if (parentNode.hasProperty("sling:resourceType")) {
							if (parentNode.getProperty("sling:resourceType").getString()
									.equals(CONTENTS_RESOURCE_TYPE)) {

								NodeIterator it = parentNode.getNodes();
								while (it.hasNext()) {
									Node child = it.nextNode();
									if (child.hasProperty("sling:resourceType")
											&& CONTENT_RESOURCE_TYPE.equals(child.getProperty(
													"sling:resourceType").getValue().getString())
											&& child.hasProperty("copies")) {

										if (log.isInfoEnabled())
											log.info("onEvent, updating copies params for: "
													+ child.getPath());

										updateFromChildList(child);
									}
								}

							}
						}
					}
				} catch (PathNotFoundException e) {
					e.printStackTrace();
				} catch (RepositoryException e) {
					e.printStackTrace();
				}

			}

			// Content edited
			else if ((event.getType() == Event.PROPERTY_REMOVED)) {

				// log.error("Node edit");
				int len = propPath.split("/").length;
				int pos = propPath.lastIndexOf('/');
				String nodePath = propPath.substring(0, pos);

				try {
					modifiedNode = rootNode.getNode(nodePath.substring(1));

					if (propPath.endsWith(TAG_PROP)) {

						if (modifiedNode.getProperty(("sling:resourceType")).getString()
								.equals(CONTENT_RESOURCE_TYPE)) {

							Value[] tags = modifiedNode.getProperty(TAG_PROP).getValues();
							Value[] tagsNormal = new Value[tags.length];
							String collection = propPath.split("/")[3];
							Node collectionNode = rootNode.getNode("content/"
									+ COLLECTIONS_NODE_NAME + "/" + collection);
							for (int i = 0; i < tags.length; i++) {

								String tagNormal = gadContentTool.normalizeTag(tags[i]
										.getString());
								tagsNormal[i] = new StringValue(tagNormal);
								gadContentTool.updateTags(collectionNode, tagsNormal[i]
										.getString());
							}

							modifiedNode.setProperty(TAG_PROP, tagsNormal);
							modifiedNode.save();
						}
					}

					if (propPath.endsWith("jcr:lastModified")) {

						if ((modifiedNode.hasProperty("sling:resourceType"))
								&& (SOURCE_RESOURCE_TYPE.equals(modifiedNode.getProperty(
										"sling:resourceType").getValue().getString()))) {
							Node content = modifiedNode.getParent().getParent();
							if (contentTool.is_valid_content(content)) {
								content.setProperty(VALIDATED_PROP, "1");
							} else {
								content.setProperty(VALIDATED_PROP, "0");
							}
							content.save();
						}
					}

					if (!firstEdition) {

						firstEdition = true;
						String resourceTypeModified = modifiedNode.getProperty(
								"sling:resourceType").getString();
						if (resourceTypeModified.equals(CONTENT_RESOURCE_TYPE)) {

							// Version the node
							if (!modifiedNode.hasProperty("mix:versionable")) {
								modifiedNode.addMixin("mix:versionable");
							}

							if (!modifiedNode.hasProperty("mix:referenceable")) {
								modifiedNode.addMixin("mix:referenceable");
							}

							VersionIterator vi;
							vi = modifiedNode.getVersionHistory().getAllVersions();

							String lastVersion = "";
							while (vi.hasNext()) {
								Version v = vi.nextVersion();
								if (v.getSuccessors().length == 0) {
									lastVersion = v.getName();
								}
							}

							String currentVersion = getNextVersionName(lastVersion);
							if (log.isInfoEnabled())
								log.info("Version: " + currentVersion);
							modifiedNode.setProperty("version", currentVersion);

							session.save();
							if (log.isInfoEnabled())
								log.info("Session saved");
							modifiedNode.checkin();
							modifiedNode.checkout();
						}
					}

				} catch (PathNotFoundException e) {
					e.printStackTrace();
				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}

		}
	}

	/*
	 * @see org.iavante.sling.gad.content.ContentService
	 */
	public int copySources(ServletRequest request, ServletResponse response,
			String nodePath) {

		if (log.isInfoEnabled())
			log.info("copySources, ini");

		int code = 0;

		Enumeration en2 = request.getParameterNames();
		while (en2.hasMoreElements()) {
			String cad = (String) en2.nextElement();
			if (log.isInfoEnabled())
				log.info("copySources, Parameter: " + cad);
		}

		String dest = request.getParameter("dest");
		String replace = request.getParameter("replace");
		if (replace == null) {
			replace = "true";
		}

		String[] aux = nodePath.split("/");
		String contentRelPath = aux[aux.length - 1];

		try {

			// copy all sources to the dest sources folder
			NodeIterator it = rootNode.getNode(nodePath.substring(1) + "/sources")
					.getNodes();
			while (it.hasNext()) {
				Node source = it.nextNode();
				if (source.hasProperty("sling:resourceType")
						&& SOURCE_RESOURCE_TYPE.equals(source.getProperty(
								"sling:resourceType").getValue().getString())) {

					// Get the content gaduuid
					String contentId = "";
					if (source.getParent().getParent().hasProperty(GADUUID)) {
						contentId = source.getParent().getParent().getProperty(GADUUID)
								.getValue().getString();
					} else {
						contentId = source.getParent().getParent().getUUID();
					}

					// New source name
					String[] cads = source.getPath().split("/");
					String name = cads[cads.length - 1];
					name = name.endsWith("_default") ? name.replace("_default", "_from")
							: name;
					// name += "_" + contentId;
					name += "_" + contentRelPath;
					String destSourcePath = dest + "/sources/" + name;

					if (log.isInfoEnabled())
						log.info("copySources, source: " + source.getPath() + ", dest:"
								+ destSourcePath);

					// Copy the source to 'dest' content
					rootNode.getSession().getWorkspace().copy(source.getPath(),
							destSourcePath);

					// Set property schema=playlist
					rootNode.getNode(destSourcePath.substring(1)).setProperty("schema",
							"playlist");

					rootNode.getNode(destSourcePath.substring(1)).save();

				}
			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("copySources, end");

		return code;
	}

	/*
	 * @see org.iavante.sling.gad.content.ContentService
	 */
	public int pendindRevisionPublish(ServletRequest request,
			ServletResponse response, String nodePath, String channels) {

		if (log.isInfoEnabled())
			log.info("pendindRevisionPublish, ini");

		int code = 0;

		// 0. Extract authentication info.
		SlingHttpServletRequest req = (SlingHttpServletRequest) request;
		String credentials = authTools.extractUserPass(req);

		if (credentials != null) {

			// 1. Check censor permissions.
			// This is already done in PermissionFilter

			// 2. Check publisher permissions.
			// This is already done in PermissionFilter

			// 3. Direct publication.
			DirectPublication dp = new DirectPublication(credentials, HTTP_BASE_URL,
					nodePath, channels);

		} else {
			code = 1;
		}

		return code;
	}

	/*
	 * @see org.iavante.sling.gad.content.ContentService
	 */
	public int pendindRevisionPublish(ServletRequest request,
			ServletResponse response, String nodePath, String channels,
			boolean isAdmin) {

		if (log.isInfoEnabled())
			log.info("pendindRevisionPublish, ini");

		int code = 0;

		// 0. Extract authentication info.
		slingProperties = portalSetup.get_config_properties("/sling");
		String credentials = slingProperties.get("sling_user") + ":"
				+ slingProperties.get("sling_pass");

		if (credentials != null) {

			// 1. Check censor permissions.
			// This is already done in PermissionFilter

			// 2. Check publisher permissions.
			// This is already done in PermissionFilter

			// 3. Direct publication.
			DirectPublication dp = new DirectPublication(credentials, HTTP_BASE_URL,
					nodePath, channels);

		} else {
			code = 1;
		}

		return code;
	}

	/*
	 * @see org.iavante.sling.gad.content.ContentService
	 */
	public int requestUnpublish(ServletRequest request, ServletResponse response,
			String nodePath) {

		int code = 0;
		Node node = null;

		if (log.isInfoEnabled())
			log.info("requestUnpublish, unpublication requested for node: "
					+ nodePath);

		String gaduuid = "";
		try {
			node = session.getRootNode().getNode(nodePath.substring(1));
			if (node.hasProperty("gaduuid")) {
				gaduuid = node.getProperty("gaduuid").getValue().getString();
			}
		} catch (PathNotFoundException e) {
			log.error("requestUnpublish, node not found, path: " + node);
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("requestUnpublish, node.gaduuid: " + gaduuid);

		// Search in catalog all revisions with 'gaduuid'
		int num = markInCatalog(gaduuid, "unpublicationRequested", "1");

		if (log.isInfoEnabled())
			log.info("requestUnpublish, revisions marked in catalog: " + num);

		// Search in channels all revisions with 'gaduuid'
		num = markInChannels(gaduuid, "unpublicationRequested", "1");

		if (log.isInfoEnabled())
			log.info("requestUnpublish, revisions marked in channels: " + num);

		return code;
	}

	// ------------------- Internal ----------------------

	/**
	 * Check if a node is copied from another one
	 * 
	 * @param node
	 * @return The genuine node
	 */
	private Node isCloned(Node node) {

		if (log.isInfoEnabled())
			log.info("isCloned, ini");

		boolean res = false;
		Node from = null;

		String id = "gaduuid";

		// check if exists any content with the same gaduuid
		try {

			if (node.hasProperty(id)) {

				String nodeId = node.getProperty(id).getValue().getString();
				NodeIterator it = node.getParent().getNodes();
				while (it.hasNext() && !res) {
					Node child = it.nextNode();
					if (child.hasProperty(id)) {
						String childId = child.getProperty(id).getValue().getString();
						if (nodeId.compareTo(childId) == 0) {
							res = true;
							from = child;
						}
					}
				}
			}

		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("isCloned, end");

		return from;
	}

	/**
	 * Update the gaduuid list for the contents cloned
	 * 
	 * @param from
	 */
	private void updateFromChildList(Node from) {

		if (log.isInfoEnabled())
			log.info("updateFromChildList, ini");

		String copies = "copies";

		try {
			if (from.hasProperty(copies)) {

				String ids = brothers(from.getParent(), from.getProperty(GADUUID)
						.getValue().getString());

				Value[] newValues = null;
				if ("".compareTo(ids) != 0) {
					String[] cads = ids.split(",");
					newValues = new Value[cads.length];
					for (int i = 0; i < newValues.length; i++) {
						newValues[i] = new StringValue(cads[i]);
						// log.info("newCopies[" + i + "]=" + newValues[i].getString());
					}
				}
				from.setProperty(copies, newValues);
			}
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("updateFromChildList, end");

	}

	/**
	 * Check if there are 2 or more contents with the same gaduuid
	 * 
	 * @param parent
	 * @param id
	 * @return
	 */
	private String brothers(Node parent, String fromId) {

		if (log.isInfoEnabled())
			log.info("brothers, ini");

		String brothers = "";

		NodeIterator it;
		try {
			it = parent.getNodes();
			while (it.hasNext()) {
				Node child = it.nextNode();
				if (child.hasProperty(FROM_PROP)
						&& fromId.compareTo(child.getProperty(FROM_PROP).getValue()
								.getString()) == 0) {
					brothers += child.getProperty(GADUUID).getValue().getString() + ",";
				}
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if ("".compareTo(brothers) != 0) {
			brothers = brothers.replace(",$", "");
		}

		if (log.isInfoEnabled())
			log.info("brothers, fromId:" + fromId + ", brothers: " + brothers);

		if (log.isInfoEnabled())
			log.info("brothers, end");

		return brothers;
	}

	/**
	 * Mark all revisions with 'property'='value'
	 * 
	 * @param id
	 * @param propertyName
	 * @param value
	 * @return num of revisions marked.
	 */
	private int markInCatalog(String id, String propertyName, String value) {
		int numMarked = 0;

		try {
			QueryManager queryManager = session.getWorkspace().getQueryManager();
			Query query = queryManager.createQuery("/jcr:root/" + CATALOG_PATH
					+ "/element(*, nt:unstructured)[@sling:resourceType = '"
					+ REVISION_RESOURCE_TYPE + "' and @gaduuid = '" + id
					+ "'] order by @title descending", "xpath");

			NodeIterator it = query.execute().getNodes();
			Node revision = null;
			while (it.hasNext()) {
				revision = it.nextNode();
				revision.setProperty(propertyName, value);
				numMarked++;

				if (log.isInfoEnabled())
					log.info("markInCatalog, node requested to unpublish: "
							+ revision.getPath());
			}

			if (numMarked > 0) {
				revision.getParent().save();
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return numMarked;
	}

	/**
	 * Mark all revisions with 'property'='value'
	 * 
	 * @param id
	 * @param propertyName
	 * @param value
	 * @return num of revisions marked.
	 */
	private int markInChannels(String id, String propertyName, String value) {
		int numMarked = 0;

		Node channels = null;
		NodeIterator itChannels = null;
		try {
			channels = session.getRootNode().getNode(CHANNELS_PATH.substring(1));
			itChannels = channels.getNodes();
		} catch (PathNotFoundException e) {
			log.error("markInChannels, node not found, path: " + channels);
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		while (itChannels.hasNext()) {

			Node channel = itChannels.nextNode();

			try {
				QueryManager queryManager = session.getWorkspace().getQueryManager();
				Query query = queryManager.createQuery("/jcr:root/" + channel.getPath()
						+ "/contents"
						+ "/element(*, nt:unstructured)[@sling:resourceType = '"
						+ REVISION_RESOURCE_TYPE + "' and @gaduuid = '" + id
						+ "'] order by @title descending", "xpath");

				NodeIterator it = query.execute().getNodes();
				Node revision = null;
				while (it.hasNext()) {
					revision = it.nextNode();
					revision.setProperty(propertyName, value);
					numMarked++;

					if (log.isInfoEnabled())
						log.info("markInChannels, node requested to unpublish: "
								+ revision.getPath());
				}

				if (numMarked > 0) {
					revision.getParent().save();
				}

			} catch (RepositoryException e) {
				e.printStackTrace();
			}

		}

		return numMarked;
	}

	/**
	 * Add the schema to the addedNode.
	 * 
	 * @param addedNode
	 */
	private void createSchema(Node addedNode) {

		Node schemaNode = null;
		String schema = null;

		// Get schema property
		try {
			schema = addedNode.getProperty("schema").getString();
		} catch (ValueFormatException e1) {
			e1.printStackTrace();
		} catch (PathNotFoundException e1) {
			e1.printStackTrace();
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		log.info("GAD Node Schema: " + schema);
		// Get schema node
		try {
			schemaNode = session.getRootNode().getNode(SCHEMAS_PATH + "/" + schema);
		} catch (PathNotFoundException e) {
			log.info("GAD Schema not found, path:" + SCHEMAS_PATH + "/" + schema);
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		try {
			NodeIterator nodeIterator = schemaNode.getNodes();
			while (nodeIterator.hasNext()) {
				Node node = nodeIterator.nextNode();
				jcrTool.copy(node, addedNode, null);
			}
		} catch (RepositoryException e1) {
			e1.printStackTrace();
		}

		// Save session
		try {
			addedNode.save();
			log.info("GAD Schema associated");
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (InvalidItemStateException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (NoSuchNodeTypeException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create log node.
	 * 
	 * @param parentNode
	 */
	private void createLogNode(Node parentNode) {

		// Create preview node into transformations folder if not exists
		Node logNode = null;

		try {
			if (!parentNode.hasNode(LOG_NODE)) {
				logNode = parentNode.addNode(LOG_NODE, "nt:unstructured");
				logNode.setProperty("sling:resourceType", LOG_RESOURCE_TYPE);
				logNode.setProperty("url", parentNode.getPath().toString());
				logNode.setProperty("state", "");
				logNode.setProperty("version", "");
				logNode.addMixin("mix:versionable");
				logNode.addMixin("mix:referenceable");
				log.info("Log node created");
				parentNode.save();
			}
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchNodeTypeException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the next version name. For example: getNextVersionName(1.0) = 1.1
	 * getNextVersionName(1.9) = 1.10
	 * 
	 * @param currentVersion
	 *          The current version name
	 * @return The next version name
	 */
	private String getNextVersionName(String currentVersion) {

		String nextVersion = "";
		if (currentVersion.equals("jcr:rootVersion")) {
			nextVersion = "1.0";
		} else {

			String integerPart = currentVersion.split("[.]")[0];
			int decimalPart = Integer.parseInt(currentVersion.split("[.]")[1]);
			int nextVersionDec = decimalPart + 1;
			nextVersion = integerPart + "." + nextVersionDec;
		}
		return nextVersion;
	}

}
