/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Operations about content.
 */
public interface ContentService {

	/**
	 * In order: - Send a node to .../catalogo/pendientes/... - Accept the
	 * revision - Publish the revision into a list of channels.
	 * 
	 * @param request
	 * @param response
	 * @param node
	 * @param channels
	 *          List of channels.
	 * @return 0 if successful, 1 if wrong censor permissions, 2 if wrong
	 *         publisher permissions.
	 */
	public int pendindRevisionPublish(ServletRequest request,
			ServletResponse response, String nodePath, String channels);

	/**
	 * In order: - Send a node to .../catalogo/pendientes/... - Accept the
	 * revision - Publish the revision into a list of channels.
	 * 
	 * @param request
	 * @param response
	 * @param node
	 * @param channels
	 *          List of channels.
	 * @param isAdmin
	 * @return 0 if successful, 1 if wrong censor permissions, 2 if wrong
	 *         publisher permissions.
	 */
	public int pendindRevisionPublish(ServletRequest request,
			ServletResponse response, String nodePath, String channels,
			boolean isAdmin);

	/**
	 * The revision related with the content is marked with the property called
	 * 'unpublishRequested', value '1'. That property is also set to that revision
	 * in all channels where it has been published earlier.
	 * 
	 * @param request
	 * @param response
	 * @param nodePath
	 * @return 0 if successful.
	 */
	public int requestUnpublish(ServletRequest request, ServletResponse response,
			String nodePath);

	/**
	 * Copy the sources from a content into another content
	 * 
	 * @param request
	 * @param response
	 * @param nodePath
	 *          of content
	 * @return 0 if successful, 1 if error.
	 */
	public int copySources(ServletRequest request, ServletResponse response,
			String nodePath);
}
