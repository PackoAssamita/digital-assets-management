<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="text/json; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("author");
	etiquetas.add("file");
	etiquetas.add("mimetype");
	etiquetas.add("text_encoding");
	etiquetas.add("type");
	etiquetas.add("bitrate");
	etiquetas.add("tags");
	etiquetas.add("tracks_number");
	etiquetas.add("track_1_type");
	etiquetas.add("track_1_encoding");
	etiquetas.add("track_1_features");
	etiquetas.add("track_2_type");
	etiquetas.add("track_2_encoding");
	etiquetas.add("track_2_features");
	etiquetas.add("lang");
	etiquetas.add("size");
	etiquetas.add("length");
	etiquetas.add("default_subtitle");
	etiquetas.add("default_audiodescription");
%>
<% 
String finish = "0";
if (currentNode.hasProperty("finish")) {
	finish = currentNode.getProperty("finish").getValue().getString();
}
%>

{"Result": {<% for(int i=0; i<etiquetas.size(); i++){String tagName = (String) etiquetas.get(i);%>
"<%= tagName%>": "<% if(currentNode.hasProperty(tagName)){ %><%=currentNode.getProperty(tagName).getValue().getString()%><%}%>",<%}%>
"path": "<%=currentNode.getPath()%>",
"finish" : "<%=finish%>",
"urlInternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode)%>",
"urlExternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode)%>", 
"ticket": "<%=sling.getService(org.iavante.sling.commons.services.ServiceProvider.class).get_service_ticket("uploader")%>",<% if(currentNode.hasProperty("jcr:created")) { %>
"created": "<%=currentNode.getProperty("jcr:created").getValue().getString()%>",
<%}%><% if(currentNode.hasProperty("jcr:createdBy")) { %>"createdBy": "<%=currentNode.getProperty("jcr:createdBy").getValue().getString()%>",
<%}%><% if(currentNode.hasProperty("jcr:lastModified")) { %>"lastModified": "<%=currentNode.getProperty("jcr:lastModified").getValue().getString()%>",
<%}%><% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %>"lastModifiedBy": "<%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%>"
<%}%>}}
