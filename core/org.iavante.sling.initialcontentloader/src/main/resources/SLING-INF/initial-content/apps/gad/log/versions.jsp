<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="javax.jcr.Node"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects/>
<%@ include file="/apps/gad/util.jsp" %>

<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<logs>
<%
VersionIterator vi = currentNode.getVersionHistory().getAllVersions();
while(vi.hasNext()) {
	Version v = vi.nextVersion();	
	Node n = v.getNode("jcr:frozenNode");%>
	<log>
      <created><%=DateUtils.format(v.getCreated())%></created>
      <state><%=n.hasProperty("state") ? n.getProperty("state").getString() : ""%></state>
      <url><%=n.hasProperty("url") ? n.getProperty("url").getString() : ""%></url>
      <version><%=n.hasProperty("version") ? n.getProperty("version").getString() : ""%></version>
    </log>
<%
}%>
</logs>
</Result>
