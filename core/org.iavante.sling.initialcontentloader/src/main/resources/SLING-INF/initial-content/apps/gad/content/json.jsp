<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="text/json; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="javax.jcr.*, 
                javax.jcr.Property, 
                javax.jcr.Value,
			    org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%

String VERSION = "ver";
String version = "";
boolean version_exists = true;

if (request.getParameter(VERSION) != null) {
    version = request.getParameter(VERSION);    
    version_exists = false;
    VersionIterator vi = currentNode.getVersionHistory().getAllVersions();
    while(vi.hasNext()) {
    	Version v = vi.nextVersion();
	String name = v.getName(); %>
        <%if (name.equals(version)) {
			currentNode = v.getNode("jcr:frozenNode");  
      	    version_exists = true;   
		}
    }
}

else {
	if (currentNode.hasProperty("hits")) {
	    int count = Integer.parseInt(currentNode.getProperty("hits").getValue().getString());
	    currentNode.setProperty("hits", Integer.toString(count + 1));
	}
	else {
	    currentNode.setProperty("hits", "0");
	}
	currentNode.save();
}

if (version_exists) {

	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("description");
	etiquetas.add("author");
	etiquetas.add("origin");
	etiquetas.add("lang");
	etiquetas.add("tags");
	etiquetas.add("schema");
	etiquetas.add("version");
	etiquetas.add("validated");
	etiquetas.add("gaduuid");	
	etiquetas.add("hits");
	%>
	
{"Result": { 
<% for(int i=0; i<etiquetas.size(); i++){
	String tagName = (String) etiquetas.get(i);
	String string_value = "";
	Property prop = null;
	if(currentNode.hasProperty(tagName)){
		prop = currentNode.getProperty(tagName);
		if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
				string_value = string_value + values[j].getString() + ",";
			}		
			if (string_value.length() != 0) {
		    	string_value = string_value.substring(0, string_value.length() - 1);
		    	prop = null;
			}
		}	
		else {
			string_value = prop.getValue().getString();
			prop = null;
		}%>	
"<%=tagName%>": <%="\""+ string_value + "\""%>,
	<%}
}%>
<% 
String finish = "0";
if (currentNode.hasProperty("finish")) {
	finish = currentNode.getProperty("finish").getValue().getString();
}
%>
"path": "<%=currentNode.getPath()%>",	
"finish" : "<%=finish%>",
"urlInternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode)%>",
"urlExternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode)%>", 
"thumbnail": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode)%>",
<% if(currentNode.hasProperty("jcr:created")) { %>"created": "<%=currentNode.getProperty("jcr:created").getValue().getString()%>",<%}%>
<% if(currentNode.hasProperty("jcr:createdBy")) { %>"createdBy": "<%=currentNode.getProperty("jcr:createdBy").getValue().getString()%>",<%}%>
<% if(currentNode.hasProperty("jcr:lastModified")) { %>"lastModified": "<%=currentNode.getProperty("jcr:lastModified").getValue().getString()%>",<%}%>
<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %>"lastModifiedBy": "<%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%>"<%}%>
}
}
<% 
} else {%>
{"Result" : "Error: Version name doesn't exist" }
<%}%>