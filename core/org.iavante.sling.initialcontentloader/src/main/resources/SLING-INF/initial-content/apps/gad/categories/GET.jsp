<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<%@ page trimDirectiveWhitespaces="true"%>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
NodeIterator it = currentNode.getNodes();
%>


<cats>
<%
while (it.hasNext()) {
Node node = it.nextNode(); 
%>
<cat>   
    <title><%if(node.hasProperty("title")) { %><%=node.getProperty("title").getValue().getString() %><% } %></title>
	<path><%=node.getPath()%></path>
	<subcats>
	<% if(node.hasNodes()) {
		 NodeIterator node_iter_subcats = node.getNodes();
		 while (node_iter_subcats.hasNext()) {
			 Node node_subcat = node_iter_subcats.nextNode();%>
			 <subcat>
			     <title><% if(node_subcat.hasProperty("title")) {%><%=node_subcat.getProperty("title").getValue().getString() %><%}%></title>
			     <path><%=node_subcat.getPath()%></path>
			 </subcat>
		 <%}		 
	   }
	%>
	</subcats>
	
</cat>
<%}%>
</cats>
</Result>