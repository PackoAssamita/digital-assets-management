<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%><%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import=" javax.jcr.Property"%>
<%@ page import="javax.jcr.Value"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String AUTHOR = "author";
Integer MAX_LEN_DESCRIPTION = 40;
String author_cmd = "";
String order_cmd = "";

if (request.getParameter(AUTHOR) != null) {
    String author = request.getParameter(AUTHOR);
    author_cmd = " [@author = '"+author+"'] ";
}

if (request.getParameter(ORDER) != null) {
    String order = request.getParameter(ORDER);
    if( order.compareToIgnoreCase("inverso") == 0 ){
    	order_cmd = " order by @jcr:created descending ";
    }
}


QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query = queryManager.createQuery("/jcr:root/" + request.getPathInfo() + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/comment']"+author_cmd+order_cmd, "xpath");
NodeIterator it = query.execute().getNodes();
%>

<count><%= Long.toString(it.getSize())%></count>

<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}

%>

<comments>
<%
while (it.hasNext() && count != 0) {
Node node = it.nextNode(); 
%>
<comment>   
    <title><%if(node.hasProperty("title")) { %><%=node.getProperty("title").getValue().getString() %><% } %></title>
	<path><%=node.getPath()%></path>
	<resourceType><%if(node.hasProperty("sling:resourceType")){ %>
	<%=node.getProperty("sling:resourceType").getValue().getString()%>
	<%}%></resourceType>
	<comment><% if(node.hasProperty("comment")) { %><%=node.getProperty("comment").getValue().getString()%><%}%></comment>
	<author><% if(node.hasProperty("author")) { %><%=node.getProperty("author").getValue().getString()%><%}%></author>
	<created><% if(node.hasProperty("jcr:created")) { %><%=node.getProperty("jcr:created").getValue().getString()%><%}%></created>
	<createdBy><% if(node.hasProperty("jcr:createdBy")) { %><%=node.getProperty("jcr:createdBy").getValue().getString()%><%}%></createdBy>
	<lastModified><% if(node.hasProperty("jcr:lastModified")) { %><%=node.getProperty("jcr:lastModified").getValue().getString()%><%}%></lastModified>
    <lastModifiedBy><% if(node.hasProperty("jcr:lastModifiedBy")) { %><%=node.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%></lastModifiedBy>
</comment>
<%count--; %>
<%}%>
</comments>
</Result>
