<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="text/json; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import=" javax.jcr.Property"%>
<%@ page import="javax.jcr.Value"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
{"Result": {
<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
Integer MAX_LEN_DESCRIPTION = 40;

String PathInfo = request.getPathInfo();

PathInfo = PathInfo.replace(".json", "");

String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "ascending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

String description = "";
int i = 0;

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/content'] order by @" + order_by + " " + order, "xpath");
NodeIterator it = query.execute().getNodes();
%>"count": <%= Long.toString(it.getSize())%>,
<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}
long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}
else {
	count = it.getSize();
}
%>	
"contents": [
<% while (it.hasNext() && count != 0) {
i++;
Node node = it.nextNode(); 
String tags = "";
%>
<% if(node.hasProperty("description")) { 
       description = node.getProperty("description").getValue().getString();
	   if (description.length() > MAX_LEN_DESCRIPTION) {
	       description = description.substring(0, MAX_LEN_DESCRIPTION);
	   }
	}%>
<%
    if (node.hasProperty("tags")) {
	    Property prop = node.getProperty("tags");
	    if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
			    tags = tags + values[j].getString() + ",";
			}	
	    }
	    else {
	        tags = prop.getValue().getString();
	    }
	     if (tags.endsWith(",")) {
	    	 tags = tags.substring(0, tags.length() - 1);
	     }
	}%>
{"title":<% if(node.hasProperty("title")) { %><%="\""+node.getProperty("title").getValue().getString()+"\"" %><%}else{%><%= "null"%><%}%>, 
 "path":"<%=node.getPath() %>", 
 "resourceType":<% if(node.hasProperty("sling:resourceType")) { %><%="\""+node.getProperty("sling:resourceType").getValue().getString()+"\"" %><%}else{%><%= "null"%><%}%>, 
 "description":<% if(description.length() > 0) {%>"<%=description%>"<%}else{%><%= "null"%><%}%>, 
 "author":<% if(node.hasProperty("author")) { %>"<%=node.getProperty("author").getValue().getString()%>"<%}%>,
 "author":<% if(node.hasProperty("hits")) { %>"<%=node.getProperty("hits").getValue().getString()%>"<%}%>,
 "schema":<% if(node.hasProperty("schema")) { %>"<%=node.getProperty("schema").getValue().getString()%>"<%}%>,
 "thumbnail": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode)%>",
 "tags":<% if(tags.length() > 0) {%>"<%=tags%>"<%}else{%><%= "null"%><%}%>,
 "created":<% if(node.hasProperty("jcr:created")) { %>"<%=node.getProperty("jcr:created").getValue().getString()%>"<%}else{%><%= "null"%><%}%>,
 "createdBy":<% if(node.hasProperty("jcr:createdBy")) { %>"<%=node.getProperty("jcr:createdBy").getValue().getString()%>"<%}else{%><%= "null"%><%}%>,
 "lastModified":<% if(node.hasProperty("jcr:lastModified")) {%>"<%=node.getProperty("jcr:lastModified").getValue().getString()%>"<%}else{%><%= "null"%><%}%>,
 "lastModifiedBy":<% if(node.hasProperty("jcr:lastModifiedBy")) {%>"<%=node.getProperty("jcr:lastModifiedBy").getValue().getString()%>"<%}else{%><%= "null"%><%}%>
}
<%count--; %>
<% if(count != 0){%>,<%}%>
<%}%>
]
}
}