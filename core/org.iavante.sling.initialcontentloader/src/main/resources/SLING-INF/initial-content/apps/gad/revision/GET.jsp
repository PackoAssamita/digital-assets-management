<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, 
                 javax.jcr.Property, 
                 javax.jcr.Value,
                 org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%><sling:defineObjects />

<%
if (request.getParameter("onlyvotes") != null) { %>
	<votes_figures>
    <%String number_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes(currentNode);%>
    <%String sum_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes_points(currentNode);%>
    <number><%=number_votes%></number>
    <sum><%=sum_votes%></sum>
</votes_figures>
</Result>
<%
return;
}%>

<%
// Update hits count
if (currentNode.hasProperty("hits")) {
    Integer count = Integer.parseInt(currentNode.getProperty("hits").getValue().getString());
    currentNode.setProperty("hits", count + 1);
}
else {
    currentNode.setProperty("hits", 0);
}
currentNode.save();
%>
<%
String width = "470";
if (request.getParameter("width") != null) {
    width = request.getParameter("width");
}
String height = "320";
if (request.getParameter("height") != null) {
    height = request.getParameter("height");
}
%>
<%
ArrayList etiquetas = new ArrayList();
etiquetas.add("title");
etiquetas.add("description");
etiquetas.add("author");
etiquetas.add("origin");
etiquetas.add("lang");
etiquetas.add("tags");
etiquetas.add("schema");
etiquetas.add("version");
etiquetas.add("gaduuid");
etiquetas.add("hits");
etiquetas.add("votes");
etiquetas.add("unpublicationRequested");
etiquetas.add("needEnding");
etiquetas.add("needTvicon");

for(int i=0; i<etiquetas.size(); i++){
String tagName = (String) etiquetas.get(i);
String string_value = "";
Property prop = null;
if(currentNode.hasProperty(tagName)){
prop = currentNode.getProperty(tagName);
if (prop.getDefinition().isMultiple()) {
Value[] values = prop.getValues();
for (int j=0; j<values.length;j++) {
string_value = string_value + values[j].getString() + ", ";
}
if (string_value.length() != 0) {
    string_value = string_value.substring(0, string_value.length() - 2);
    prop = null;
}
}
else {
string_value = prop.getValue().getString();
prop = null;
}%>
<<%=tagName%>><%=string_value%></<%=tagName%>>
<%}
}%>
<categories>
<%
if (currentNode.hasProperty("categories")) {
   Property prop = currentNode.getProperty("categories");
   if (prop.getDefinition().isMultiple()) {
Value[] values = prop.getValues();
for (int j=0; j<values.length;j++) {
   String category_path = values[j].getString();
   Node root_node = currentNode.getSession().getRootNode();
   Node cat_node = null;
   if (root_node.hasNode(category_path.substring(1))) {
    cat_node = root_node.getNode(category_path.substring(1));
    String title = "";
    if (cat_node.hasProperty("title")) {
    title = cat_node.getProperty("title").getValue().getString();
    %>    
   <category>
     <title><%=title%></title>
     <path><%=category_path%></path>
   </category>
<%
    }    
   }
}
   }    
}
%>
</categories>
<path><%=currentNode.getPath()%></path>
<urlInternal><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode)%>]]></urlInternal>
<% 
String urlExternal = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode);
%>
<urlExternal><![CDATA[<%=urlExternal%>]]></urlExternal>
<% 
String urlThumbnail = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode);
%>
<urlThumbnail><![CDATA[<%=urlThumbnail%>]]></urlThumbnail>
<% 
String urlGif = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getExternalUrl(currentNode, "thumbnail_preview_3");
%>
<urlGif><![CDATA[<%=urlGif%>]]></urlGif>
<% 
String urlSubtitle = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getSubtitleUrl(currentNode, null);
%>
<urlSubtitle><![CDATA[<%=urlSubtitle%>]]></urlSubtitle>
<% 
String urlDownload = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getDownloadUrl(currentNode);
%>
<urlDownload><![CDATA[<%=urlDownload%>]]></urlDownload>


<%
String playlistUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getXSPFUrl(currentNode.getPath());
%>

<%
	Node channel = currentNode.getParent().getParent();
	if ((channel.hasProperty("distribution_format_video") && (!"".equals(channel.getProperty("distribution_format_video").getValue().getString())))) {%>
		<channelUrl>
		<%
		String df = channel.getProperty("distribution_format_video").getValue().getString();
		String[] formats = null;
		
		if (df.contains(",")) {
			formats = df.split(",");
		}
		else {
			formats = new String[1];
			formats[0] = df;
		}
		
		for (int i=0;i<formats.length;i++) {
			String format = formats[i];%>
			<%String url = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getExternalUrl(currentNode, format);%>
			<url format="<%=format%>"><![CDATA[<%=url%>]]></url>
		<%}
		%>
		</channelUrl>
	<%}
%>

<%
String tviconUrl = "";
if( currentNode.hasProperty("needTvicon") 
		&& "1".compareTo(currentNode.getProperty("needTvicon").getValue().getString().trim())==0
		&& currentNode.getParent().getParent().hasNode("multimedia_assets")){
	Node assets = currentNode.getParent().getParent().getNode("multimedia_assets");
	if (assets.hasNode("tvicon/transformations/preview")) {
		tviconUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(assets.getNode("tvicon"));
	}
}
%>

<votes_figures>
    <%String number_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes(currentNode);%>
    <%String sum_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes_points(currentNode);%>
    <number><%=number_votes%></number>
    <sum><%=sum_votes%></sum>
</votes_figures>
<embed>
<% String embed_player = sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_node_player(request, currentNode);%>
<![CDATA[<%=embed_player%>]]>
</embed>
<% if(currentNode.hasProperty("jcr:created")) { %>
<created><%=currentNode.getProperty("jcr:created").getValue().getString()%></created>
<%}%>
<% if(currentNode.hasProperty("jcr:createdBy")) { %>
<createdBy><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%></createdBy>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModified")) { %>
<lastModified><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%></lastModified>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %>
<lastModifiedBy><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%></lastModifiedBy>
<%}%>
<% if(currentNode.hasProperty("pubDate_formated")) { %>
<pubDate><%=currentNode.getProperty("pubDate_formated").getValue().getString()%></pubDate>
<%}%>

</Result>