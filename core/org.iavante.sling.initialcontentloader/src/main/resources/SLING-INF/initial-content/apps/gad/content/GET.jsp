<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="javax.jcr.version.Version"%>
<%@ page import="javax.jcr.version.VersionIterator"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, 
                javax.jcr.Property, 
                javax.jcr.Value,
			    org.apache.sling.api.resource.Resource"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%><sling:defineObjects />

<%
String width = "470";
if (request.getParameter("width") != null) {
    width = request.getParameter("width");
}
String height = "320";
if (request.getParameter("height") != null) {
    height = request.getParameter("height");
}
%>
<%
String VERSION = "ver";
String version = "";
boolean version_exists = true;

if (request.getParameter(VERSION) != null) {
    version = request.getParameter(VERSION);    
    version_exists = false;
    VersionIterator vi = currentNode.getVersionHistory().getAllVersions();
    while(vi.hasNext()) {
    	Version v = vi.nextVersion();
	String name = v.getName(); %>
        <%if (name.equals(version)) {
			currentNode = v.getNode("jcr:frozenNode");  
      	    version_exists = true;   
		}
    }
}
else {	
	if ((currentNode.hasProperty("finish")) && ("1".equals(currentNode.getProperty("finish").getValue().getString()))) {
	  if (currentNode.hasProperty("hits")) {
	    int count = Integer.parseInt(currentNode.getProperty("hits").getValue().getString());
	    currentNode.setProperty("hits", Integer.toString(count + 1));
	  }
	  else {
	    currentNode.setProperty("hits", "0");
	  }
	  currentNode.save();
	}
}
%>

<% if (version_exists) {

	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("description");
	etiquetas.add("author");
	etiquetas.add("origin");
	etiquetas.add("lang");
	etiquetas.add("tags");
	etiquetas.add("schema");
	etiquetas.add("version");
	etiquetas.add("validated");
	etiquetas.add("gaduuid");
	etiquetas.add("hits");
	etiquetas.add("unpublicationRequested");

for(int i=0; i<etiquetas.size(); i++){
	String tagName = (String) etiquetas.get(i);
	String string_value = "";
	Property prop = null;
	if(currentNode.hasProperty(tagName)){
		prop = currentNode.getProperty(tagName);
		if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
				string_value = string_value + values[j].getString() + ",";
			}		
			if (string_value.length() != 0) {
		    	string_value = string_value.substring(0, string_value.length() - 1);
		    	prop = null;
			}
		}	
		else {
			string_value = prop.getValue().getString();
			prop = null;
		}%>			
<<%=tagName%>><%=string_value%></<%=tagName%>>
	<%}		
}%>
<path><%=currentNode.getPath()%></path>
<% 
String finish = "0";
if (currentNode.hasProperty("finish")) {
	finish = currentNode.getProperty("finish").getValue().getString();
}
%>
<finish><%=finish%></finish>
<urlInternal><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request,currentNode)%>]]></urlInternal>
<% 
String urlExternal = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode);

%>
<urlExternal><![CDATA[<%=urlExternal%>]]></urlExternal>

<% 
String urlThumbnail = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode);
%>
<urlThumbnail><![CDATA[<%=urlThumbnail%>]]></urlThumbnail>

<% 
String urlGif = "";
if(currentNode.hasNode("sources/fuente_default/transformations/thumbnail_preview_3")){
	Node nodeGif = currentNode.getNode("sources/fuente_default/transformations/thumbnail_preview_3");
	urlGif = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getExternalUrl(nodeGif, "");
}
%>
<urlGif><![CDATA[<%=urlGif%>]]></urlGif>
<% 
String urlSubtitle = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getSubtitleUrl(currentNode, null);
%>
<urlSubtitle><![CDATA[<%=urlSubtitle%>]]></urlSubtitle>

<%
String playlistUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getXSPFUrl(currentNode.getPath());
%>

<embed>
<% String embed_player = sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_node_player(request, currentNode);%>
<![CDATA[<%=embed_player%>]]>
</embed>
<% if(currentNode.hasProperty("jcr:created")) { %>
<created><%=currentNode.getProperty("jcr:created").getValue().getString()%></created>
<%}%>
<% if(currentNode.hasProperty("jcr:createdBy")) { %>
<createdBy><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%></createdBy>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModified")) { %>
<lastModified><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%></lastModified>
<%}%>
<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %>
<lastModifiedBy><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%></lastModifiedBy>
<%}
}

else { %>
	<error>Version name doesn't exist</error>
<%}%>

</Result>