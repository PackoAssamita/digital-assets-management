/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.services;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Constants {

	public static String DIRUPLOADFILES;
	public static String PARAMDATA;
	public static String PARAMNOTIFICATION;
	public static String PARAMFILENAME;
	public static Long MAXFILESIZE;
	public static String BASICURL = "";
	public static String URLPOST = "";
	public static String PATHFFMPEG = "";
	public static String PATHFILE = "";
	public static String HTTPBASEURL = "";
	public static String SLINGUSER = "";
	public static String SLINGPASS = "";

	public Constants() {
	}

	public static String getDIRUPLOADFILES() {
		return DIRUPLOADFILES;
	}

	public static void setDIRUPLOADFILES(String diruploadfiles) {
		DIRUPLOADFILES = diruploadfiles;
	}

	public static String getPARAMDATA() {
		return PARAMDATA;
	}

	public static void setPARAMDATA(String paramdata) {
		PARAMDATA = paramdata;
	}

	public static String getPARAMNOTIFICATION() {
		return PARAMNOTIFICATION;
	}

	public static void setPARAMNOTIFICATION(String paramnotification) {
		PARAMNOTIFICATION = paramnotification;
	}

	public static Long getMAXFILESIZE() {
		return MAXFILESIZE;
	}

	public static void setMAXFILESIZE(Long maxfilesize) {
		MAXFILESIZE = maxfilesize;
	}

	public static String getBASICURL() {
		return BASICURL;
	}

	public static void setBASICURL(String basicurl) {
		BASICURL = basicurl;
	}

	public static String getURLPOST() {
		return URLPOST;
	}

	public static void setURLPOST(String urlpost) {
		URLPOST = urlpost;
	}

	public static String getPARAMFILENAME() {
		return PARAMFILENAME;
	}

	public static void setPARAMFILENAME(String paramfilename) {
		PARAMFILENAME = paramfilename;
	}

	public static String getPATHFFMPEG() {
		return PATHFFMPEG;
	}

	public static void setPATHFFMPEG(String pATHFFMPEG) {
		PATHFFMPEG = pATHFFMPEG;
	}

	public static String getPATHFILE() {
		return PATHFILE;
	}

	public static void setPATHFILE(String pATHFILE) {
		PATHFILE = pATHFILE;
	}

	public static String getHTTPBASEURL() {
		return HTTPBASEURL;
	}

	public static void setHTTPBASEURL(String hTTPBASEURL) {
		HTTPBASEURL = hTTPBASEURL;
	}

	public static String getSLINGUSER() {
		return SLINGUSER;
	}

	public static void setSLINGUSER(String sLINGUSER) {
		SLINGUSER = sLINGUSER;
	}

	public static String getSLINGPASS() {
		return SLINGPASS;
	}

	public static void setSLINGPASS(String sLINGPASS) {
		SLINGPASS = sLINGPASS;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
