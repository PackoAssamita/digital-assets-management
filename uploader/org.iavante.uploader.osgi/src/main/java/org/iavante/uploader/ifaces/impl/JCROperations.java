/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.uploader.ifaces.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import junit.framework.AssertionFailedError;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.iavante.uploader.services.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Some operations about JCR Initialization.
 */
public class JCROperations
		extends AbstractHttpOperation implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** Admin user name. */
	private String adminUser;

	/** Admin user password. */
	private String adminPasswd;

	/** Default admin user name. */
	private String defaultAdminUser = "admin";

	/** Default admin user password. */
	private String defaultAdminPasswd = "admin";

	/** Anonymous user name. */
	private String anonymousUser = "anonymous";

	// ------------- Constructor ----------------------

	public JCROperations() {
		try {
			init();
		} catch (IOException e) {
			log.error("activate, error assigning permissions to user "
					+ anonymousUser);
			e.printStackTrace();
		}
	}

	private void init() throws IOException {

		if (log.isInfoEnabled())
			log.info("init, ini");

		// Getting admin username and password from global configuration service
		adminUser = Constants.SLINGUSER;
		adminPasswd = Constants.SLINGPASS;

		if (adminUser == null || adminPasswd == null) {
			adminUser = defaultAdminUser;
			adminPasswd = defaultAdminPasswd;
			log.error("init, using default admin username and password");
		} else {
			if (log.isInfoEnabled())
				log.info("init, auth_properties gotten from config node");
		}

		// Setting privileges to 'anonymous' user
		try {
			setAnonymousPrivileges();
		} catch (IOException e) {
			log.error("activate, error assigning permissions to " + anonymousUser);
			e.printStackTrace();
		} catch (JSONException e) {
			log.error("activate, could not check if permissions to " + anonymousUser
					+ " were stablished correctly");
			e.printStackTrace();
		} catch (AssertionFailedError e) {
			log.error("activate, error assigning permissions to " + anonymousUser);
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("init, end");
	}

	// ----------------- Internal ---------------------
	/**
	 * Set privileges to anonymous user in JCR.
	 * 
	 * @throws IOException
	 * @throws JSONException
	 */
	private void setAnonymousPrivileges() throws IOException, JSONException {

		if (log.isInfoEnabled())
			log.info("setAnonymousPrivileges, ini");

		// Root nodes will be applied priveleges from
		List<String> rootNodes = new ArrayList<String>();
		rootNodes.add("/uploader");

		Credentials creds = new UsernamePasswordCredentials(adminUser, adminPasswd);

		// Assigning privileges in every node
		for (String node : rootNodes) {

			String postUrl = HTTP_BASE_URL + node + ".modifyAce.html";
			log.error("postUrl: " + postUrl);
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new NameValuePair("principalId", anonymousUser));
			postParams.add(new NameValuePair("privilege@jcr:all", "granted"));

			assertAuthenticatedPostStatus(creds, postUrl, HttpServletResponse.SC_OK,
					postParams, null);

			// fetch the JSON for the acl to verify the settings:
			// {"anonymous":{"granted":["jcr:all"]}}
			String getUrl = HTTP_BASE_URL + node + ".acl.json";
			String json = getAuthenticatedContent(creds, getUrl, CONTENT_TYPE_JSON,
					null, HttpServletResponse.SC_OK);
			assertNotNull(json);
			JSONObject jsonObj = new JSONObject(json);
			String aceString = jsonObj.getString(anonymousUser);
			assertNotNull(aceString);

			JSONObject aceObject = new JSONObject(aceString);
			assertNotNull(aceObject);

			JSONArray grantedArray = aceObject.getJSONArray("granted");
			assertNotNull(grantedArray);
			assertEquals("jcr:all", grantedArray.getString(0));

			if (log.isInfoEnabled())
				log.info("setAnonymousPrivileges, permissions assigned to "
						+ anonymousUser);
		}

		if (log.isInfoEnabled())
			log.info("setAnonymousPrivileges, end");
	}

}
