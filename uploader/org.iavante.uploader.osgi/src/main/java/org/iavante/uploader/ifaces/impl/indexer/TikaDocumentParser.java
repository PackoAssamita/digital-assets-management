/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.uploader.ifaces.impl.indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;

/**
 * This class the is the bridge between Lucene and Tika. It uses Tika to
 * retrieve the file content and metadata and generates a Lucene document.
 */
public class TikaDocumentParser implements ContentParser {

	/** Default Logger. */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	AutoDetectParser autoDetectParser;
	TikaConfig config;

	public TikaDocumentParser() {
		try {
			// load tika config to replace the image parser with our own
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(
					"tika-config.xml");

			// if(logger.isInfoEnabled()) logger.info("numBytes of tika-config.xml=" +
			// is.available());

			config = new TikaConfig(is);

			// use tika's auto detect parser
			autoDetectParser = new AutoDetectParser(config);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/*
	 * @see org.iavante.uploader.ifaces.impl.indexer.ContentParser
	 */
	public Document getDocument(File file, String mimetype)
			throws ContentParserException {

		InputStream input;
		try {
			input = new FileInputStream(file);

			if (input == null) {
				logger.error("Could not open stream from specified resource: "
						+ file.getName());
			}

			Document doc;

			doc = getDocument(input, mimetype, file.getCanonicalPath());
			// add the file name to the meta data
			if (doc != null) {

				doc.add(new Field("filename", file.getCanonicalPath(), Field.Store.YES,
						Field.Index.NO));

			}
			return doc;

		} catch (FileNotFoundException e) {
			throw new ContentParserException(e);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	// ---------- Internal ----------------

	private Document getDocument(InputStream input, String mimetype, String filename)
			throws ContentParserException {

		if (logger.isInfoEnabled())
			logger.info("getDocument, parser -> " + autoDetectParser.getClass());

		Document doc = null;
		try {
			Metadata metadata = new Metadata();

			if (mimetype != null) {
				metadata.set(Metadata.CONTENT_TYPE, mimetype);
				metadata.set("filename", filename);
			}
			ContentHandler handler = new BodyContentHandler();
			try {
				autoDetectParser.parse(input, handler, metadata);
			} catch (Exception e) {
				throw new ContentParserException(e);
			}

			doc = new Document();
			// add the content to lucene index document
			doc.add(new Field("body", handler.toString(), Field.Store.NO,
					Field.Index.TOKENIZED));

			// add meta data
			String[] names = metadata.names();
			for (String name : names) {
				String value = metadata.get(name);
				doc.add(new Field(name, value, Field.Store.YES, Field.Index.TOKENIZED));
			}

		} finally {
			try {
				input.close();
			} catch (IOException e) {
				throw new ContentParserException(e);
			}
		}

		return doc;
	}
}
