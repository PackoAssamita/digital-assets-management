/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.services;

/**
 * File Management.
 */
public interface IFileManagement {

	/**
	 * Returns a file as byte array.
	 * 
	 * @param name
	 *          filename
	 * @return 
	 * @throws ServiceException
	 */
	public byte[] readFile(String name) throws ServiceException;

	/**
	 * Save a file to disk.
	 * 
	 * @param name
	 *          filename
	 * @param data
	 * @return bytes saved to disk
	 * @throws ServiceException
	 */
	public int writeFile(String name, byte[] data)
			throws ServiceException;

	/**
	 * Delete a file.
	 * 
	 * @param name
	 *          filename
	 * @throws ServiceException
	 */
	public void deleteFile(String name) throws ServiceException;

	/**
	 * Rename a file.
	 * 
	 * @param oldName
	 *          Actual name
	 * @param newName
	 *          New name
	 * @throws ServiceException
	 */
	public void renameFile(String oldName, String newName)
			throws ServiceException;
}
