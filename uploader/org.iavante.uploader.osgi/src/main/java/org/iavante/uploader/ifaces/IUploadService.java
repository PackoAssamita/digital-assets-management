/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.ifaces;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Upload Service operations.
 * 
 * @scr.property name="service.description" value="IAVANTE - Upload Service"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface IUploadService {

	/**
	 * Upload a file.
	 * 
	 * @param request
	 * @param response
	 * @return text with the results of the upload
	 * @throws ServletException
	 * @throws IOException
	 */
	public String uploadFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;

	/**
	 * Remove a file from repository.
	 * @param name of the file
	 * @return
	 * @throws Exception
	 */
	public String removeFile(String name) throws Exception;
}
