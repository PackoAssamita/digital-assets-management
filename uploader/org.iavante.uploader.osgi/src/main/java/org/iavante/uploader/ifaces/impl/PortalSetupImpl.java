/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.uploader.ifaces.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.uploader.ifaces.PortalSetup;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.uploader.ifaces.PortalSetup
 * @scr.component immediate="true"
 * @scr.property name="service.description" value="IAVANTE - PortalSetup"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.uploader.ifaces.PortalSetup"
 */
public class PortalSetupImpl implements PortalSetup, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** Default logger. */
	private static final Logger log = LoggerFactory
			.getLogger(PortalSetupImpl.class);

	/** @scr.reference */
	private SlingRepository repository;

	/** JCR Session. */
	private Session session;

	/** NodeTree Observation Manager. */
	private ObservationManager observationManager;

	/** Sling Component Context. */
	private ComponentContext myComponentContext;

	/** Array of installed bundles. */
	private Bundle[] bundlesInstalled;

	/** Root Node for GAD configuration. */
	private String CONFIG_PATH = "/content/configUploader";

	/** Root node. */
	private Node rootNode = null;

	/** Webdav methods variables */
	private final String WEBDAV_METHOD_PROPFIND = "PROPFIND";
	private final String WEBDAV_METHOD_PROPPATCH = "PROPPATCH";
	private final String WEBDAV_METHOD_MKCOL = "MKCOL";
	private final String WEBDAV_METHOD_COPY = "COPY";
	private final String WEBDAV_METHOD_MOVE = "MOVE";
	private final String WEBDAV_METHOD_LOCK = "LOCK";
	private final String WEBDAV_METHOD_UNLOCK = "UNLOCK";
	private final String WEBDAV_METHOD_OPTIONS = "OPTIONS";

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			this.rootNode = session.getRootNode();

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED
						| Event.NODE_REMOVED | Event.PROPERTY_ADDED, CONFIG_PATH, true,
						null, types, false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}

		// The context will be use to extract the installed bundles
		myComponentContext = context;
		getDistributionBundles();
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see org.iavante.sling.commons.services.PortalSetup
	 */
	public boolean isBundleInstalled(String bundleName) {
		boolean installed = false;
		updateBundlesIsntalled();

		int k = bundlesInstalled.length;
		for (int m = 0; m < k; m++) {
			if (bundlesInstalled[m].getSymbolicName().equals(bundleName)) {
				installed = true;
			}
		}
		return installed;
	}

	/*
	 * @see org.iavante.sling.commons.services.PortalSetup
	 */
	public List<Bundle> getDistributionBundles() {

		List<Bundle> myList = new ArrayList<Bundle>();
		updateBundlesIsntalled();

		int k = bundlesInstalled.length;

		for (int m = 0; m < k; m++) {
			if (bundlesInstalled[m].getSymbolicName().startsWith("org.iavante")) {

				String myBundleType = (String) bundlesInstalled[m].getHeaders().get(
						"Bundle-Type");

				if ((myBundleType == null) || (myBundleType.isEmpty())) {
					// No type
				} else if (myBundleType.equals("Distribution Server")) {
					myList.add(bundlesInstalled[m]);
				}
			}
		}
		return myList;
	}

	/*
	 * @see org.iavante.sling.commons.services.PortalSetup
	 */
	public List<Bundle> getTranscodingBundles() {

		List<Bundle> myList = new ArrayList<Bundle>();
		updateBundlesIsntalled();

		int k = bundlesInstalled.length;

		for (int m = 0; m < k; m++) {
			if (bundlesInstalled[m].getSymbolicName().startsWith("org.iavante")) {

				String myBundleType = (String) bundlesInstalled[m].getHeaders().get(
						"Bundle-Type");
				if ((myBundleType.equals(null)) | (myBundleType.equals(""))) {
					// No type
				} else if (myBundleType.equals("Streaming Server")) {
					myList.add(bundlesInstalled[m]);
				}
			}
		}
		return myList;
	}

	/*
	 * @see org.iavante.sling.commons.services.PortalSetup
	 */
	public Map<String, String> get_config_properties(String type) {

		String propPath = CONFIG_PATH + type;
		Map<String, String> propMap = new HashMap<String, String>();
		Property prop = null;
		try {
			PropertyIterator propIter = this.rootNode
					.getNode(propPath.substring(1)).getProperties();
			while (propIter.hasNext()) {
				prop = propIter.nextProperty();
				propMap.put(prop.getName(), prop.getString());
			}

		} catch (RepositoryException e) {
			log("Repository Exception");
			e.printStackTrace();
		}
		return propMap;
	}

	/*
	 * @see org.iavante.sling.commons.services.PortalSetup
	 */
	public boolean isWebDavMethod(String method) {

		return method.equals(WEBDAV_METHOD_PROPFIND)
				|| method.equals(WEBDAV_METHOD_PROPPATCH)
				|| method.equals(WEBDAV_METHOD_MKCOL)
				|| method.equals(WEBDAV_METHOD_COPY)
				|| method.equals(WEBDAV_METHOD_MOVE)
				|| method.equals(WEBDAV_METHOD_LOCK)
				|| method.equals(WEBDAV_METHOD_OPTIONS)
				|| method.equals(WEBDAV_METHOD_UNLOCK);
	}


	public void onEvent(EventIterator eventIterator) {

		Map<String, String> map = new HashMap<String, String>();
		Map<String, List<String>> restart = new HashMap<String, List<String>>();

		map = this.get_config_properties("/automatic_restart");
		Set set = map.entrySet();
		Iterator it = set.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String[] sns = entry.getValue().toString().split(",");
			restart.put(entry.getKey().toString(), Arrays.asList(sns));

		}

		while (eventIterator.hasNext()) {
			Event event = eventIterator.nextEvent();
			String propPath = "";
			try {
				propPath = event.getPath();
			} catch (RepositoryException e1) {
				e1.printStackTrace();
			}

			if (propPath.endsWith("sling:resourceType")) {
				// Extract Key Map from propPath
				String[] aux = propPath.split("/"); // /content/config/<subconfig>/...

				String key = aux[3];
				if (log.isInfoEnabled())
					log.info("onEvent, extracted key: " + key);

				// Get list of bundles to restart
				if (restart.containsKey(key)) {
					List<String> snames = restart.get(key);

					for (String symbolicName : snames) {
						if (log.isInfoEnabled())
							log.info("onEvent, trying to stop bundle: " + symbolicName);
						Bundle bundle = getBundle(symbolicName);
						try {
							bundle.stop();
							bundle.start();
						} catch (BundleException e) {
							e.printStackTrace();
						}
						if (log.isInfoEnabled())
							log.info("onEvent, Bundle: " + symbolicName + " started");
					}
				} else {
					if (log.isInfoEnabled())
						log.info("onEvent, no bundles related to key: " + key);
				}
			}
		}

	}

	// -------------------- Internal------------------------
	/**
	 * This method update the list of bundles installed
	 */
	private void updateBundlesIsntalled() {
		bundlesInstalled = myComponentContext.getBundleContext().getBundles();
	}

	/**
	 * Get a bundle given its symbolic name
	 * 
	 * @param symbolicName
	 * @return the bundle
	 */
	private Bundle getBundle(String symbolicName) {
		Bundle bundle = null;
		boolean seguir = true;
		int i = 0;
		while (i < bundlesInstalled.length && seguir) {
			if (bundlesInstalled[i].getSymbolicName().startsWith(symbolicName)) {
				bundle = bundlesInstalled[i];
				seguir = false;
			}
			i++;
		}
		return bundle;
	}

}
