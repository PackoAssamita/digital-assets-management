<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>GadUploaderForm</title>
    	<script type="text/javascript">
    		function send(){
				//alert('action: ' + document.getElementById('uploaderForm').action.value);
				//alert('content' + document.getElementById('uploaderForm').content.value);
        		
        	var server = document.getElementById('uploaderForm').server.value;
    			//alert(action);
    			document.getElementById('uploaderForm').setAttribute("action", server);
    			document.getElementById('uploaderForm').filename.value = document.getElementById('uploaderForm').data.value;
    			document.getElementById('uploaderForm').submit();
        }
            
    	</script>
    	
  </head>
  <body>
    <h1>Uploader de fuentes</h1>
    <form id="uploaderForm" name="uploaderForm" action="<%=request.getContextPath()%>/Controller" method="POST" enctype="multipart/form-data">
	    <table cellpadding="1" cellspacing="0" border="0" width="80%">
	    	<tr>
	    		<td width="20%">Fichero:</td>
	    		<td width="80%">
					<input type="file" id="data" name="data"/>
				</td>
	    	</tr>
	    	<tr>
	    		<td>URL Notificacion:</td>
	    		<td>
					<input type="text" id="content" name="content" value="/content/colecciones/coleccion_de_usuario/contents/contenido_test_1/sources/fuente_1"/>
				</td>
	    	</tr>
	    	<tr>
	    		<td>Server:</td>
	    		<td>
					<input type="text" id="server" name="server" value="http://localhost:8888/uploader"/>
					
				</td>
	    	</tr>
	    </table>
	    <input type="hidden" id="filename" name="filename" value="nombrePrueba"/>
	    <input type="submit" onclick="javascript:send()"/>
    </form>
  </body>
</html> 
