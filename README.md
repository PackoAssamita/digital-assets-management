Digital Assets Management
=========================

It's a management system that is focused on importing and indexing multimedia content.

It has the control over the life cycle of the asset: registration, revision and publishing.

In the registration process the content is uploaded to the platform and it will be indexed. The system will transcode the content to several preview format.

If the asset is reviewed and accepted, it will be passed to the common catalog.

Once in the common catalog, a publisher can publish the content through a publishing channel. The system will transcode the content to the default format of the channel.

Youtube, Red5, S3, blogs, etc are the publishing channels.

It is build using Apache Sling, and also it is using: Ffmpeg, Tika, ImageMagick, Felix, Lucene, Apache Jackrabbit and with  OSGi paradigm.

The system is divided into three components: the uploader,  transcoder and the core, they can be installed in the same machine or in several machines.

The uploader component manages the upload, registration and indexing of the assets. The transcoder manages the transcoding of the content and the core handles  the whole logic.


Links
-----

Home page: http://info.iavante.es/pages/viewpage.action?pageId=23298065

Apache Sling: http://sling.apache.org/

Authors
-------
* Francisco José Moreno Llorca | <packo@assamita.net> | https://github.com/kotejante
* Francisco Jesús González Mata | <chuspb@gmail.com> |  https://github.com/chuspb
* Juan Antonio Guzmán Hidalgo | <juan@guzmanhidalgo.com> | https://github.com/JuanObiJuan
* Daniel de la Cuesta Navarrete | <cues7a@gmail.com> | https://github.com/cues7a
* Manuel José Cobo Fernández | <ranrrias@gmail.com>

Third parties licenses
----------------------

Apache_2_0_LICENSE

Common Public License - v 1.0 

GPLv2

Base64Coder.java - Copyright 2003: Christian d'Heureuse, Inventec Informatik AG, Switzerland. LGPLS

License
-------

    Digital Assets Management
    
    Copyright 2009 Fundación Iavante
    
    Authors: 
      Francisco José Moreno Llorca <packo@assamita.net>
      Francisco Jesús González Mata <chuspb@gmail.com>
      Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
      Daniel de la Cuesta Navarrete <cues7a@gmail.com>
      Manuel José Cobo Fernández <ranrrias@gmail.com>
   
    Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
    the European Commission - subsequent versions of the EUPL (the "Licence");
    You may not use this work except in compliance with the Licence.
    You may obtain a copy of the Licence at:
   
    http://ec.europa.eu/idabc/eupl
   
    Unless required by applicable law or agreed to in writing, software 
    distributed under the Licence is distributed on an "AS IS" basis,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the Licence for the specific language governing permissions and 
    limitations under the Licence.
  
 
