/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import java.io.File;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.transcodingServer.modules.impl.Ffmpeg;
import org.iavante.sling.transcodingServer.*;

public class FfmpegTest extends RepositoryTestBase {
	
	Ffmpeg FfmpegInstance;
	SlingRepository repo;
	Session session;
	
	private Node content_node;
	private Node rootNode;
	
	private final String resources_path = "src/test/resources/";
	private final String SOURCE_VIDEO_FILE_NAME = "test.mp4";
	private final String FLV_VIDEO_FILE_NAME = "test.flv";
	private final String PDF_VIDEO_FILE_NAME = "test.pdf";
	private final String FLVPARAMS = "-f flv -b 512k -ar 44100";
	private final String WRONGPARAMS = "-f wrongcodec -b 512k -ar 44100";
	
	
	
	private File videoFile;
	

   
    protected void setUp() throws Exception {
    	//System.out.println("___________SetupTest");
    	super.setUp();
    	session = getSession();
    	
    	String[] prefixes = session.getWorkspace().getNamespaceRegistry().getPrefixes();
    	
    	boolean registered = false;
    	
    	for (int i=0; i< prefixes.length; i++) {
    		if (prefixes[i].equals("sling")) registered = true;
    		
    	}
    	
    	if (!registered)  session.getWorkspace().getNamespaceRegistry().registerNamespace("sling", "http://www.iavante.es/wiki/1.0");


        rootNode = getSession().getRootNode(); 
        
        // Add a jobNode
        content_node = rootNode.addNode("testjob" +  System.currentTimeMillis(), "nt:unstructured");

        
        rootNode.save();
        
	
		
        session.save();     
        
        FfmpegInstance = new Ffmpeg();
        //System.out.println("___________FfmpegInstance...");
    }	
    
	@Override	
    protected void tearDown() throws Exception {
        super.tearDown();        
    }
    
    public void test_workingFfmpegProcess() { 
    	System.out.println("________________test_workingFfmpegProcess...");
    	
    	videoFile = new File(resources_path + SOURCE_VIDEO_FILE_NAME);
      
    	if (videoFile.exists()){
    		try{
      	content_node.setProperty("source_location", videoFile.getAbsolutePath());
      	content_node.setProperty("temp_source_location", videoFile.getAbsolutePath());
      	content_node.setProperty("target_location", videoFile.getParent()+ FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("temp_target_location", videoFile.getParent()+ FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("executable", "ffmpeg");
      	content_node.setProperty("params", FLVPARAMS);
      	content_node.setProperty("extern_storage_server", "S3");
      	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
      	content_node.setProperty("tempDestination", videoFile.getParent());
      	content_node.setProperty("source_file", SOURCE_VIDEO_FILE_NAME);
      	content_node.setProperty("target_file", FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("notification_url", "www.google.es");
      	content_node.save();
      	
    		}
    		 catch( ArrayIndexOutOfBoundsException e ) {
    			 e.printStackTrace(); 
    		 } catch (ValueFormatException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (VersionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ConstraintViolationException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (RepositoryException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
      }
    	else{
    		
    		
    	}
      
      try {
  			Thread.sleep(1000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      
  		String response = FfmpegInstance.runProcess(content_node);
  		System.out.println("response:"+response);
  		assertTrue(response.toUpperCase().startsWith("OK"));
    	    	
    }
    
    public void test_wrongParamsFfmpegProcess() { 
    	System.out.println("test_wrongParamsFfmpegProcess...");
    	videoFile = new File(resources_path + SOURCE_VIDEO_FILE_NAME);
      
    	if (videoFile.exists()){
    		try{
      	content_node.setProperty("source_location", videoFile.getAbsolutePath());
      	content_node.setProperty("temp_source_location", videoFile.getAbsolutePath());
      	content_node.setProperty("target_location", resources_path + FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("executable", "ffmpeg");
      	content_node.setProperty("params", WRONGPARAMS);
      	content_node.setProperty("extern_storage_server", "S3");
      	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
      	content_node.setProperty("tempDestination", "/tmp");
      	content_node.setProperty("source_file", SOURCE_VIDEO_FILE_NAME);
      	content_node.setProperty("target_file", FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("notification_url", "www.google.es");
      	content_node.save();
      	
    		}
    		 catch( ArrayIndexOutOfBoundsException e ) {
    			 e.printStackTrace(); 
    		 } catch (ValueFormatException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (VersionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ConstraintViolationException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (RepositoryException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
      }
    	else{
    		
    		
    	}
      
      try {
  			Thread.sleep(1000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      
  		String response = FfmpegInstance.runProcess(content_node);
  		System.out.println("response:"+response);
  		assertTrue(!response.toUpperCase().startsWith("OK"));
    	    	
    }
    
    public void test_wrongInputPathFfmpegProcess() { 
    	System.out.println("_____________test_wrongInputFfmpegProcess...");
    	videoFile = new File(resources_path + SOURCE_VIDEO_FILE_NAME);
      
    	if (videoFile.exists()){
    		try{
      	content_node.setProperty("source_location", "/wrongdir"+videoFile.getAbsolutePath());
      	content_node.setProperty("temp_source_location", "/wrongdir/"+videoFile.getAbsolutePath());
      	content_node.setProperty("target_location", resources_path + PDF_VIDEO_FILE_NAME);
      	content_node.setProperty("executable", "ffmpeg");
      	content_node.setProperty("params", FLVPARAMS);
      	content_node.setProperty("extern_storage_server", "S3");
      	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
      	content_node.setProperty("tempDestination", "/tmp");
      	content_node.setProperty("source_file", SOURCE_VIDEO_FILE_NAME);
      	content_node.setProperty("target_file", FLV_VIDEO_FILE_NAME);
      	content_node.setProperty("notification_url", "www.google.es");
      	content_node.save();
      	
    		}
    		 catch( ArrayIndexOutOfBoundsException e ) {
    			 e.printStackTrace(); 
    		 } catch (ValueFormatException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (VersionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ConstraintViolationException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (RepositoryException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
      }
    	else{
    		
    		
    	}
      
      try {
  			Thread.sleep(1000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      
  		String response = FfmpegInstance.runProcess(content_node);
  		System.out.println("response:"+response);
  		assertTrue(!response.toUpperCase().startsWith("OK"));
    	    	
    }
    
    public void test_wrongInputFileTypeFfmpegProcess() { 
    	System.out.println("test_wrongInputFileTypeFfmpegProcess...");
    	videoFile = new File(resources_path + PDF_VIDEO_FILE_NAME);
      
    	if (videoFile.exists()){
    		try{
    			content_node.setProperty("source_location", videoFile.getAbsolutePath());
        	content_node.setProperty("temp_source_location", videoFile.getAbsolutePath());
        	content_node.setProperty("target_location", resources_path + FLV_VIDEO_FILE_NAME);
        	content_node.setProperty("executable", "ffmpeg");
        	content_node.setProperty("params", FLVPARAMS);
        	content_node.setProperty("extern_storage_server", "S3");
        	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
        	content_node.setProperty("tempDestination", "/tmp");
        	content_node.setProperty("source_file", SOURCE_VIDEO_FILE_NAME);
        	content_node.setProperty("target_file", FLV_VIDEO_FILE_NAME);
        	content_node.setProperty("notification_url", "www.google.es");
        	content_node.save();
        	
    		}
    		 catch( ArrayIndexOutOfBoundsException e ) {
    			 e.printStackTrace(); 
    		 } catch (ValueFormatException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (VersionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ConstraintViolationException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (RepositoryException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
      }
    	else{
    		
    		
    	}
      
      try {
  			Thread.sleep(1000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      
  		String response = FfmpegInstance.runProcess(content_node);
  		System.out.println("response:"+response);
  		assertTrue(!response.toUpperCase().startsWith("OK"));
    	
    }
    

   
   
    
}