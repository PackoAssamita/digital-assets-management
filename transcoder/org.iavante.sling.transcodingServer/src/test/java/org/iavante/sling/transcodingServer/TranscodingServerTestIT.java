/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test the transcodingServer bundle with a erroneous job.
 */
public class TranscodingServerTestIT
	extends TestCase {

	private final String JOBS_PENDING_URL = "/content/components/transcodingServer/jobs/pending/";
	private final String JOBS_DOING_URL = "/content/components/transcodingServer/jobs/doing/";
	private final String JOBS_DONE_URL = "/content/components/transcodingServer/jobs/done/";
	//private final String JOBS_ERROR_URL = "/content/components/transcodingServer/jobs/doing/";
	private final String JOBS_ERROR_URL = "/content/components/transcodingServer/jobs/error/";
	
	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);

	private final String HOSTVAR = "SLINGHOST";
	//private final String HOSTPREDEF = "localhost:8888";
	private final String HOSTPREDEF = "1.2.3.4:8080";
	private String SLING_URL = "http://";

	
	private final String title = "_test_transcoding_";
	private final String resourceType = "gad/job";
	private final String wrong_source_location = "/wrongdir/wrongfile.mp8";
	private final String wrong_target_location = "/wrongdir/wrongfile.mp9";
	private final String executable = "ffmpeg";
	private final String params = "-f flv -b 512k -ar 44100";
	private final String notification_url = "http://wrongurl.com";
	private final String extern_storage_url = "http://s3.amazonaws.com/bucket";
	private final String extern_storage_server = "S3";
	
	
	HttpClient client;

	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}

		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Create a wrong job
 
		PostMethod post_create_job = new PostMethod(SLING_URL + JOBS_PENDING_URL);

		post_create_job.setDoAuthentication(true);

		NameValuePair[] data_create_job = {
				new NameValuePair("sling:resourceType", resourceType),
				new NameValuePair("source_location", wrong_source_location),
				new NameValuePair("target_location", wrong_target_location),
				new NameValuePair("executable", executable),
				new NameValuePair("title", title),
				new NameValuePair("notification_url", notification_url),
				new NameValuePair("extern_storage_server", extern_storage_server),
				new NameValuePair("extern_storage_url", extern_storage_url),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_job.setRequestBody(data_create_job);

		try {
			client.executeMethod(post_create_job);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create_job.releaseConnection();

	}

	protected void tearDown() {

		// Delete the content
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PostMethod post_delete = new PostMethod(SLING_URL + JOBS_ERROR_URL + title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(post_delete.getStatusCode(), 200);
		// System.out.println("Response: " + post_delete.getStatusCode());
		post_delete.releaseConnection();
	}

	public void test_error_job() {

		try {
			Thread.sleep(6000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		//System.out.println("---------------------     test_error_job        ---------------------------------");
		HttpMethod get_job = new GetMethod(SLING_URL + JOBS_ERROR_URL + title+".xml");

		try {
			this.client.executeMethod(get_job);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String response_body = "";
		try {
			response_body = get_job.getResponseBodyAsString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println("-------------------------------------------------------");
		//System.out.println(response_body);
		//System.out.println("-------------------------------------------------------");
		assertTrue(response_body.contains("state=\"Error\""));
		get_job.releaseConnection();
	}

}