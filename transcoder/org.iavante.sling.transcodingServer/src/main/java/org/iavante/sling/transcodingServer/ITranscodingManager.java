/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import javax.jcr.Node;

/**
 * Service for Manage conversions jobs, uploads and notifications for the
 * processed files.
 * 
 * 
 * @scr.property name="service.description"
 *               value="IAVANTE - Transcoding Manager"
 * @scr.property name="service.vendor" value="IAVANTE"
 */
public interface ITranscodingManager {

	/**
	 * This method creates and controls dynamics instances for each type of Job.
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 * @return The response is a String, and returns something starting with
	 *         "Error. ..." if there's some problem or "OK." if there were no
	 *         problems.
	 */
	public String createJob(Node jobNode);

	/**
	 * This method notify to the Manager that this Job can be uploaded.
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 * @return The response is a String, and returns something starting with
	 *         "Error. ..." if there's some problem or "OK." if there were no
	 *         problems.
	 */
	public String readyForUpload(Node jobNode);

	/**
	 * This method notify to the Manager that this Job can notified.
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 * @return The response is a String, and returns something starting with
	 *         "Error. ..." if there's some problem or "OK." if there were no
	 *         problems.
	 */
	public String readyForNotification(Node jobNode);

	/**
	 * This method notify to the Manager can take the first job in the list.
	 * 
	 */
	public void startFirstPendingJob();

	/**
	 * This method notify to the Manager that there's a new node in Error folder
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 */
	public void notifyErrorJob(Node jobNode);

	/**
	 * This method notify to the Manager that there's a new node in Done folder
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 */
	public void notifyDoneJob(Node jobNode);

	/**
	 * This method notify to the Manager that there's a new node in Pending folder
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 */
	public void notifyPendingJob(Node addedNode);

	/**
	 * This method notify to the Manager that there's a new node in Doing folder
	 * 
	 * @param jobNode
	 *          The Node that contains the job specs in the JCR
	 * 
	 */
	public void notifyDoingJob(Node addedNode);

}