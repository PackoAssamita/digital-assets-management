/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.impl;

import java.io.Serializable;
import java.util.Calendar;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.JcrTools;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.transcodingServer.CalendarTools;
import org.iavante.sling.transcodingServer.ITranscodingManager;
import org.iavante.sling.transcodingServer.ITranscodingServer;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This service is always Listening the "Pending" node. When a Job goes to
 * "Pending" is notified to transcodingManager
 * 
 * @scr.service
 * 
 * @scr.component immediate="true" metatype="false"
 * 
 * @scr.property name="service.description" value="Pending Jobs listener"
 * 
 * @scr.property name="service.vendor" value="IAVANTE"
 * 
 */
public class PendingTranscodingServiceImpl implements ITranscodingServer,
		EventListener, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Session session;

	private ObservationManager observationManager;
	/** @scr.reference */
	private SlingRepository repository;
	/** @scr.reference */
	private ITranscodingManager transcodingManager;
	/**
	 * Registered jcr path
	 */
	private final String REGISTERED_PATH = "/content/components/transcodingServer/jobs/pending";
	/**
	 * Content sling:resourceTypes
	 * 
	 */
	private final String JOB_RESOURCE_TYPE = "gad/job";

	private static final Logger log = LoggerFactory
			.getLogger(PendingTranscodingServiceImpl.class);

	/**
	 * Log node
	 */
	void log(String text) {
		log.info(text);
	}

	protected void activate(ComponentContext context) {
		try {
			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED,
						REGISTERED_PATH, true, null, types, false);
				log.info("Registered Path:" + REGISTERED_PATH);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	public void onEvent(EventIterator eventIterator) {

		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));
					log("Node added: " + addedNode.getPath().toString());
					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getValue()
								.getString().compareToIgnoreCase(JOB_RESOURCE_TYPE) == 0) {
							log.info("New job in :" + addedNode.getPath().toString());
							addedNode.setProperty("duration", "");
							addedNode.setProperty("video", "");
							addedNode.setProperty("bitrate", "");
							addedNode.setProperty("mime", "");
							addedNode.setProperty("log", "");
							addedNode.setProperty("state", "");

							String dateString = "";
							String dateTimeStamp = "";

							dateString = CalendarTools.getDateFormated();

							addedNode.setProperty("date", dateString);

							addedNode.save();
							try {
								Thread.sleep(400);
							} catch (InterruptedException e1) {

								e1.printStackTrace();
							}
							transcodingManager.notifyPendingJob(addedNode);

						} else
							log.error("sling:resourceType is not valid");

					} else
						log.error("the post has missing sling:resourceType parameter");
				} catch (RepositoryException e) {
					e.printStackTrace();

					log.error(e.toString());
				}
			}
		}

	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

}