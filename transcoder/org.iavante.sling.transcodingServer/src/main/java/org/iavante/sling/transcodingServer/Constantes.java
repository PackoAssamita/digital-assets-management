/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

public class Constantes {

	private static String pathFFMPEG = null;
	private static String pathFile = null;
	private static String sourceDir = null;
	private static String workingDir = null;
	private static String outputDir = null;
	private static String jobsPath = null;
	private static String GAD_user = null;
	private static String GAD_password = null;
	private static String threadsTotal = null;
	private static String threadsH264 = null;
	private static String threadsOthers = null;
	private static String pathImageMagick = null;

	public Constantes() {
	}

	public static String getpathFFMPEG() {
		return pathFFMPEG;
	}

	public static String getpathImageMagick() {
		return pathImageMagick;
	}

	public static void setpathFFMPEG(String string) {
		pathFFMPEG = string;
	}

	public static void setpathImageMagick(String string) {
		pathImageMagick = string;
	}

	public static String getpathFile() {
		return pathFile;
	}

	public static void setpathFile(String string) {
		pathFile = string;
	}

	public static String getsourceDir() {
		return sourceDir;
	}

	public static void setsourceDir(String string) {
		sourceDir = string;
	}

	public static String getworkingDir() {
		return workingDir;
	}

	public static void setworkingDir(String string) {
		workingDir = string;
	}

	public static String getjobsPathr() {
		return jobsPath;
	}

	public static void setjobsPath(String string) {
		jobsPath = string;
	}

	public static String getoutputDir() {
		return outputDir;
	}

	public static void setoutputDir(String string) {
		outputDir = string;
	}

	public static String getGADuser() {
		return GAD_user;
	}

	public static void setGADuser(String string) {
		GAD_user = string;
	}

	public static String getGADpassword() {
		return GAD_password;
	}

	public static void setGAD_password(String string) {
		GAD_password = string;
	}

	public static String getthreadsTotal() {
		return threadsTotal;
	}

	public static void setthreadsTotal(String string) {
		threadsTotal = string;
	}

	public static String getthreadsH264() {
		return threadsH264;
	}

	public static void setthreadsH264(String string) {
		threadsH264 = string;
	}

	public static String getthreadsOthers() {
		return threadsOthers;
	}

	public static void setthreadsOthers(String string) {
		threadsOthers = string;
	}

}
