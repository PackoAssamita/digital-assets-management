/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.modules.impl;

import javax.jcr.Node;

import org.iavante.sling.transcodingServer.modules.gadJobInterface;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mencoder implements gadJobInterface {
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory.getLogger(Mencoder.class);

	protected void activate(ComponentContext context) {
		log.info("Activated Mencoder Class");
	}

	private void initialize() {

	}

	public String extractMetadata(Node jobNode) {
		return null;
	}

	public String runProcess(Node jobNode) {
		// Run Process
		return null;
	}

	private String checkFiles(Node JobNode) {
		// Check if all files and folders exists
		return "";
	}

	public String startJob(Node jobNode) {
		// Main method
		return "Error.";
	}

}