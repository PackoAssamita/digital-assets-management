/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.modules.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.transcodingServer.Constantes;
import org.iavante.sling.transcodingServer.CopyFile;
import org.iavante.sling.transcodingServer.Proceso;
import org.iavante.sling.transcodingServer.modules.gadJobInterface;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class is called when the param "noconversion" is filled in the
 * executable field. Normally is because this job require no video process and
 * only it's needed the upload to the Distribution server indicated.
 * 
 */
public class Convert implements gadJobInterface {
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory.getLogger(Convert.class);

	private static final String call_in_test_mode = "convert ";

	private final String tempdir_in_test_mode = "src/test/resources/";

	private final String resources_path = "src/test/resources/";

	private final String JPG_IMAGE_FILE_NAME = "test.jpg";

	private File myfile;

	protected void activate(ComponentContext context) {
		log.info("Activated Convert Class");
		myfile = new File(resources_path + JPG_IMAGE_FILE_NAME);
	}

	private void initialize() {

	}

	private String extractOutputMetadata(Node jobNode) {

		// Metadata Extract
		String outputMimeType = "";

		String nombre = null;

		String temp_target_location = null;
		try {

			temp_target_location = jobNode.getProperty("temp_target_location")
					.getValue().getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i "
				+ temp_target_location;
		Map responseMime = p0.start(orden0);

		Iterator it = responseMime.entrySet().iterator();

		try {
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String mimeLine = e.getValue().toString();
				if (mimeLine.contains(": ")) {
					if (mimeLine.indexOf(": ") < mimeLine.indexOf(";")) {
						outputMimeType = mimeLine.substring(mimeLine.indexOf(": "),
								mimeLine.indexOf(";"));
					}
				}
			}
			jobNode.setProperty("outputMimeType", outputMimeType);
			jobNode.save();
		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";
		} catch (ValueFormatException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (VersionException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (LockException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (RepositoryException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		}
		log.info("outputMimeType is " + outputMimeType);
		return "OK. MimeType & Metadata extracted.";
	}

	private String extractInputMetadata(Node jobNode) {

		// Metadata Extract
		String inputMimeType = "";

		String nombre = null;

		String temp_source_location = null;
		try {

			temp_source_location = jobNode.getProperty("temp_source_location")
					.getValue().getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i "
				+ temp_source_location;
		Map responseMime = p0.start(orden0);

		// The output format is something like myFileName.mp4: video/mp4;
		// charset=binary
		// we're looking for ("inputMime")
		Iterator it = responseMime.entrySet().iterator();

		try {
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String mimeLine = e.getValue().toString();
				if (mimeLine.contains(": ")) {
					if (mimeLine.indexOf(": ") < mimeLine.indexOf(";")) {
						inputMimeType = mimeLine.substring(mimeLine.indexOf(": "), mimeLine
								.indexOf(";"));
					}
				}
			}
			jobNode.setProperty("inputMimeType", inputMimeType);
			jobNode.save();
		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";
		} catch (ValueFormatException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (VersionException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (LockException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		} catch (RepositoryException e) {

			e.printStackTrace();
			return "OK. But some Metadata not found.";
		}
		log.info("outputMimeType is " + inputMimeType);
		return "OK. MimeType & Metadata extracted.";
	}

	public String runProcess(Node jobNode) {
		@SuppressWarnings("unused")
		String response = "";
		@SuppressWarnings("unused")
		Boolean error = false;
		@SuppressWarnings("unused")
		String my_source_location = null;
		@SuppressWarnings("unused")
		String my_target_location;
		@SuppressWarnings("unused")
		String my_executable = null;
		@SuppressWarnings("unused")
		String my_params = "";
		@SuppressWarnings("unused")
		String my_notification_url;
		@SuppressWarnings("unused")
		String my_extern_storage_url;
		@SuppressWarnings("unused")
		String my_extern_storage_server;
		@SuppressWarnings("unused")
		String my_source_file;
		@SuppressWarnings("unused")
		String my_target_file = null;
		@SuppressWarnings("unused")
		String my_temp_source_location = null;

		try {
			log.info("Convert:runProcess:getting properties");
			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();
			my_executable = jobNode.getProperty("executable").getValue().getString();
			if (jobNode.hasProperty("params")) {
				my_params = jobNode.getProperty("params").getValue().getString();
			}
			my_notification_url = jobNode.getProperty("notification_url").getValue()
					.getString();
			my_extern_storage_server = jobNode.getProperty("extern_storage_server")
					.getValue().getString();
			my_extern_storage_url = jobNode.getProperty("extern_storage_url")
					.getValue().getString();
			my_source_file = jobNode.getProperty("source_file").getValue()
					.getString();
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();
			my_temp_source_location = jobNode.getProperty("temp_source_location")
					.getValue().getString();

		} catch (ValueFormatException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. ValueFormatException" + e1;
			log.info("Error:" + response);
		} catch (IllegalStateException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. IllegalStateException" + e1;
			log.info("Error:" + response);
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException" + e1;
			log.info("Error:" + response);
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. RepositoryException" + e1;
			log.info("Error:" + response);
		}

		if (!error) {

			List ErrorStrings = new ArrayList();
			ErrorStrings.add("no such file");
			ErrorStrings.add("unrecognized option");
			ErrorStrings.add("unable to open image");
			ErrorStrings.add("java.io.IOException");
			ErrorStrings.add("no decode delegate");

			Proceso p0 = new Proceso();
			String orden0 = "";
			if (!(Constantes.getpathImageMagick() == null)) {
				orden0 = Constantes.getpathImageMagick() + "/convert "
						+ my_source_location + " " + my_params + " "
						+ Constantes.getworkingDir() + "/" + my_target_file;
			} else {
				// only in test mode
				log.info("Executing in Test Mode: " + orden0);
				String tempPath = my_temp_source_location.substring(0,
						my_temp_source_location.lastIndexOf("/"));
				orden0 = tempPath + "/" + call_in_test_mode + my_temp_source_location
						+ " " + my_params + " " + tempPath + "/" + my_target_file;

			}
			log.info("Executing : '" + orden0);
			Map aux0 = p0.start(orden0);

			Iterator itr = aux0.entrySet().iterator();

			while (itr.hasNext()) {

				Map.Entry e = (Map.Entry) itr.next();
				Iterator errorsIt = ErrorStrings.iterator();
				while (errorsIt.hasNext()) {
					String thisError = (String) errorsIt.next();
					if (e.getValue().toString().toLowerCase().contains(
							thisError.toLowerCase())) {
						response = "Error in Convert process. " + e.getValue().toString();
						error = true;
						log.info("Se ha detectado un error en la llamada:"
								+ e.getValue().toString());
						return response;
					}
				}

			}

		}

		// We've to check that the outputfile exists

		if (!error) {
			String salida = "";

			if ((Constantes.getworkingDir() == null)) {
				// modo test
				log.info("Modo test activo");
				salida = tempdir_in_test_mode;
			} else {
				salida = Constantes.getworkingDir();

			}
			salida = salida + "/" + my_target_file;

			File my_output_file = new File(salida);

			if (my_output_file.exists()) {
				if (my_output_file.length() > 1) {
					response = "OK. File converted.";
					try {
						log.info("saving temp_target_location");
						jobNode.setProperty("temp_target_location", salida);
						jobNode.save();
						response = "OK. File converted.";
						return response;
					} catch (ValueFormatException e) {

						e.printStackTrace();
					} catch (VersionException e) {

						e.printStackTrace();
					} catch (LockException e) {

						e.printStackTrace();
					} catch (ConstraintViolationException e) {

						e.printStackTrace();
					} catch (RepositoryException e) {

						e.printStackTrace();
					}

				} else {
					response = "1 Error converting the file. Check Convert codecs and configuration parameters.";
					error = true;
					return response;
				}

			} else {
				response = "2 Error converting the file. Check Convert configuration parameters.";
				error = true;
				return response;
			}

		}

		return response;

	}

	public String runCustomProcess(Node jobNode) {
		@SuppressWarnings("unused")
		String response = "OK. runCustom OK.";
		@SuppressWarnings("unused")
		Boolean error = false;
		String env = setEnvironmentVars(jobNode);
		try {
			Thread.sleep(300);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		@SuppressWarnings("unused")
		String my_source_location;
		@SuppressWarnings("unused")
		String target_location = null;
		@SuppressWarnings("unused")
		String temp_target_location = null;
		@SuppressWarnings("unused")
		String my_executable = null;
		@SuppressWarnings("unused")
		String my_params = "";
		@SuppressWarnings("unused")
		String my_notification_url;
		@SuppressWarnings("unused")
		String my_extern_storage_url;
		@SuppressWarnings("unused")
		String my_extern_storage_server;
		@SuppressWarnings("unused")
		String my_tempDestination = null;
		@SuppressWarnings("unused")
		String my_source_file;
		@SuppressWarnings("unused")
		String my_target_file = null;

		try {
			log.info("CustomConvert:runProcess:getting properties");

			if (jobNode.hasProperty("params")) {
				my_params = jobNode.getProperty("params").getValue().getString();
				my_params = my_params.substring("custom ".length());
			}
			if (jobNode.hasProperty("notification_url")) {
				my_notification_url = jobNode.getProperty("notification_url")
						.getValue().getString();
			}
			if (jobNode.hasProperty("extern_storage_server")) {
				my_extern_storage_server = jobNode.getProperty("extern_storage_server")
						.getValue().getString();
			}
			if (jobNode.hasProperty("extern_storage_server")) {
				my_extern_storage_url = jobNode.getProperty("extern_storage_url")
						.getValue().getString();
			}
			if (jobNode.hasProperty("target_location")) {
				target_location = jobNode.getProperty("target_location").getValue()
						.getString();
			}
			if (jobNode.hasProperty("temp_target_location")) {
				temp_target_location = jobNode.getProperty("temp_target_location")
						.getValue().getString();
			}

		} catch (ValueFormatException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. ValueFormatException" + e1;
			log.info("Error:" + response);
		} catch (IllegalStateException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. IllegalStateException" + e1;
			log.info("Error:" + response);
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException" + e1;
			log.info("Error:" + response);
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. RepositoryException" + e1;
			log.info("Error:" + response);
		}

		if (!error) {
			List ErrorStrings = new ArrayList();
			ErrorStrings.add("no such file");
			ErrorStrings.add("unrecognized option");
			ErrorStrings.add("unable to open image");
			ErrorStrings.add("java.io.IOException");
			ErrorStrings.add("no decode delegate");

			Proceso p0 = new Proceso();
			String orden0 = Constantes.getpathImageMagick() + my_params;
			log.info("Ejecutando orden: '" + orden0);
			Map aux0 = p0.start(orden0);
			Iterator itr = aux0.entrySet().iterator();

			while (itr.hasNext()) {

				Map.Entry e = (Map.Entry) itr.next();
				Iterator errorsIt = ErrorStrings.iterator();
				while (errorsIt.hasNext()) {
					String thisError = (String) errorsIt.next();
					if (e.getValue().toString().toLowerCase().contains(
							thisError.toLowerCase())) {
						response = "Error in Convert process. " + e.getValue().toString();
						error = true;
						log.info("Error detected in calling:" + e.getValue().toString());
						return response;
					}
				}

			}

		}

		// copy the output in the temp to upload to S3 later
		try {
			CopyFile.copy(target_location, temp_target_location);
		} catch (IOException e) {

			log.info("Error making a copy of custom Process");
			e.printStackTrace();
			return "Error.";
		}

		return response;

	}

	private String checkFiles(Node JobNode) {

		log.info("ChekingFiles...");
		String response = "";
		Boolean error = false;
		Node myNode = null;
		String my_source_location = null;
		String my_target_location = null;
		myNode = JobNode;

		try {
			my_source_location = myNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_source_location = "";
			e1.printStackTrace();
		} catch (RepositoryException e1) {

			e1.printStackTrace();
		}

		try {
			my_target_location = myNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_target_location = "";
			e1.printStackTrace();
		} catch (RepositoryException e1) {

			e1.printStackTrace();
		}

		String source_file = "";
		String tempDestination = "";
		String source_folder = null;
		String target_folder = null;

		source_folder = (String) my_source_location.subSequence(0,
				my_source_location.lastIndexOf("/"));

		target_folder = (String) my_target_location.subSequence(0,
				my_target_location.lastIndexOf("/"));

		File my_source_folder = new File(source_folder);
		File my_source_file = new File(my_source_location.toString());
		File my_target_folder = new File(target_folder);

		if (!my_source_folder.isDirectory()) {
			error = true;
			response = "Error. Source Folder doesn't exist (" + source_folder + ")";
			log.info("Error:" + response);

		} else if (!my_source_file.exists()) {
			error = true;
			response = "Error. File doesn't exist (" + my_source_location + ")";
			log.info("Error:" + response);
		} else if (!my_target_folder.isDirectory()) {
			error = true;
			response = "Error. Target Folder doesn't exist (" + target_folder + ")";
			log.info("Error:" + response);
		}
		;

		if (!error) {
			try {

				myNode.setProperty("source_file", my_source_file.getName());
				myNode.setProperty("source_folder", source_folder);
				myNode.setProperty("target_folder", target_folder);
				myNode.save();
				log.info("CheckingFiles:OK");
				response = "OK";
			} catch (ValueFormatException e) {

				e.printStackTrace();
			} catch (VersionException e) {

				e.printStackTrace();
			} catch (LockException e) {

				e.printStackTrace();
			} catch (ConstraintViolationException e) {

				e.printStackTrace();
			} catch (RepositoryException e) {

				e.printStackTrace();
			}

		}

		return response;

	}

	private String setEnvironmentVars(Node jobNode) {

		Boolean error = false;
		String thisresponse = "OK.";
		String my_source_location = null;
		String my_target_location = null;
		String source_file = null;
		String target_file = null;
		String temp_source_location = null;
		String temp_target_location = null;
		String temp_location = null;
		try {
			if (jobNode.hasProperty("source_location")) {
				my_source_location = jobNode.getProperty("source_location").getValue()
						.getString();
			}

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property source_location property not accesible.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		}

		try {

			if (jobNode.hasProperty("target_location")) {
				my_target_location = jobNode.getProperty("target_location").getValue()
						.getString();
			}
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property target_location property not accesible.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		}

		if (error) {
			thisresponse = "Error. Checking the job files and Folders";
			log.info("Error:" + thisresponse);
		} else {
			log.info("my_source_location" + my_source_location);
			log.info("my_target_location" + my_target_location);

			source_file = my_source_location.substring(my_source_location
					.lastIndexOf("/") + 1, my_source_location.length());
			target_file = my_target_location.substring(my_target_location
					.lastIndexOf("/") + 1, my_target_location.length());
			temp_location = Constantes.getworkingDir();

			if (!temp_location.endsWith("/")) {
				temp_location = temp_location + "/";
			}

			temp_source_location = temp_location + source_file;

			temp_target_location = temp_location + target_file;

		}

		try {
			// in convert we're not going to use the source in temp folder
			// we're using the source folder
			jobNode.setProperty("temp_source_location", my_source_location);
			jobNode.setProperty("temp_target_location", temp_target_location);
			jobNode.setProperty("source_file", source_file);
			jobNode.setProperty("target_file", target_file);
			jobNode.save();
			log.info("info saved");
		} catch (ValueFormatException e) {

			e.printStackTrace();
			log.info("check3");
			return "Error saving properties.";
		} catch (VersionException e) {

			e.printStackTrace();
			log.info("check4");
			return "Error saving properties.";
		} catch (LockException e) {

			e.printStackTrace();
			log.info("check5");
			return "Error saving properties.";
		} catch (ConstraintViolationException e) {

			log.info("check6");
			e.printStackTrace();
			return "Error saving properties.";
		} catch (RepositoryException e) {

			e.printStackTrace();
			log.info("check7");
			return "Error saving properties.";
		}
		return thisresponse;

	}

	private String copyFiletoWorkingFolder(Node jobNode) {
		log.info("copyFiletoWorkingFolder");
		Boolean error = false;
		String thisresponse = "";
		String my_source_location = null;
		String my_target_location = null;
		String source_file = null;
		String target_file = null;
		String tempDestination = null;

		try {

			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property source_location property not accesible.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		}

		try {
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property target_location property not accesible.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
			return thisresponse;
		}

		if (error) {
			thisresponse = "Error. Checking the job files and Folders";
			log.info("Error:" + thisresponse);
		} else {
			source_file = (String) my_source_location.subSequence(my_source_location
					.lastIndexOf("/") + 1, my_source_location.length());
			target_file = (String) my_target_location.subSequence(my_target_location
					.lastIndexOf("/") + 1, my_target_location.length());
			tempDestination = Constantes.getworkingDir();

			if (!tempDestination.endsWith("/")) {
				tempDestination = tempDestination + "/";
			}

			tempDestination = tempDestination + source_file;

			try {

				CopyFile.copy(my_source_location, tempDestination);

				thisresponse = "OK. Copied to WorkingFolder";
				log.info("Copying..." + thisresponse);
			} catch (IOException e) {
				error = true;
				thisresponse = "Error copying the file to workingFolder."
						+ e.getMessage();
				log.info("Error:" + thisresponse);
				e.printStackTrace();
				return thisresponse;
			}

		}

		try {
			jobNode.setProperty("temp_source_location", tempDestination);
			jobNode.setProperty("temp_target_location", tempDestination);
			jobNode.setProperty("source_file", source_file);
			jobNode.setProperty("target_file", target_file);
			jobNode.save();
		} catch (ValueFormatException e) {

			e.printStackTrace();
			return "Error saving properties.";
		} catch (VersionException e) {

			e.printStackTrace();
			return "Error saving properties.";
		} catch (LockException e) {

			e.printStackTrace();
			return "Error saving properties.";
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
			return "Error saving properties.";
		} catch (RepositoryException e) {

			e.printStackTrace();
			return "Error saving properties.";
		}
		return thisresponse;

	}

	private String copyFiletoTargetFolder(Node jobNode) {

		String response = "";
		boolean error = false;
		String my_target_file = null;
		String my_target_fullpath = null;
		String my_output_fullpath = null;

		try {
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();
			my_target_fullpath = Constantes.getworkingDir() + "/" + my_target_file;
			my_output_fullpath = Constantes.getoutputDir() + "/" + my_target_file;
			jobNode.setProperty("output_fullpath", my_output_fullpath);
			jobNode.save();
			CopyFile.copy(my_target_fullpath, my_output_fullpath);

			response = "OK. Copied to OutPutFolder";
			log.info(response);
		} catch (ValueFormatException e) {

			e.printStackTrace();
			error = true;
			response = "Error. ValueFormatException.";
			log.info(response);
		} catch (IllegalStateException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IllegalStateException.";
			log.info(response);
		} catch (PathNotFoundException e) {

			e.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException.";
			log.info(response);
		} catch (RepositoryException e) {

			e.printStackTrace();
			error = true;
			response = "Error. RepositoryException.";
			log.info(response);
		} catch (IOException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IOException.";
			log.info(response);
		}
		return response;
	}

	public String startJob(Node jobNode) {

		log.info("Convert:...starting the Job.");
		initialize();
		Boolean error = false;
		String the_params = "";

		String response_checking = "";
		String response_set = "";
		String response_copy_out = "";
		String response_metadata = "";
		String response_process = "";
		String response = "";

		try {
			the_params = jobNode.getProperty("params").getValue().getString();

		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (IllegalStateException e) {

			e.printStackTrace();
		} catch (PathNotFoundException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		if (the_params.toLowerCase().startsWith("custom")) {
			// customized convert
			response = runCustomProcess(jobNode);
			return response;

		}

		else {
			// normal process

			log.info("1 Convert: checkingFiles");
			response_checking = checkFiles(jobNode);

			if (response_checking.startsWith("OK")) {
				// copyFiletoWorkingFolder
				log.info("1 checkingFiles:OK");
				log.info("2 settingEnvironmentVars");
				response_set = setEnvironmentVars(jobNode);
			} else {
				// if response_cheking is error
				error = true;
				response = response_checking;
				log.info("1 checkingFiles:Error");
			}
			if ((!error) && (response_set.startsWith("OK"))) {
				// extractMetadata
				log.info("2 settingEnvoronmentVars:OK");
				log.info("3 extractingMetadata");
				response_metadata = extractInputMetadata(jobNode);

			} else {
				// if response_set is error or there is another error before
				error = true;
				response = response_set;
				log.info("2 settingenvoronmentFiles:Error");
			}

			if ((!error) && (response_metadata.startsWith("OK"))) {
				// runProcess
				log.info("3 extractingMetadata:OK");
				log.info("4 runningProcess");
				response_process = runProcess(jobNode);
			} else {
				// if response_metadata is error
				error = true;
				response = response_metadata;
				log.info("3 extractingMetadata:Error");

			}
			if ((!error) && (response_process.startsWith("OK"))) {
				// externalStorage
				log.info("4 runningProcess:OK");
				log.info("5 copyToOutputFolder");
				// ew've to copy the file to outpoutfolder
				extractOutputMetadata(jobNode);
				response_copy_out = copyFiletoTargetFolder(jobNode);

			} else {
				// if response_process is error
				error = true;
				response = response_process;
				log.info("4 runningProcess:Error");
			}

			if ((!error) && (response_copy_out.startsWith("OK"))) {
				// all OK
				log.info("5 copyToOutputFolder:OK");
			} else {
				// if error en response_copy_out
				error = true;
				response = "Error.";
				log.info("5 copyToOutputFolder:Error");
			}
		}
		// end of else (normal process)

		if (!error) {
			response = "OK.";
			log.info("Convert: everything was OK");

		}

		return response;
	}

}