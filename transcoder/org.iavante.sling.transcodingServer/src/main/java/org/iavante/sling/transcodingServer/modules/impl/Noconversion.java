/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.modules.impl;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.transcodingServer.Constantes;
import org.iavante.sling.transcodingServer.CopyFile;
import org.iavante.sling.transcodingServer.Proceso;
import org.iavante.sling.transcodingServer.modules.gadJobInterface;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class is called when the param "noconversion" is filled in the
 * executable field. Normally is because this job require no video process and
 * only it's needed the upload to the Distribution server indicated.
 * 
 */
public class Noconversion implements gadJobInterface {
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory.getLogger(Noconversion.class);

	private Session session;
	/** @scr.reference */
	private SlingRepository repository;

	protected void activate(ComponentContext context) {
		log.info("Noconversion Dynamic instance Activated");
	}

	private String extractInputMetadata(Node jobNode) {
		// Metadata Extract
		String inputMimeType = "";
		String inputDuration = "";
		String inputBitrate = "";
		String inputVideo = "";
		String inputAudio = "";
		String nombre = null;
		try {
			nombre = jobNode.getProperty("source_file").getValue().getString();
			log.info("Extrackting Metadata of " + nombre);
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		String temp_source_location = null;
		try {
			temp_source_location = jobNode.getProperty("temp_source_location")
					.getValue().getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i "
				+ temp_source_location;
		Map responseMime = p0.start(orden0);

		
		Iterator it = responseMime.entrySet().iterator();

		try {
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String mimeLine = e.getValue().toString();
				if (mimeLine.contains(": ")) {
					inputMimeType = mimeLine.substring(mimeLine.indexOf(": "), mimeLine
							.indexOf(";"));
					jobNode.setProperty("inputMimeType", inputMimeType);
					jobNode.setProperty("outputMimeType", inputMimeType);
					jobNode.save();
				}

			}

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";

		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		String DurationLabelBegin = "Duration: ";
		String DurationLabelEnd = ", ";
		String VideoLabelBegin = "Video: ";
		String VideoLabelEnd = ", ";
		String AudioLabelBegin = "Audio: ";
		String AudioLabelEnd = ", ";
		String BitrateLabelBegin = "bitrate: ";
		String BitrateLabelEnd = "s";

		Proceso p1 = new Proceso();
		// call to make
		String orden = Constantes.getpathFFMPEG() + "/ffmpeg -i "
				+ temp_source_location;
		Map responseMeta = p1.start(orden);
		Iterator it2 = responseMeta.entrySet().iterator();

		try {
			while (it2.hasNext()) {
				Map.Entry myentry = (Map.Entry) it2.next();
				String metaLine = myentry.getValue().toString();
				if (metaLine.contains(DurationLabelBegin)) {
					log.info("contains " + DurationLabelBegin);
					inputDuration = metaLine
							.substring(metaLine.indexOf(DurationLabelBegin)
									+ DurationLabelBegin.length(), metaLine
									.indexOf(DurationLabelEnd));
				}
				if (metaLine.contains(BitrateLabelBegin)) {
					log.info("contains " + BitrateLabelBegin);
					inputBitrate = metaLine.substring(metaLine.indexOf(BitrateLabelBegin)
							+ BitrateLabelBegin.length(), metaLine
							.lastIndexOf(BitrateLabelEnd) + 1);
				}
				if (metaLine.contains(VideoLabelBegin)) {
					log.info("contains " + VideoLabelBegin);
					inputVideo = metaLine.substring(metaLine.indexOf(VideoLabelBegin)
							+ VideoLabelBegin.length(), metaLine.indexOf(VideoLabelEnd));
				}
				if (metaLine.contains(AudioLabelBegin)) {
					log.info("contains " + AudioLabelBegin);
					inputAudio = metaLine.substring(metaLine.indexOf(AudioLabelBegin)
							+ AudioLabelBegin.length(), metaLine.indexOf(AudioLabelEnd));
				}

			}
			jobNode.setProperty("inputDuration", inputDuration);
			jobNode.setProperty("inputBitrate", inputBitrate);
			jobNode.setProperty("inputVideo", inputVideo);
			jobNode.setProperty("inputAudio", inputAudio);
			jobNode.setProperty("outputDuration", inputDuration);
			jobNode.setProperty("outputBitrate", inputBitrate);
			jobNode.setProperty("outputVideo", inputVideo);
			jobNode.setProperty("outputAudio", inputAudio);

			jobNode.save();

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";
		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		log.info("inPutMimeType is " + inputMimeType);
		log.info("inputDuration is " + inputDuration);
		log.info("inputBitrate is " + inputBitrate);
		log.info("inputVideo is " + inputVideo);
		log.info("inputAudio is " + inputAudio);

		return "OK. MimeType & Metadata extracted.";
	}

	private void initialize() {
	}

	private String copyFiletoWorkingFolder(Node jobNode) {
		log.info("copyFiletoWorkingFolder");
		Boolean error = false;
		String thisresponse = "";
		String my_source_location = null;
		String my_target_location = null;
		String source_file = null;
		String target_file = null;
		String tempDestination = null;

		try {

			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property source_location property not accesible.";
			log.info("Error:" + thisresponse);
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
		}

		try {
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property target_location property not accesible.";
			log.info("Error:" + thisresponse);
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("Error:" + thisresponse);
		}

		if (error) {
			thisresponse = "Error. Checking the job files and Folders";
			log.info("Error:" + thisresponse);
		} else {
			source_file = (String) my_source_location.subSequence(my_source_location
					.lastIndexOf("/") + 1, my_source_location.length());
			target_file = (String) my_target_location.subSequence(my_target_location
					.lastIndexOf("/") + 1, my_target_location.length());
			tempDestination = Constantes.getworkingDir();

			if (!tempDestination.endsWith("/")) {
				tempDestination = tempDestination + "/";
			}

			tempDestination = tempDestination + source_file;

			try {

				CopyFile.copy(my_source_location, tempDestination);

				thisresponse = "OK. Copied to WorkingFolder";
				log.info("Copying..." + thisresponse);
			} catch (IOException e) {
				error = true;
				thisresponse = "Error copying the file to workingFolder."
						+ e.getMessage();
				log.info("Error:" + thisresponse);
				e.printStackTrace();
			}

		}

		try {

			jobNode.setProperty("temp_target_location", tempDestination);
			jobNode.setProperty("temp_source_location", tempDestination);
			jobNode.setProperty("source_file", source_file);
			jobNode.setProperty("target_file", target_file);
			jobNode.save();
		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}
		return thisresponse;

	}

	public String startJob(Node jobNode) {

		// We've to create the porperty output_fullpath in the node
		// in this case is exactly the same as inputFile
		// It's needed too create the "source_file" porperty for notification

		String thisResponse = "Error.";

		try {

			thisResponse = copyFiletoWorkingFolder(jobNode);

			log.info("reading properties...OK!");

		} catch (IllegalStateException e) {

			thisResponse = "Error. IllegalStateException";
			e.printStackTrace();
			log.info("Error:" + thisResponse);
			return thisResponse;
		}
		thisResponse = extractInputMetadata(jobNode);
		log.info("Extracted Metadata. OK");
		return thisResponse;
	}

}