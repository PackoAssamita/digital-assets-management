/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.modules.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.transcodingServer.Constantes;
import org.iavante.sling.transcodingServer.CopyFile;
import org.iavante.sling.transcodingServer.Proceso;
import org.iavante.sling.transcodingServer.modules.gadJobInterface;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class is called when the param "noconversion" is filled in the
 * executable field. Normally is because this job require no video process and
 * only it's needed the upload to the Distribution server indicated.
 * 
 */
public class UnknownApp implements gadJobInterface {
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory.getLogger(UnknownApp.class);

	protected void activate(ComponentContext context) {
		log.info("Activated UnknownApp Class");
	}

	private void initialize() {

	}

	private String extractMetadata(Node jobNode) {
		// Metadata Extract

		String nombre = null;
		try {
			nombre = jobNode.getProperty("source_file").getValue().getString();
			log.info("Extrackting Metadata of " + nombre);
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		String tempDestination = null;
		try {
			tempDestination = jobNode.getProperty("tempDestination").getValue()
					.getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		List<String> cadenas0 = new ArrayList();
		cadenas0.add(nombre);
		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i " + tempDestination;
		Map aux0 = p0.start(orden0);
		for (int i = 0; i < cadenas0.size(); i++) {
			if (((List) aux0.get(cadenas0.get(i))).size() > 0) {
				String linea = (String) ((List) aux0.get(cadenas0.get(i))).get(0);
				String regex = cadenas0.get(i) + "(\\:)(\\s+)[\\S]+";
				String res = "";
				Pattern p = Pattern.compile(regex);
				Matcher m = p.matcher(linea);
				if (m.find()) {
					res = m.group();
					String[] aux2 = res.split("\\:[\\s]+");
					aux2[1] = aux2[1].replaceAll(";$", "");
					try {
						jobNode.setProperty("mime", aux2[1]);
						log.info("Mime Type is " + aux2[1]);
						jobNode.save();
					} catch (ValueFormatException e) {
						e.printStackTrace();
					} catch (VersionException e) {

						e.printStackTrace();
					} catch (LockException e) {

						e.printStackTrace();
					} catch (ConstraintViolationException e) {

						e.printStackTrace();
					} catch (RepositoryException e) {

						e.printStackTrace();
					}

				}
			}
		}

		return "OK.";
	}

	public String runProcess(Node jobNode) {

		@SuppressWarnings("unused")
		String response = "";
		@SuppressWarnings("unused")
		Boolean error = false;

		@SuppressWarnings("unused")
		String my_source_location;
		@SuppressWarnings("unused")
		String my_target_location;
		@SuppressWarnings("unused")
		String my_executable = null;
		@SuppressWarnings("unused")
		String my_params = "";
		@SuppressWarnings("unused")
		String my_notification_url;
		@SuppressWarnings("unused")
		String my_extern_storage_url;
		@SuppressWarnings("unused")
		String my_extern_storage_server;
		@SuppressWarnings("unused")
		String my_tempDestination = null;
		@SuppressWarnings("unused")
		String my_source_file;
		@SuppressWarnings("unused")
		String my_target_file = null;

		try {
			log.info("unknowRunProcess:>getting properties");
			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();
			my_executable = jobNode.getProperty("executable").getValue().getString();
			if (jobNode.hasProperty("params")) {
				my_params = jobNode.getProperty("params").getValue().getString();
			}

			my_notification_url = jobNode.getProperty("notification_url").getValue()
					.getString();
			my_extern_storage_server = jobNode.getProperty("extern_storage_server")
					.getValue().getString();
			my_extern_storage_url = jobNode.getProperty("extern_storage_url")
					.getValue().getString();
			my_tempDestination = jobNode.getProperty("tempDestination").getValue()
					.getString();
			my_source_file = jobNode.getProperty("source_file").getValue()
					.getString();
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();

		} catch (ValueFormatException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. ValueFormatException" + e1;
		} catch (IllegalStateException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. IllegalStateException" + e1;
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException" + e1;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			response = "Error. RepositoryException" + e1;
		}

		if (!error) {

			List cadenas0 = new ArrayList();
			cadenas0.add("Error");
			Proceso p0 = new Proceso();
			String orden0 = my_executable + " " + my_tempDestination + " "
					+ my_params + " " + Constantes.getworkingDir() + "/" + my_target_file;

			log.info("System Call-->" + orden0 + "   strings:" + cadenas0);
			Map aux0 = p0.start(orden0);

			for (int i = 0; i < cadenas0.size(); i++) {

				if (((List) aux0.get(cadenas0.get(i))).size() > 0) {
					String linea = (String) ((List) aux0.get(cadenas0.get(i))).get(0);
					String regex = cadenas0.get(i) + "(\\:)(\\s+)[\\S]+";
					String res = "";
					Pattern p = Pattern.compile(regex);
					Matcher m = p.matcher(linea);
					if (m.find()) {
						res = m.group();
						String[] aux2 = res.split("\\:[\\s]+");
						aux2[1] = aux2[1].replaceAll(";$", "");

					}
				}
			}
		}

		// Checking the outputFile

		if (!error) {
			String salida = Constantes.getworkingDir() + "/" + my_target_file;
			File my_output_file = new File(salida);
			if (my_output_file.exists()) {

				if (my_output_file.length() > 1) {
					response = "OK. File converted.";

				} else {
					response = "Error converting the file. Check programs isntalled and configuration parameters.";
					error = true;
				}

			}
		}

		return response;

	}

	private String checkFiles(Node JobNode) {

		log.info("checkingFiles");
		String response = "";
		Boolean error = false;
		Node myNode = null;
		String my_source_location = null;
		String my_target_location = null;
		myNode = JobNode;

		try {
			my_source_location = myNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_source_location = "";
			e1.printStackTrace();
		} catch (RepositoryException e1) {

			e1.printStackTrace();
		}

		try {
			my_target_location = myNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_target_location = "";
			e1.printStackTrace();
		} catch (RepositoryException e1) {

			e1.printStackTrace();
		}

		String source_file = "";
		String tempDestination = "";
		String source_folder = null;
		String target_folder = null;

		// Check input Path
		// Check file exist
		// Check output Path
		// set Property "source_folder

		source_folder = (String) my_source_location.subSequence(0,
				my_source_location.lastIndexOf("/"));

		target_folder = (String) my_target_location.subSequence(0,
				my_target_location.lastIndexOf("/"));

		File my_source_folder = new File(source_folder);
		File my_source_file = new File(my_source_location.toString());
		File my_target_folder = new File(target_folder);

		if (!my_source_folder.isDirectory()) {
			error = true;
			response = "Error. Source Folder doesn't exist (" + source_folder + ")";
			log.info("_____error________" + response);
		} else if (!my_source_file.exists()) {
			error = true;
			response = "Error. File doesn't exist (" + my_source_location + ")";
			log.info("_____error________" + response);
		} else if (!my_target_folder.isDirectory()) {
			error = true;
			response = "Error. Target Folder doesn't exist (" + target_folder + ")";
			log.info("_____error________" + response);
		}
		;

		if (!error) {
			try {

				myNode.setProperty("source_file", my_source_file.getName());
				myNode.setProperty("source_folder", source_folder);
				myNode.setProperty("target_folder", target_folder);
				myNode.save();
				log.info("CheckingFiles:OK");

				response = "OK";

			} catch (ValueFormatException e) {

				e.printStackTrace();
			} catch (VersionException e) {

				e.printStackTrace();
			} catch (LockException e) {

				e.printStackTrace();
			} catch (ConstraintViolationException e) {

				e.printStackTrace();
			} catch (RepositoryException e) {

				e.printStackTrace();
			}

		}

		return response;

	}

	private String copyFiletoWorkingFolder(Node jobNode) {

		log.info("copyFiletoWorkingFolder");
		Boolean error = false;
		String thisresponse = null;
		String my_source_location = null;
		String my_target_location = null;
		String source_file = null;
		String target_file = null;
		String tempDestination = null;

		try {

			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();
			log.info("c1");
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property source_location property not accesible.";
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
		}

		try {
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();
			log.info("c2");
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property target_location property not accesible.";
			log.info(thisresponse);
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info(thisresponse);
		}

		if (error) {
			thisresponse = "Error. Checking the job files and Folders";
			log.info(thisresponse);
		} else {
			source_file = (String) my_source_location.subSequence(my_source_location
					.lastIndexOf("/") + 1, my_source_location.length());
			target_file = (String) my_target_location.subSequence(my_target_location
					.lastIndexOf("/") + 1, my_target_location.length());
			tempDestination = Constantes.getworkingDir();
			log.info("c3" + tempDestination);

			if (!tempDestination.endsWith("/")) {
				tempDestination = tempDestination + "/";
			}

			tempDestination = tempDestination + source_file;

			try {
				log.info("...trying");
				CopyFile.copy(my_source_location, tempDestination);
				log.info("...OK");
				thisresponse = "OK. Copied to WorkingFolder";
			} catch (IOException e) {
				error = true;
				thisresponse = "Error copying the file to workingFolder."
						+ e.getMessage();
				log.info("_____error________" + thisresponse);
				e.printStackTrace();
			}

		}

		try {
			jobNode.setProperty("tempDestination", tempDestination);
			jobNode.setProperty("source_file", source_file);
			jobNode.setProperty("target_file", target_file);
			jobNode.save();
		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}
		return thisresponse;

	}

	private String copyFiletoTargetFolder(Node jobNode) {

		String response = "";
		boolean error = false;
		String my_target_file = null;
		String my_target_fullpath = null;
		String my_output_fullpath = null;

		try {
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();
			my_target_fullpath = Constantes.getworkingDir() + "/" + my_target_file;
			my_output_fullpath = Constantes.getoutputDir() + "/" + my_target_file;
			jobNode.setProperty("output_fullpath", my_output_fullpath);
			jobNode.save();
			CopyFile.copy(my_target_fullpath, my_output_fullpath);
			log.info("...OK");
			response = "OK. Copied to OutPutFolder";

		} catch (ValueFormatException e) {

			e.printStackTrace();
			error = true;
			response = "Error. ValueFormatException.";
		} catch (IllegalStateException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IllegalStateException.";
		} catch (PathNotFoundException e) {

			e.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException.";
		} catch (RepositoryException e) {

			e.printStackTrace();
			error = true;
			response = "Error. RepositoryException.";
		} catch (IOException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IOException.";
		}
		return response;
	}

	public String startJob(Node jobNode) {

		log.info("UnknownApp: The Job starts");
		initialize();
		Boolean error = false;
		String response_checking = "";
		String response_copy = "";
		String response_copy_out = "";
		String response_metadata = "";
		String response_process = "";
		String response = "";

		log.info("1 checkingFiles");
		response_checking = checkFiles(jobNode);

		if (response_checking.startsWith("OK")) {
			// copyFiletoWorkingFolder
			log.info("1 checkingFiles:OK");
			log.info("2 copyingFiles");
			response_copy = copyFiletoWorkingFolder(jobNode);
		} else {
			// if response_cheking is error
			error = true;
			response = response_checking;
			log.info("1 checkingFiles:Error");
		}
		if ((!error) && (response_copy.startsWith("OK"))) {
			// extractMetadata
			log.info("2 copyingFiles:OK");
			log.info("3 extractingMetadata");
			response_metadata = extractMetadata(jobNode);

		} else {
			// if response_copy is error or there is another error before
			error = true;
			response = response_copy;
			log.info("2 copyingFiles:Error");
		}

		if ((!error) && (response_metadata.startsWith("OK"))) {
			// runProcess
			log.info("3 extractingMetadata:OK");
			log.info("4 runningProcess");
			response_process = runProcess(jobNode);
		} else {
			// if response_metadata is error
			error = true;
			response = response_metadata;
			log.info("3 extractingMetadata:Error");

		}
		if ((!error) && (response_process.startsWith("OK"))) {
			// externalStorage
			log.info("4 runningProcess:OK");
			log.info("5 copyToOutputFolder");
			// we've to copy to output folder
			response_copy_out = copyFiletoTargetFolder(jobNode);

		} else {
			// if response_process is error
			error = true;
			response = response_process;
			log.info("4 runningProcess:Error");
		}

		if ((!error) && (response_copy_out.startsWith("OK"))) {
			// everything was OK
			log.info("5 copyToOutputFolder:OK");
		} else {
			// if error en response_copy_out
			error = true;
			response = response_copy_out;
			log.info("5 copyToOutputFolder:Error");
		}

		if (!error) {
			response = "OK.";

		}

		return response;
	}

}