<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.PropertyIterator, javax.jcr.Property, javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%> 
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
	String url = request.getRequestURL().toString();
	String uri = request.getRequestURI();
	String aux = url.replaceAll(uri,"");
	pageContext.setAttribute("urlBase", aux);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  

</head>
<body style="background-color:#FAFAFA">


 <%
	ArrayList sections = new ArrayList();
	
	NodeIterator it = currentNode.getNodes();
    while (it.hasNext()){
    	Node myNode = it.nextNode();
    	String nodeName = myNode.getName();
    	sections.add(nodeName);
    	}
%>

<div id="maincontainer">
	<br />
    <div id="maintitle"><h2>Transcoding Server<h2></div><br />
   <div id="Links"><a style="margin-right:40px" href="pending.html">Pending</a> <a style="margin-right:40px" href="doing.html">Doing</a> <a style="margin-right:40px" href="error.html">Error</a> <a style="margin-right:40px" href="done.html">Done</a></div>
    <br />
	<%
	String c1="#EFEFEF";
	String c2="#E0E0E0";
	String c3="";
	for(int i=0; i<sections.size(); i++){
		String section = (String) sections.get(i);
		Node sectionNode = currentNode.getNode(section);
		PropertyIterator propiter = sectionNode.getProperties(); 

		if (c3.equals(c1)){c3=c2;}
		else{c3=c1;}
		//style="background-color:#FFFF00"
	%>
		<div style="clear:both;width:800px;text-align:left;background-color: <%=c3%>">
		<h4><n><%= section %> </n></h4>
        <table width="800px" style="font-size:10px">		



			<tr>
			<td width="50%">
			<span>Duration, Bitrate, Video: </span><span></td>
			<td width="50%">
			<%if(sectionNode.hasProperty("duration")&& sectionNode.hasProperty("bitrate") && sectionNode.hasProperty("video")) {%>
			<%= sectionNode.getProperty("duration").getValue().getString() %>,<%= sectionNode.getProperty("bitrate").getValue().getString() %>,<%= sectionNode.getProperty("video").getValue().getString() %><%}%>
			</td></tr>				

			<tr>
			<td width="50%">
			<span>Date: </span><span></td>
			<td width="50%">
			<%if(sectionNode.hasProperty("date")) {%>
			<%= sectionNode.getProperty("date").getValue().getString() %><%}%>
			</td></tr>	
			
			<tr>
			<td width="50%">
			<span>Source Location: </span><span></td>
			<td width="50%"><%= sectionNode.getProperty("source_location").getValue().getString() %>
			</td></tr>				


			</td></tr>				
			
			<tr>
			<td width="50%">
			<span>Target Location: </span><span></td>
			<td width="50%"><%= sectionNode.getProperty("target_location").getValue().getString() %>
			</td></tr>				

			</td></tr>
							
				
			

			<tr>
			<td width="50%">
			<span>Executable ,Params: </span></td>
			<td width="50%">
			<%if(sectionNode.hasProperty("executable")&& sectionNode.hasProperty("params") ) {%>
			<%= sectionNode.getProperty("executable").getValue().getString() %>,<%= sectionNode.getProperty("params").getValue().getString() %><%}%>
			



			<tr>
			<td width="50%">
			<span>Mimetype: </span></td>
			<td width="50%">
			<%if(sectionNode.hasProperty("mime")) {%>
			<%= sectionNode.getProperty("mime").getValue().getString() %><%}%>
			</td></tr>				

			<tr>
			<td width="50%">
			<span>External Storage + Url: </span><span></td>
			<td width="50%"><%= sectionNode.getProperty("extern_storage_server").getValue().getString() +" "+ sectionNode.getProperty("extern_storage_url").getValue().getString()%>
			</td></tr>				

				
				
			
			<tr>
			<td width="50%">
			<span>Log: </span></td>
			<td width="50%">
			<%if(sectionNode.hasProperty("log")) {%>
			<%= sectionNode.getProperty("log").getValue().getString() %><%}%>
			</td></tr>

					


		</table>	
		<input type="hidden" name=":redirect" value="<%= url %>" />
        </div>

	<%}%>
</div>

</body>